package cz.mendelu.xjakubis.vida_cs_parlament_server.model;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_server.controller.ClientHandler;

import java.util.Locale;

public class Client {
    private final ClientHandler clientHandler;
    private final Character id;
    private int rank;
    private Law law;
    private Locale language;
    private ClientGame clientGame;

    public Client(ClientHandler clientHandler, Character id) {
        this.clientHandler = clientHandler;
        this.id = id;
    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    public Character getId() {
        return id;
    }

    public Law getLaw() {
        return law;
    }

    public void setLaw(Law law) {
        this.law = law;
    }

    public Locale getLanguage() {
        return language;
    }

    public void setLanguage(Locale language) {
        this.language = language;
    }

    public ClientGame getClientGame() {
        return clientGame;
    }

    public void setClientGame(ClientGame clientGame) {
        this.clientGame = clientGame;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
