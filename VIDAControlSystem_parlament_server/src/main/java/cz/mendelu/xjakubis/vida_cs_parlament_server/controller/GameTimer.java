package cz.mendelu.xjakubis.vida_cs_parlament_server.controller;

import java.util.Timer;
import java.util.TimerTask;

public class GameTimer extends TimerTask {

    private static final long endGamePeriod = 5*60*1000; //The period after which the game ends in milliseconds

    private final Server server;
    private static Timer timer;

    private GameTimer(Server server) {
        this.server = server;
    }

    @Override
    public void run() {
        server.closeGame();
    }

    public static synchronized void refreshTimer(Server server) {
        timer.cancel();
        start(server);
    }

    public static void start(Server server) {
        timer = new Timer(true);
        timer.scheduleAtFixedRate( new GameTimer(server), endGamePeriod, endGamePeriod);
    }
}
