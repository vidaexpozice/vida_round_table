package cz.mendelu.xjakubis.vida_cs_parlament_server.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Message;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.VotingResult;
import cz.mendelu.xjakubis.vida_cs_parlament_server.model.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ClientHandler extends Thread {
    private ObjectInputStream objectInput;
    private ObjectOutputStream objectOutput;
    private Server server;
    private Socket socket;
    private final BlockingQueue<Message> queue = new ArrayBlockingQueue<Message>(50);
    private Client client;

    public ClientHandler(Server server, Socket socket) {
        try {
            this.server = server;
            this.socket = socket;
            this.objectOutput = new ObjectOutputStream(socket.getOutputStream());
            this.objectInput = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
            server.closeGame();
        }
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread thread = new Thread(() -> {
                    while (true) {
                        try {
                            queue.put((Message)objectInput.readObject());
                        } catch (SocketException ex) {
                            try {
                                socket.close();
                                return;
                            } catch (IOException ex1) {
                                ex1.printStackTrace();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });

                thread.start();

                Message message;

                while (true) {
                    message = queue.take();

                    GameTimer.refreshTimer(server);
                    switch (message.message) {
                        case "LANGUAGE":
                            server.addClientLanguage(this, (Locale) message.object);
                            break;
                        case "START_VOTING":
                            server.runVoting((Law) message.object);
                            break;
                        case "VOTING_RESULT":
                            this.server.gameController.addVotingResult((VotingResult) message.object);
                            break;
                        case "GET_ALL_LAWS":
                            server.sendSummary(this);
                            break;
                        case "GAME_TYPE":
                            server.gameController.setArgumentationGameType((boolean) message.object);
                            break;
                        case "END_GAME":
                            server.closeGame();
                            break;
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public ObjectOutputStream getOutput() {
        return objectOutput;
    }
}
