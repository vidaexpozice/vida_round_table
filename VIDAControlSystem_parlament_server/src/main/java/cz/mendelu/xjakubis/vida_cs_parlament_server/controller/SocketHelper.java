package cz.mendelu.xjakubis.vida_cs_parlament_server.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Message;
import cz.mendelu.xjakubis.vida_cs_parlament_server.model.Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class SocketHelper {

    private final ReentrantLock lock = new ReentrantLock(true);
    private final List<Client> clients;
    private final Server server;

    public SocketHelper(List<Client> clients, Server server) {
        this.clients = clients;
        this.server = server;
    }

    public void sendToSelectedClient(Message message , final ClientHandler clientHandler) {
        try {
            Thread thread = new Thread(() -> {
                try {
                    writeToObject(clientHandler, message);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            thread.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void writeToObject(
            ClientHandler clientHandler,
            Message message)
            throws IOException {
        lock.lock();
        try {
            ObjectOutputStream output = clientHandler.getOutput();
            output.writeObject(message);
            output.flush();
            output.reset();
        } finally {
            lock.unlock();
        }
    }

    public synchronized void sendToAllClients(final Message message) {
        Thread thread = new Thread(() -> {
            try {
                Iterator<Client> iterator = new ArrayList<>(clients).iterator();
                Client client;

                while (iterator.hasNext()) {
                    client = iterator.next();
                    writeToObject(client.getClientHandler(), message);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                server.closeGame();
            }
        });

        thread.start();
    }
}
