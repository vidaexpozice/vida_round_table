package cz.mendelu.xjakubis.vida_cs_parlament_server.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.*;
import cz.mendelu.xjakubis.vida_cs_parlament_common.utils.LawsLoader;
import cz.mendelu.xjakubis.vida_cs_parlament_server.model.Client;

import java.time.LocalDateTime;
import java.util.*;

public class GameController {

    private final List<Client> clients;
    private final Server server;
    private final Random rand = new Random();
    private final List<Character> clientIds = new ArrayList<>();

    public boolean isArgumentationGameType() {
        return isArgumentationGameType;
    }

    public void setArgumentationGameType(boolean argumentationGameType) {
        isArgumentationGameType = argumentationGameType;
    }

    private boolean isArgumentationGameType = false;

    private Game game;
    private Character lastId = '@';

    public GameController(Server server) {
        this.server = server;
        this.clients = new ArrayList<>();
    }

    public List<Character> getClientIds() {
        return clientIds;
    }

    public void setPoints(Law law){
        if (isArgumentationGameType) {
            setPointsForArgumentationMode(law);
        } else {
            setPointsForClassicMode(law);
        }
    }

    private void setPointsForClassicMode(Law law) {
        LawState majorVoteResult = law.getMajorVotingResult();
        if(majorVoteResult == LawState.Unknown) return;

        for (Client client : clients) {
            Optional<VotingResult> clientVotingResult = getClientVotingResultByLaw(client.getId(), law);

            if(!clientVotingResult.isPresent()){
                return;
            }

            if (clientVotingResult.get().getResult() == majorVoteResult) {
                client.getClientGame().addPoint();
            }
        }

        orderClientsByPoints();
    }

    private void setPointsForArgumentationMode(Law law) {
        for (Client client : clients) {
            if(client.getClientGame().getClientLaw().getId() == law.getLawId()){
                continue;
            }

            Law clientLaw =  client.getClientGame().getLawById(law.getId());

            if(clientLaw.getOpinion() == LawState.Unknown){
                client.getClientGame().addPoint();
                continue;
            }

            Optional<VotingResult> clientVotingResult = getClientVotingResultByLaw(client.getId(), law);

            if(!clientVotingResult.isPresent()){
                return;
            }

            if(clientVotingResult.get().getResult() == clientLaw.getOpinion()) {
                client.getClientGame().addPoint();
            }
        }

        orderClientsByPoints();
    }

    private Optional<VotingResult> getClientVotingResultByLaw(Character clientId, Law law) {
        return law.getVotingResultList()
                .stream()
                .filter(v -> v.getVoter().equals(clientId))
                .findFirst();
    }


    public void orderClientsByPoints(){
        clients.sort(Comparator.comparing(c -> c.getClientGame().getPoints(),
                Comparator.reverseOrder()));
        Client lastClient = null;

        for(Client client: clients){
            if(lastClient == null){
                client.setRank(clients.indexOf(client) + 1);
            } else if (lastClient.getClientGame().getPoints() == client.getClientGame().getPoints()) {
                  client.setRank(lastClient.getRank());
            } else {
                client.setRank(lastClient.getRank() + 1);
            }
            lastClient = client;
        }
    }

    public Game prepareGame() throws Exception {
        LawsLoader lawsLoader = new LawsLoader();
        List<Law> allLaws = lawsLoader.getLaws();
        List<Law> selectedLaws = new ArrayList<>();

        if (clients.size() > allLaws.size()) {
            throw new Exception("Cannot find enough laws");
        }

        for (Client client : clients) {
            int randomIndex = rand.nextInt(allLaws.size());
            Law law = allLaws.get(randomIndex);
            allLaws.remove(randomIndex);
            selectedLaws.add(law);
            client.setLaw(law);
        }

        return game = new Game(selectedLaws, this.clientIds);
    }

    public synchronized void addVotingResult(VotingResult result) {
        Game game = this.game;
        game.setVotingResult(result);

        Law law = game.getLawById(result.getLaw().getLawId());

        if (law.getVotingResultList().size() == this.clients.size()){
            game.evaluateLawState(result.getLaw().getLawId());

            this.server.projectorWindowController.updatePieChart();

            GameAnalytics.getRestService()
                    .at("law", "history", "add")
                    .sendPost(new LawHistory(law, law.getLawState(), LocalDateTime.now()), String.class);

            if (!allLawsVoted()) {
                server.endVoting(false, law);
            } else {
                GameAnalytics.getRestService().at("votingResult","addAll")
                        .sendPost(game.getAllVotingResults(), String.class);
                server.endGame(law);
            }
        }
    }

    public ClientGame getClientGame(Client client){

        List<Law> clientLaws =  new ArrayList<>();
        LawState[] lawStates = new LawState[]{LawState.Approved, LawState.Rejected, LawState.Unknown};
        int[] lawStatesCount = new int[]{0,0,0};

        for(Law law: this.getGame().getLaws()){
            int index = rand.nextInt(lawStates.length);

            while (lawStatesCount[index] == 4) {
                index = rand.nextInt(lawStates.length);
            }

            Law lawForClient = new Law(law.getLawId(),law.getName_cz(), law.getName_en(), law.getDescription_cz(), law.getDescription_en(),
                    lawStates[index], law.getLawState(), client.getId());
            clientLaws.add(lawForClient);
            lawStatesCount[index]++;
        }

        return new ClientGame(clientLaws, this.getClientIds(), client.getLaw(), client.getId());
    }

    public Client initClient(ClientHandler clientHandler){
        lastId++;
        Character newClientId = lastId;
        this.getClientIds().add(newClientId);
        Client client = new Client(clientHandler, newClientId);
        this.getClients().add(client);
        return client;
    }

    public Summary getSummary(Client client){
        return new Summary( this.getGame().getLaws(),
                                client.getClientGame().getPoints(),
                                client.getRank());
    }

    public boolean allLawsVoted() {
        long unVotedLawsCount = this.game.getLaws()
                                            .stream()
                                            .filter(law -> law.getLawState() == LawState.Unknown)
                                            .count();
        return unVotedLawsCount == 0;
    }

    public List<Client> getClients() {
        return clients;
    }

    public Game getGame() {
        return game;
    }
}
