package cz.mendelu.xjakubis.vida_cs_parlament_server;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Game;
import cz.mendelu.xjakubis.vida_cs_parlament_server.controller.ProjectorWindowController;
import cz.mendelu.xjakubis.vida_cs_parlament_server.controller.Server;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Random;

public class MainLoader extends Application {
    private Stage primaryStage;
    private static int port;
    private Server server;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        server = new Server(port);
        server.addListener(
                () -> {
                    try {
                        initGame();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );

        server.start();

        final BorderPane mainPane = new BorderPane();
        final Label label = new Label("waiting for clients to connect");
        label.setFont(new Font("Arial", 28));
        mainPane.setCenter(label);
        mainPane.setPrefSize(600, 400);
        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setTitle("Parlament projector");
        primaryStage.setFullScreen(true);
        primaryStage.setOnCloseRequest(event -> {
            //server.stopThreads();
            Platform.exit();
            System.exit(0);
        });
        //primaryStage.setMinWidth(1000);
        //primaryStage.setMinHeight(1000);
        primaryStage.show();
    }

    private void initGame() throws Exception {
        FXMLLoader projectorWindowLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/xjakubis/vida_cs_parlament_server/view/ProjectorWindow.fxml"));

        Parent projectorWindowRoot;
        try {
            projectorWindowRoot = projectorWindowLoader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        Random random = new Random(System.currentTimeMillis());
        int randomBackgroundNumber = random.nextInt(4) + 1;

        Game game = server.sendInitGameToAll();

        ProjectorWindowController projectorWindowController = projectorWindowLoader.getController();
        this.server.setProjectorWindowController(projectorWindowController);

        try {
            projectorWindowController.init(
                    game
                    );
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        Platform.runLater(() -> {
            this.primaryStage.setFullScreenExitHint("");
            this.primaryStage.setFullScreen(true);
            this.primaryStage.getScene().setRoot(projectorWindowRoot);
            //this.primaryStage.setScene(new Scene(projectorWindowRoot));
            projectorWindowRoot.requestFocus();
        });
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            return;
        }

        port = Integer.parseInt(args[0]);
        launch(args);
    }
}
