package cz.mendelu.xjakubis.vida_cs_parlament_server.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Game;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import cz.mendelu.xjakubis.vida_cs_parlament_server.localization.I18N;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.FlowPane;

public class ProjectorWindowController {

    @FXML
    private FlowPane rootFlowPane;

    private static final Dimension2D WINDOW_SIZE = new Dimension2D(1024, 768);
    private Game game;
    private PieChart chart;

    public void init(Game game) {
        Platform.runLater(() -> {
            this.game = game;
            rootFlowPane.getChildren().clear();
            rootFlowPane.setPrefWidth(WINDOW_SIZE.getWidth());
            rootFlowPane.setPrefHeight(WINDOW_SIZE.getHeight());
            PieChart pieChart = getPieChart();
            pieChart.setPrefHeight(WINDOW_SIZE.getHeight()-40);
            pieChart.setPrefWidth(WINDOW_SIZE.getHeight()-40);
            rootFlowPane.getChildren().add(pieChart);
            applyCustomColorSequence(pieChart.getData());
        });
    }

    private PieChart getPieChart() {
        ObservableList<PieChart.Data> pieChartData = getPieChartData();
        chart = new PieChart(pieChartData);
        chart.setLegendVisible(false);
        return chart;
    }

    private  ObservableList<PieChart.Data> getPieChartData() {
        long approvedCount = game.getNumberOfLawsByState(LawState.Approved);
        long rejectedCount = game.getNumberOfLawsByState(LawState.Rejected);
        long unknownCount = game.getNumberOfLawsByState(LawState.Unknown);

        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data(I18N.get("label.approved"), approvedCount),
                        new PieChart.Data(I18N.get("label.rejected"), rejectedCount),
                        new PieChart.Data(I18N.get("label.remaining"), unknownCount));

        pieChartData = pieChartData.filtered(data -> data.getPieValue() > 0);
        return pieChartData;
    }

    public void updatePieChart() {
        Platform.runLater(() -> {
            ObservableList<PieChart.Data> data = getPieChartData();
            this.chart.setData(data);
            this.applyCustomColorSequence(data);
        });
    }

    private void applyCustomColorSequence(
            ObservableList<PieChart.Data> pieChartData) {

        final String localizedApproved = I18N.get("label.approved");
        final String localizedRejected = I18N.get("label.rejected");

        for (PieChart.Data data : pieChartData) {
            Node node = data.getNode();
            String color = "orange";

            if (data.getName().equals(localizedApproved)) {
                color = "green";
            } else if (data.getName().equals(localizedRejected)){
                color = "red";
            }

            node.setStyle("-fx-pie-color: " + color  + ";");
        }
    }
}
