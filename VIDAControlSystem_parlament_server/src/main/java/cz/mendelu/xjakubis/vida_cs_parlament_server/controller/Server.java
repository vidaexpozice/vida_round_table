package cz.mendelu.xjakubis.vida_cs_parlament_server.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.*;
import cz.mendelu.xjakubis.vida_cs_parlament_server.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_server.model.Client;
import javafx.application.Platform;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class Server extends Thread {

    private final int endGameTimeInSeconds = 30;
    private final int votingResultTimeInSeconds = 15;

    public ProjectorWindowController projectorWindowController;
    public final GameController gameController;

    private final SocketHelper socketHelper;
    private final ServerSocket serverSocket;
    private final List<IInitGameListener> initGameListeners = new ArrayList<>();
    private boolean acceptClients = true;


    public Server(int port) throws IOException {
        gameController = new GameController(this);
        serverSocket = new ServerSocket(port);
        GameTimer.start(this);
        printMessageOnServer("Poslouchám na IP adrese "
                + InetAddress.getLocalHost().getHostAddress() + ", na portu "
                + serverSocket.getLocalPort());
        printMessageOnServer("Čekám až se připojí klienti");
        
        this.socketHelper = new SocketHelper(gameController.getClients(), this);
    }

    @Override
    public void run() {
        while (acceptClients) {
            Socket socket;
            try {
                socket = serverSocket.accept();
                socket.setKeepAlive(true);
                socket.setSoTimeout(Integer.MAX_VALUE);
                acceptClient(socket);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void addClientLanguage(ClientHandler clientHandler, Locale locale) {
        boolean allHaveLanguage = true;

        for (Client client : gameController.getClients()) {
            if (client.getClientHandler().equals(clientHandler)) {
                client.setLanguage(locale);
            } else if (client.getLanguage() == null) {
                allHaveLanguage = false;
            }
        }

        if (allHaveLanguage) {
            chooseGlobalLanguage();
            for (IInitGameListener listener : initGameListeners) {
                listener.initBoard();
            }
        }
    }

    public synchronized void acceptClient(Socket socket) {
        printMessageOnServer("Přijal jsem klienta na portu " + socket.getPort() +
                " a IP adrese " + socket.getInetAddress().toString().replace("/", ""));

        ClientHandler clientHandler = new ClientHandler(this, socket);

        Client client = gameController.initClient(clientHandler);
        clientHandler.setClient(client);
        clientHandler.start();

        if (gameController.getClients().size() == 1) {
            countdownLanguageChoosing();
        }
    }

    public void runVoting(Law law){
        socketHelper.sendToAllClients(new Message( "VOTE", law));
    }

    private void countdownLanguageChoosing() {
        Thread colorCountdownThread = new Thread() {
            @Override
            public void run() {
                super.run();

                for (int i = 10; i > 0; i--) {
                    socketHelper.sendToAllClients(new Message("COUNTDOWN_LANGUAGE", i));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }

                acceptClients = false;
                socketHelper.sendToAllClients(new Message("SEND_ME_LANGUAGE"));
            }
        };

        colorCountdownThread.start();
    }

    public synchronized void setProjectorWindowController(ProjectorWindowController projectorWindowController) {
        this.projectorWindowController = projectorWindowController;
    }

    public void sendSummary(ClientHandler clientHandler) {
        Message message = new Message("ALL_LAWS", gameController.getSummary(clientHandler.getClient()));
        socketHelper.sendToSelectedClient(message, clientHandler);
    }

    public synchronized void addListener(IInitGameListener listener) {
        initGameListeners.add(listener);
    }

    public synchronized Game sendInitGameToAll() throws Exception {
        Game game = gameController.prepareGame();

        this.gameController.getClients().forEach(client -> {
            ClientGame clientGame = gameController.getClientGame(client);
            client.setClientGame(clientGame);
            socketHelper.sendToSelectedClient(new Message("INIT_GAME" , clientGame),
                    client.getClientHandler());
        });
        return game;
    }

    private void runCountdown(int timeInSeconds) {
        for (int i = timeInSeconds; i > 0; i--) {
            socketHelper.sendToAllClients(new Message("COUNTDOWN", i));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void endVoting(boolean endGame, Law law) {
        gameController.setPoints(law);
        socketHelper.sendToAllClients(new Message( "END_VOTING", law));
        runCountdown(votingResultTimeInSeconds);

        if(!endGame){
            socketHelper.sendToAllClients(new Message( "START_WINDOW", gameController.getGame().getLaws()));
        }
    }

    public synchronized void endGame(Law law) {
        Thread countdownThread = new Thread() {
            @Override
            public void run() {
                super.run();
                endVoting(true, law);
                sendSummaryToClients();
                runCountdown(endGameTimeInSeconds);
                socketHelper.sendToAllClients(new Message("STATISTICS"));
                runCountdown(endGameTimeInSeconds);

                closeGame();
            }
        };

        countdownThread.start();
    }

    public void closeGame(){
        Platform.exit();
        System.exit(0);
    }

    private void sendSummaryToClients(){
        gameController.getClients().forEach(client -> {
            Message message = new Message("END_GAME", gameController.getSummary(client));
            socketHelper.sendToSelectedClient( message, client.getClientHandler());
        });
    }

    private void chooseGlobalLanguage(){
        Locale defaultLocale = new Locale("cz");
        List<Locale> locale = gameController.getClients().stream().map(Client::getLanguage).collect(Collectors.toList());
        int czLocaleCount = (int)locale.stream().filter(l-> l.getLanguage().equals("cz")).count();
        int enLocaleCount = (int)locale.stream().filter(l-> l.getLanguage().equals("en")).count();

        if(czLocaleCount < enLocaleCount){
            I18N.setLocale(Locale.ENGLISH);
            return;
        }

        I18N.setLocale(defaultLocale);
    }

    private synchronized void printMessageOnServer(String message) {
        System.out.println(message);
    }
}
