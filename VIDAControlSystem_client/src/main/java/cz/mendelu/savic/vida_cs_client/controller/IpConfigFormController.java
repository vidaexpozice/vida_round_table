/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_client.controller;

import cz.mendelu.savic.vida_cs.model.IPAddressConfig;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author david
 */
public class IpConfigFormController {

    @FXML
    private TextField ipAddressTextField;
    private Stage ipConfigFormStage;

    public void init(Stage ipConfigFormStage) {
        this.ipConfigFormStage = ipConfigFormStage;
    }

    public void handleConfirmButton() {
        try {
            new IPAddressConfig().setIPAddress(ipAddressTextField.getText());
            ipConfigFormStage.close();
        } catch (IOException ex) {
            ipAddressTextField.setText("Neda�� se zapsat IP adresu do souboru.");
        }
    }

    @FXML
    public void onEnterPressed(ActionEvent event) {
        handleConfirmButton();
    }

    public void ipAddressTextFieldAppend(String text) {
        ipAddressTextField.setText(ipAddressTextField.getText() + text);
    }

    public void handle0() {
        ipAddressTextFieldAppend("0");
    }

    public void handle1() {
        ipAddressTextFieldAppend("1");
    }

    public void handle2() {
        ipAddressTextFieldAppend("2");
    }

    public void handle3() {
        ipAddressTextFieldAppend("3");
    }

    public void handle4() {
        ipAddressTextFieldAppend("4");
    }

    public void handle5() {
        ipAddressTextFieldAppend("5");
    }

    public void handle6() {
        ipAddressTextFieldAppend("6");
    }

    public void handle7() {
        ipAddressTextFieldAppend("7");
    }

    public void handle8() {
        ipAddressTextFieldAppend("8");
    }

    public void handle9() {
        ipAddressTextFieldAppend("9");
    }

    public void handleDot() {
        ipAddressTextFieldAppend(".");
    }

    public void handleBackspace() {
        if (!ipAddressTextField.getText().isEmpty()) {
            ipAddressTextField.setText(ipAddressTextField.getText()
                    .substring(0, ipAddressTextField.getText().length() - 1));
        }
    }
}
