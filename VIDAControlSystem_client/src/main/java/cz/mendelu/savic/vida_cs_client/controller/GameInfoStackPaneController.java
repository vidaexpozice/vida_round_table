/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_client.controller;

import cz.mendelu.savic.vida_cs_client.model.GameClient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

/**
 *
 * @author david
 */
public class GameInfoStackPaneController extends StackPane {

    private final boolean REPEATABLE_ITERATION = true;
    @FXML
    private StackPane root;
    @FXML
    private GameInfoController gameInfoController;
    private Client client;
    private HashMap<GameClient, Node> gameInfoScreens;
    private ArrayList<GameClient> games;
    private ListIterator<GameClient> gameIterator;
    private GameClient actualGame;
    private GameClient previousGame;
    private GamePickerController gamePickerController;

    public void init(Client client, ArrayList<GameClient> games, GamePickerController gamePickerController) throws IOException {
        this.client = client;
        this.gamePickerController = gamePickerController;

        gameInfoScreens = new HashMap<>();

        for (GameClient game : games) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/mendelu/savic/vida_cs_client/view/GameInfo.fxml"));
            Parent root = (Parent) loader.load();
            //GameInfoController gameInfoController = loader.getController();
            //this.gameInfoController.init(client, game);
            gameInfoScreens.put(game, root);
        }

        this.games = games;
        gameIterator = this.games.listIterator();
        actualGame = games.get(0);  // set actual game to first

        this.gameInfoController.init(client, actualGame, gamePickerController);
    }

    public void switchToNextGame() {
        if (gameInfoController.getStartStatus() == GameStartStatus.canJoin) {
            getChildren().clear();
            setNextGame();
            getChildren().add(gameInfoScreens.get(actualGame));
            playFadeInEffect();
            gameInfoController.init(client, actualGame, gamePickerController);
        }
    }

    public void switchToPreviousGame() {
        if (gameInfoController.getStartStatus() == GameStartStatus.canJoin) {
            getChildren().clear();
            setPreviousGame();
            getChildren().add(gameInfoScreens.get(actualGame));
            playFadeInEffect();
            this.gameInfoController.init(client, actualGame, gamePickerController);
        }
    }

    private void playFadeInEffect() {
        final DoubleProperty opacity = root.opacityProperty();

        Timeline fadeInEffect = new Timeline(
                new KeyFrame(new Duration(0), new KeyValue(opacity, 0.0)),
                new KeyFrame(new Duration(750), new KeyValue(opacity, 1.0)));
        fadeInEffect.play();
    }

    private void setNextGame() {
        if (REPEATABLE_ITERATION && !gameIterator.hasNext()) {
            // set actualGame to first
            while (gameIterator.hasPrevious()) {
                actualGame = gameIterator.previous();
            }
        } else {
            previousGame = actualGame;
            actualGame = (GameClient) (gameIterator.hasNext() ? gameIterator.next() : actualGame);
            if (actualGame == previousGame) {
                actualGame = (GameClient) (gameIterator.hasNext() ? gameIterator.next() : actualGame);
            }
        }
    }

    private void setPreviousGame() {
        if (REPEATABLE_ITERATION && !gameIterator.hasPrevious()) {
            // set actualGame to last
            while (gameIterator.hasNext()) {
                actualGame = gameIterator.next();
            }
        } else {
            previousGame = actualGame;
            actualGame = (GameClient) (gameIterator.hasPrevious() ? gameIterator.previous() : actualGame);
            if (actualGame == previousGame) {
                actualGame = (GameClient) (gameIterator.hasPrevious() ? gameIterator.previous() : actualGame);
            }
        }
    }

    public GameInfoController getGameInfoController() {
        return gameInfoController;
    }

}
