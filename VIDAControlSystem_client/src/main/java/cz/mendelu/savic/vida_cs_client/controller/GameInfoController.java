/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_client.controller;

import cz.mendelu.savic.vida_cs.model.TransitionFactory;
import cz.mendelu.savic.vida_cs_client.model.GameClient;
import java.io.File;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

enum GameStartStatus {
    canJoin, canLeave, canStart, isRunning;
}

/**
 * FXML Controller class
 *
 * @author david
 */
public class GameInfoController extends VBox {

    @FXML
    private Label gameTitle;
    @FXML
    private ImageView gameImage;
    @FXML
    private Text description;
    @FXML
    private Button actionButton;
    @FXML
    private Button leaveButton;
    @FXML
    private HBox buttonBox;
    private FadeTransition actionButtonTransition;
    @FXML
    private StatusIconController numberOfPlayersIconController;
    @FXML
    private StatusIconController connectedPlayersIconController;
    @FXML
    private StatusIconController projectorIconController;
    private Client client;
    private GameStartStatus startStatus;
    private GameClient game;
    private Label headingLabel;
    private FadeTransition headingLabelTransition;
    private double headingDefaultSize = -1;
    private double headingSmallSize;
    private FadeTransition connectedLabelTransition;

    public void init(Client client, GameClient game, GamePickerController gamePickerController) {
        buttonBox.getChildren().remove(leaveButton);
        this.client = client;
        this.game = game;
        this.headingLabel = gamePickerController.getHeadingLabel();
        this.headingLabelTransition = gamePickerController.getHeadingLabelTransition();
        if (headingDefaultSize == -1) {
            headingDefaultSize = headingLabel.getFont().getSize();
            headingSmallSize = 0.7 * headingDefaultSize;
        }
        description.setText(game.getDescription());
        gameTitle.setText(game.getName());
        // create fading effect for action button (but don't play it yet)
        actionButtonTransition = TransitionFactory.createSlightlyFadingNode(actionButton);
        initStatusIcons();
        initImages();
        updateGUI();
    }

    private void initStatusIcons() {
        numberOfPlayersIconController.init("Po�adovan�\npo�et hr���:", "../resources/images/players.png",
                game.getNumberOfPlayers());
        connectedPlayersIconController.init("P�ipojeno\nhr���:", "../resources/images/connectedPlayers.png",
                String.valueOf(game.getConnectedPlayers()));
        projectorIconController.init("Vyu��v�\nprojektor:", "../resources/images/projector.png",
                game.getIfUsesProjector() ? "Ano" : "Ne");

        if (game.getIfUsesProjector()) {
            projectorIconController.setStatusColorYellow();
        } else {
            projectorIconController.setStatusColorRed();
        }

        updateConnectedPlayersStatusColor();

        // create fading effect for connected players label (but don't play it yet)
        connectedLabelTransition = TransitionFactory.createFadingNode(
                connectedPlayersIconController.getStatusLabel());
    }

    public void updateConnectedPlayersStatusColor() {
        if (game.getConnectedPlayers() < game.getNumberOfPlayersMin()) {
            connectedPlayersIconController.setStatusColorRed();
        } else {
            connectedPlayersIconController.setStatusColorYellow();
        }
    }

    private void initImages() {
        File file = new File(game.getScreenPath());
        Image image = new Image(file.toURI().toString(), 320, 240, false, true);
        gameImage.setImage(image);
    }

    public void handleActionButton() {
        switch (startStatus) {
            case canStart:
                client.startGame(game.getIndex());
                break;
            case canJoin:
                client.joinGame(game.getIndex());
                break;
            case canLeave:
                client.leaveGame(game.getIndex());
                break;
            case isRunning:
                // do nothing
                break;
        }
    }

    public void handleLeaveButton() {
        client.leaveGame(game.getIndex());
    }

    public void updateConnectedPlayers() {
        try {
            connectedPlayersIconController.setStatus(String.valueOf(game.getConnectedPlayers()));
            updateGUI();
            updateConnectedPlayersStatusColor();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateGUIAfterStartGame() {
        startStatus = GameStartStatus.isRunning;
        updateGUI();
    }

    private void setHeadingFontSizeToDefault() {
        headingLabel.setStyle("-fx-font-size: " + headingDefaultSize + ";");
    }

    private void setHeadingFontSizeToSmall() {
        headingLabel.setStyle("-fx-font-size: " + headingSmallSize + ";");
    }

    private void setHeadingFontToRedAndSizeToSmall() {
        headingLabel.setStyle("-fx-font-size: " + headingSmallSize + ";-fx-text-fill: red");
    }

    private void headingLabelTransitionReset() {
        if (headingLabelTransition != null) {
            headingLabelTransition.stop();
            headingLabel.setOpacity(1.0);
            headingLabelTransition.play();
        }
    }

    private void connectedLabelTransitionPlay() {
        connectedLabelTransition.play();
        headingLabelTransitionReset();
    }

    private void connectedLabelTransitionStop() {
        connectedLabelTransition.stop();
        connectedPlayersIconController.getStatusLabel().setOpacity(1.0);
    }

    private void actionButtonTransitionPlay() {
        actionButtonTransition.play();
        headingLabelTransitionReset();
    }

    private void actionButtonTransitionStop() {
        actionButtonTransition.stop();
        actionButton.setOpacity(1.0);
    }

    public void updateGUI() {
        if (startStatus != GameStartStatus.isRunning && game.getIfUsesProjector()
                && client.isProjectorInUse()) {
            Platform.runLater(() -> {
                buttonBox.setVisible(false);
                buttonBox.getChildren().remove(leaveButton);
                headingLabel.setText("Tato hra pot�ebuje projektor, kter� je nyn� vyu��van� jinou hrou");
                setHeadingFontToRedAndSizeToSmall();
                connectedLabelTransitionStop();
                actionButtonTransitionStop();
            });
        } else if (client.getActualGame() != game) {
            Platform.runLater(() -> {
                actionButton.setText("P�idat se");
                if (!buttonBox.isVisible()) {
                    buttonBox.setVisible(true);
                }
                buttonBox.getChildren().remove(leaveButton);
                headingLabel.setText("Vyber hru");
                setHeadingFontSizeToDefault();
                connectedLabelTransitionStop();
                actionButtonTransitionStop();
            });
            startStatus = GameStartStatus.canJoin;
        } else if (startStatus == GameStartStatus.isRunning) {
            Platform.runLater(() -> {
                buttonBox.setVisible(false);
                buttonBox.getChildren().remove(leaveButton);
                headingLabel.setText("Spou�t� se hra");
                setHeadingFontSizeToDefault();
                connectedLabelTransitionStop();
                actionButtonTransitionStop();
            });
        } else if (game.getConnectedPlayers() >= game.getNumberOfPlayersMin()) {
            Platform.runLater(() -> {
                actionButton.setText("Spustit hru");
                buttonBox.getChildren().add(leaveButton);
                headingLabel.setText("M��e� spustit hru");
                setHeadingFontSizeToDefault();
                connectedLabelTransitionStop();
                actionButtonTransitionPlay();
            });
            startStatus = GameStartStatus.canStart;
        } else {
            Platform.runLater(() -> {
                actionButton.setText("Opustit hru");
                buttonBox.getChildren().remove(leaveButton);
                headingLabel.setText("Po�kej a� se p�id� dostatek hr��� pro spu�t�n� hry");
                setHeadingFontSizeToSmall();
                connectedLabelTransitionPlay();
                actionButtonTransitionStop();
            });
            startStatus = GameStartStatus.canLeave;
        }
    }

    public GameStartStatus getStartStatus() {
        return startStatus;
    }

}
