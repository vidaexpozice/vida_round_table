package cz.mendelu.savic.vida_cs_client;

import java.io.IOException;

import cz.mendelu.savic.vida_cs.model.IPAddressConfig;
import cz.mendelu.savic.vida_cs.model.SimpleModalWindow;
import cz.mendelu.savic.vida_cs_client.controller.GameInfoController;
import cz.mendelu.savic.vida_cs_client.controller.GamePickerController;
import cz.mendelu.savic.vida_cs_client.controller.IpConfigFormController;
import cz.mendelu.savic.vida_cs_client.controller.Client;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Modality;
import static javafx.application.Application.launch;

public class MainLoader extends Application {

    private Stage primaryStage;
    private Client client;
    private GamePickerController gamePickerController;

    @Override
    public void start(Stage primaryStage) throws IOException, InterruptedException {
        this.primaryStage = primaryStage;

        FXMLLoader loaderPicker
                = new FXMLLoader(getClass().getResource("/cz/mendelu/savic/vida_cs_client/view/GamePicker.fxml"));
        Parent root = loaderPicker.load();

        this.primaryStage.setScene(new Scene(root));
        this.primaryStage.setTitle("VIDA Game Picker");
        this.primaryStage.setFullScreenExitHint("");
        this.primaryStage.setFullScreen(true);

        this.client = new Client(this);

        IPAddressConfig ipConfig = new IPAddressConfig();

        SimpleModalWindow connectInfoWindow = new SimpleModalWindow(this.primaryStage, "Zkou��m se p�ipojit na server.");
        connectInfoWindow.getModalWindow().setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

        Thread connectThread;

        try {
            // while it is not possible to connect to the client
            while (true) {
                connectThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            client.connect(ipConfig.getIPAddress());
                            Platform.runLater(() -> connectInfoWindow.getModalWindow().close());
                        } catch (IOException ex) {
                            // do nothing
                        }
                    }
                };

                connectThread.setDaemon(true);
                connectThread.start();

                connectInfoWindow.showModalWindow();

                while (connectThread.isAlive()) {
                    Thread.sleep(500);
                }

                if (!client.isConnected()) {
                    // show modal window to enter correct server IP addres
                    showIpConfigModalWindow();
                } else {
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

            Platform.exit();
            System.exit(0);
        }

        this.primaryStage.setOnCloseRequest(e -> {
            client.terminateGame();
            Platform.exit();
            System.exit(0);
        });

        this.primaryStage.show();

        this.gamePickerController = loaderPicker.getController();
        this.gamePickerController.init(this, this.client);

        client.updateGames();

        this.client.requestProjectorInfo();
    }

    public Client getClient() {
        return client;
    }

    public void updateGameInfoController() {
        while (gamePickerController == null) {
            try {
                Thread.sleep(100); // wait
            } catch (InterruptedException ex) {
                // do nothing
            }
        }
        final GameInfoController controller = gamePickerController.getGameInfoController();

        if (controller != null) {
            controller.updateConnectedPlayers();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void hideArrows() {
        gamePickerController.hideArrows();
    }

    public void showArrows() {
        gamePickerController.showArrows();
    }

    public void updateGUI() throws InterruptedException {
        gamePickerController.getGameInfoController().updateGUI();
    }

    public void updateGUIAfterStartGame() {
        gamePickerController.getGameInfoController().updateGUIAfterStartGame();
    }

    public void showIpConfigModalWindow() throws IOException {
        FXMLLoader loader
                = new FXMLLoader(getClass().getResource("/cz/mendelu/savic/vida_cs_client/view/IpConfigForm.fxml"));

        Parent root = loader.load();
        IpConfigFormController controller = loader.getController();

        Stage stage = new Stage();
        controller.init(stage);

        // make new window modal -> block events from parent:
        stage.initModality(Modality.WINDOW_MODAL);
        // keep primary stage on background (it's not minimized):
        stage.initOwner(primaryStage);
        //stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(new Scene(root));

        stage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

        stage.showAndWait();
    }

}
