package cz.mendelu.savic.vida_cs_client.controller;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs.model.Game;
import cz.mendelu.savic.vida_cs.model.GameLoader;
import cz.mendelu.savic.vida_cs.model.GameProcess;
import cz.mendelu.savic.vida_cs.model.IPAddressConfig;
import cz.mendelu.savic.vida_cs_client.MainLoader;
import cz.mendelu.savic.vida_cs_client.model.GameClient;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Client for communicating with server and sending following message signals:
 *
 * Client -> Server:
 *   JOIN X - I wanna join game with number X (or create if I'm first).
 *   LEAVE X - I wanna leave waiting queue for game with number X.
 *   START X - I want to launch game with number X.
 *   GET_INFO X - I wanna know how many players are connected to game X.
 *   FINISH X - I finished game with number X (either game finished, player left, or error occured)
 *   GET_PROJECTOR_INFO - I wanna know if the projector is being used.
 *
 * Server -> Client:
 *   ACCEPT X - I accept your request to join game X. (he will be accepted as long
 *              as he is playing the game) -> player goes to INGAME status.
 *   REJECT X - I reject your request to join game X.
 *   CONNECTED X Y - The game with number X has Y players connected.
 *   MESSAGE text - Client, please notify the user with this message.
 *   IN_PROGRESS X - Game with number X is in progress (already played).
 *   LAUNCH X P - Client, please start game X which you requested on port P.
 *   END X - Client, close game X (if you didn't do it yet), because it finished.
 *   PROJECTOR_IN_USE - Client, projector is being used right now.
 *   PROJECTOR_NOT_IN_USE - Client, projector is available. You can use it.
 *   KILL_YOURSELF - Client, close your application.
 *
 * Where X is number of the game and Y number of players.
 *
 */
public class Client extends Thread {

    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private ArrayList<GameClient> games;
    private final MainLoader mainLoader;
    private GameProcess clientProcess;
    private GameClient actualGame = null;
    private boolean projectorInUse = false;
    private boolean connected = false;

    public Client(MainLoader mainLoader) {
        this.mainLoader = mainLoader;
    }

    public boolean isConnected() {
        return connected;
    }

    /**
     * Metoda pro p�ipojen� k serveru.
     * 
     * @param ipAddress IP adresa serveru
     * @return zdali se poda�ilo p�ipojit
     */
    public boolean connect(String ipAddress) {
        if (ipAddress == null) {
            return false;
        }

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ipAddress, 7000), 5000);
            //socket = new Socket(ipAddress, 7000);
            input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(
                    socket.getOutputStream(), true);

            start();
            initGames();
            connected = true;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Metoda pro ukon�en� klientsk� aplikace hry.
     */
    public void terminateGame() {
        if (clientProcess != null) {
            clientProcess.end();
        }
        actualGame = null;
        mainLoader.showArrows();
    }

    /**
     * P�et�en� metoda run, kter� p�ij�m� zpr�vy od serveru.
     */
    @Override
    public void run() {
        String response;

        while (true) {
            try {
                response = input.readLine();
                String[] responseParts = response.split(" ");

                if (response.startsWith("CONNECTED")) {
                    int gameIndex = Integer.valueOf(responseParts[1]);
                    int connectedPlayers = Integer.valueOf(responseParts[2]);
                    games.get(gameIndex)
                            .setConnectedPlayers(connectedPlayers);
                    this.mainLoader.updateGameInfoController();
                } else if (response.startsWith("ACCEPT")) {
                    int gameIndex = Integer.valueOf(responseParts[1]);
                    actualGame = games.get(gameIndex);
                    mainLoader.hideArrows();
                } else if (response.startsWith("END")) {
                    terminateGame();
                } else if (response.startsWith("LAUNCH")) {
                    mainLoader.updateGUIAfterStartGame();

                    int gameIndex = Integer.valueOf(responseParts[1]);
                    int gamePort = Integer.valueOf(responseParts[2]);
                    long gamePoolId = Long.parseLong(responseParts[3]);

                    String arguments = gamePort + " "
                            + new IPAddressConfig().getIPAddress() + " "
                            +  gameIndex + " "
                            + gamePoolId;
                    clientProcess = new GameProcess(
                            games.get(gameIndex).getRunCommandClient(),
                            arguments, () -> finishGame(gameIndex));

                    clientProcess.start();

                } else if (response.startsWith("PROJECTOR_IN_USE")) {
                    projectorInUse = true;
                    mainLoader.updateGUI();
                } else if (response.startsWith("PROJECTOR_NOT_IN_USE")) {
                    projectorInUse = false;
                    mainLoader.updateGUI();
                } else if (response.startsWith("KILL_YOURSELF")) {
                    // close app
                    Platform.exit();
                    System.exit(0);
                }
            } catch (SocketException e) {
                // server disconnected
                try {
                    socket.close();
                } catch (IOException e1) {
                    LogUtils.logException(this, e1);
                }
                return;
            } catch (IOException e) {
                LogUtils.logException(this, e);
            } catch (Exception e) {
                LogUtils.logException(this, e);
            }
        }
    }

    public void finishGame(int gameIndex) {
        output.println("FINISH " + gameIndex);
    }

    public void joinGame(int gameIndex) {
        output.println("JOIN " + gameIndex);
    }

    public void startGame(int gameIndex) {
        output.println("START " + gameIndex);
    }

    public void requestProjectorInfo() {
        output.println("GET_PROJECTOR_INFO");
    }

    public ArrayList<GameClient> getGames() {
        return games;
    }
    
    public void leaveGame(int gameIndex) {
        output.println("LEAVE " + gameIndex);
        mainLoader.showArrows();
        actualGame = null;
    }

    /**
     * Inicializace seznamu her a aktualizace jejich stavu
     * dot�z�n�m serveru p�es po�adavek GET_INFO.
     * 
     * @throws IOException 
     */
    public void initGames() throws IOException {
        ArrayList<Game> games = new GameLoader().getGames();
        this.games = new ArrayList<>();

        for (Game game : games) {
            this.games.add(new GameClient(game));
        }
    }

    public void updateGames() {
        // update number of connected players:
        // request infos (number of connected players) for each game
        for (int i = 0; i < games.size(); i++) {
            output.println("GET_INFO " + i);
        }
    }

    public GameClient getActualGame() {
        return actualGame;
    }

    public boolean isProjectorInUse() {
        return projectorInUse;
    }
}
