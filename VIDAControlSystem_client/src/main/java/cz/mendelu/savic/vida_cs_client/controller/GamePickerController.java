/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_client.controller;

import cz.mendelu.savic.vida_cs.model.TransitionFactory;
import cz.mendelu.savic.vida_cs_client.MainLoader;
import cz.mendelu.savic.vida_cs_client.model.GameClient;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author david
 */
public class GamePickerController extends VBox {

    @FXML
    private GameInfoStackPaneController gameInfoStackPaneController;
    @FXML
    private ImageView rightArrow;
    @FXML
    private ImageView leftArrow;
    @FXML
    private Label headingLabel;
    private FadeTransition headingLabelTransition;
    private ArrayList<GameClient> gameInfos;
    
    public void init(MainLoader mainLoader, Client client) throws IOException {
        gameInfos = mainLoader.getClient().getGames();
        gameInfoStackPaneController.init(client, gameInfos, this);
        initArrows();
        createTransitions();
    }
    
    private void createTransitions() {
        headingLabelTransition = TransitionFactory.createFadingNode(headingLabel);
        headingLabelTransition.play();
        TransitionFactory.createFadingNode(leftArrow).play();
        TransitionFactory.createFadingNode(rightArrow).play();
    }

    private void initArrows() {
        File file = new File("../resources/images/�ipka_vlevo.png");
        Image image = new Image(file.toURI().toString(), 200, 150, true, true);
        leftArrow.setImage(image);
        file = new File("../resources/images/�ipka_vpravo.png");
        image = new Image(file.toURI().toString(), 200, 150, true, true);
        rightArrow.setImage(image);
    }
    
    public void hideArrows() {
        leftArrow.setVisible(false);
        rightArrow.setVisible(false);
    }

    public void showArrows() {
        leftArrow.setVisible(true);
        rightArrow.setVisible(true);
    }

    public void switchToPreviousGame() {
        gameInfoStackPaneController.switchToPreviousGame();
    }

    public void switchToNextGame() {
        gameInfoStackPaneController.switchToNextGame();
    }

    public GameInfoStackPaneController getGameInfoStackPaneController() {
        return gameInfoStackPaneController;
    }

    public GameInfoController getGameInfoController() {
        return gameInfoStackPaneController.getGameInfoController();
    }

    public Label getHeadingLabel() {
        return headingLabel;
    }

    public FadeTransition getHeadingLabelTransition() {
        return headingLabelTransition;
    }

}
