/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_client.model;

import cz.mendelu.savic.vida_cs.model.Game;

/**
 *
 * @author david
 */
public class GameClient extends Game {

    private int connectedPlayers;

    public GameClient(Game game) {
        this(game.getName(), game.getDescription(), game.getScreenPath(), game.getIfUsesProjector(), game.getNumberOfPlayers(),
                game.getRunCommandClient(), game.getRunCommandServer(), game.getIndex());
    }

    public GameClient(String name, String description, String screenPath, boolean usesProjector, String numberOfPlayers,
            String runCommandClient, String runCommandServer, int gameIndex) {
        super(name, description, screenPath, usesProjector, numberOfPlayers, runCommandClient,
                runCommandServer, gameIndex);
        connectedPlayers = 0;
    }

    public int getConnectedPlayers() {
        return connectedPlayers;
    }

    public void setConnectedPlayers(int connectedPlayers) {
        this.connectedPlayers = connectedPlayers;
    }

}
