/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_client.controller;

import java.io.File;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author david
 */
public class StatusIconController {

    @FXML
    Label headerLabel;
    @FXML
    Label statusLabel;
    @FXML
    ImageView image;

    protected void initImages(String imagePath) {
        File file = new File(imagePath);
        Image image = new Image(file.toURI().toString(), 80, 80, true, true);
        this.image.setImage(image);
    }

    public void init(String header, String imagePath, String status) {
        this.headerLabel.setText(header);
        statusLabel.setText(status);
        initImages(imagePath);
    }

    public Label getStatusLabel() {
        return statusLabel;
    }

    public String getStatus() {
        return statusLabel.getText();
    }

    public void setStatus(String status) {
        Platform.runLater(() -> {
            statusLabel.setText(status);
        });
    }
    
    public void setStatusColor(Color color) {
        Platform.runLater(() -> {
            statusLabel.setTextFill(color);
        });
    }
    
    public void setStatusColorRed() {
        Platform.runLater(() -> {
            statusLabel.setTextFill(Color.RED);
        });
    }
    
    public void setStatusColorYellow() {
        Platform.runLater(() -> {
            statusLabel.setTextFill(Color.YELLOW);
        });
    }
}
