package cz.mendelu.xjakubis.vida_cs_analytics_exporter.controller;

import com.opencsv.CSVWriter;
import cz.mendelu.xjakubis.vida_cs_analytics_common.Configuration;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Metric;
import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_analytics_exporter.DynamicTable;
import cz.mendelu.xjakubis.vida_cs_analytics_exporter.MessageHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class PresenterWindowController {
    public ComboBox<Metric> metricsComboBox;
    public TableView tableView;
    public Button addMetricButton;
    public Label statusLabel;
    public Button editMetricButton;
    public Button deleteMetricButton;

    private Stage primaryStage;
    private DynamicTable table;
    private MessageHelper messageHelper;
    private Metric selectedMetric;

    private final String resourcesPath = "/cz/mendelu/xjakubis/vida_cs_analytics_exporter/images/";

    public void init(Stage primaryStage) {
        this.primaryStage = primaryStage;
        Configuration.setConfigurationPath("./config/analytics.properties");
        Future<Metric[]> metrics = GameAnalytics.getRestService().at("metric","getAll").get(Metric[].class);
        table = new DynamicTable(tableView);
        messageHelper = new MessageHelper(statusLabel);
        configureMetricsComboBox();
        applyMetrics(metrics);
        metricsComboBox.getSelectionModel().selectFirst();
    }

    @FXML
    public void initialize(){
        Image addImage = new Image(getClass().getResourceAsStream(resourcesPath + "add-icon.png")) ;
        addMetricButton.setGraphic(new ImageView(addImage));

        Image deleteImage = new Image(getClass().getResourceAsStream(resourcesPath + "delete-icon.png")) ;
        deleteMetricButton.setGraphic(new ImageView(deleteImage));

        Image editImage = new Image(getClass().getResourceAsStream(resourcesPath + "edit-icon.png")) ;
        editMetricButton.setGraphic(new ImageView(editImage));
    }

    public void handleExportButton() {
        FileChooser fileChooser = getFileChooser();

        try {
            File file = fileChooser.showSaveDialog(primaryStage);
            if(file != null) {
                FileWriter fileWriter = new FileWriter(file);
                CSVWriter csvWriter = new CSVWriter(fileWriter);
                ResultSet myResultSet = table.getResultSet();
                csvWriter.writeAll(myResultSet, true);
                fileWriter.flush();
                messageHelper.writeSuccess("Export byl úspěšný");
            }
        } catch (Exception e) {
            messageHelper.writeError(e.getMessage());
        }
    }

    private FileChooser getFileChooser(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save export file");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV", "*.csv")
        );

        return fileChooser;
    }

    public void handleAddButton() {
        try {
            AddMetricWindowController controller = openAddMetricWindow();
            controller.init(this);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void refresh(Metric metric){
        Future<Metric[]> metrics = GameAnalytics.getRestService()
                .at("metric","getAll")
                .get(Metric[].class);
        applyMetrics(metrics);
        metricsComboBox.getSelectionModel().clearSelection();
        metricsComboBox.getSelectionModel().select(metric);

        if(metric == null){
            metricsComboBox.setValue(null);
        }
    }

    private void configureMetricsComboBox(){
        metricsComboBox.setConverter(new StringConverter<Metric>() {
            @Override
            public String toString(Metric object) {
                return object.getDisplayName();
            }

            @Override
            public Metric fromString(String string) {
                return null;
            }
        });

        metricsComboBox.valueProperty().addListener((obs, oldVal, newVal) -> {
            if(newVal != null){
            table.buildData(newVal.getQuery(), messageHelper);
            selectedMetric = newVal;
            editMetricButton.setVisible(true);
            deleteMetricButton.setVisible(true);

            }else{
                editMetricButton.setVisible(false);
                deleteMetricButton.setVisible(false);
            }
        });
    }

    private void applyMetrics(Future<Metric[]> metrics){
        metricsComboBox.getItems().clear();

        try {
            metricsComboBox.getItems().addAll(Arrays.asList(metrics.get()));
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            messageHelper.writeError(e.getMessage());
        }
    }

    public void handleEditButton() {
        if(selectedMetric != null){
            try{
                AddMetricWindowController controller = openAddMetricWindow();
                controller.init(this, selectedMetric);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private AddMetricWindowController openAddMetricWindow() throws IOException {
        Parent root;
        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/xjakubis/vida_cs_analytics_exporter/view/AddMetricWindow.fxml"));
        root = loader.load();
        Stage stage = new Stage();
        stage.setTitle("Nová metrika");
        stage.setScene(new Scene(root, 800, 800));
        stage.show();
        return loader.getController();
    }

    public void handleDeleteButton(ActionEvent actionEvent) {
        showYesNoDialog();
    }

    private void showYesNoDialog(){
        Alert alert = new Alert(Alert.AlertType.NONE);
        alert.setTitle("Odstránít metriku");
        alert.setContentText("Opravdu chcete odstránit tuto metriku?");
        ButtonType okButton = new ButtonType("Ano", ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType("Ne", ButtonBar.ButtonData.NO);
        alert.getButtonTypes().setAll(okButton, noButton);
        alert.showAndWait().ifPresent(type -> {
            if (type.getButtonData() == ButtonType.YES.getButtonData()) {
                GameAnalytics.getRestService()
                        .at("metric","delete", selectedMetric.getId().toString())
                        .delete();
                selectedMetric = null;
                refresh(null);
            }
        });
    }
}
