package cz.mendelu.xjakubis.vida_cs_analytics_exporter;


import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

public class MessageHelper {

    private final Label label;
    private boolean showProgress;

    public MessageHelper(Label label){
        this.label = label;
    }

    public void writeError(String message){
        this.label.setTextFill(Color.RED);
        this.label.setText("ERROR: "+ message);
    }

    public void writeSuccess(String message){
        this.label.setTextFill(Color.GREEN);
        this.label.setText("SUCCESS: "+ message);
    }

    public void writeInfo(String message){
        this.label.setTextFill(Color.BLUE);
        this.label.setText("INFO: "+ message);
    }

    public void startProgress() {
        Platform.runLater(()->{
            while(showProgress) {
                try {
                    this.label.setText(".");
                    Thread.sleep(1000);
                    this.label.setText("..");
                    Thread.sleep(1000);
                    this.label.setText("...");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void stopProgress() {
        this.showProgress = false;
    }
}
