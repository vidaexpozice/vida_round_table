package cz.mendelu.xjakubis.vida_cs_analytics_exporter.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Metric;
import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_analytics_exporter.DynamicTable;
import cz.mendelu.xjakubis.vida_cs_analytics_exporter.MessageHelper;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AddMetricWindowController {
    public TextField nameTextField;
    public TextArea queryTextField;
    public TableView testTableView;
    public Label statusLabel;
    private DynamicTable table;
    private PresenterWindowController presenterWindowController;
    private MessageHelper messageHelper;
    private Metric metric;

    @FXML
    public void initialize(){
       table = new DynamicTable(testTableView);
       messageHelper = new MessageHelper(statusLabel);

    }

    public void handleTestButton() {
        String query = queryTextField.getText();
        if(query.isEmpty()){
            messageHelper.writeError("SQL je prázdne.");
        }

        table.buildData(query, messageHelper);
    }

    public void handleSaveButton() {
        String displayName = nameTextField.getText();
        String query = queryTextField.getText();

        if(displayName.isEmpty()){
            messageHelper.writeError("Název metriky je prázdnej.");
            return;
        }

        if(query.isEmpty()){
            messageHelper.writeError("SQL je prázdne.");
            return;
        }

        if(metric!= null){
            metric.setDisplayName(displayName);
            metric.setQuery(query);
        } else {
            metric = new Metric(displayName, query);
        }

        Future<Metric> metricFuture = GameAnalytics.getRestService().at("metric", "add").sendPost(metric, Metric.class);

        Metric insertedMetric;

        try{
            insertedMetric = metricFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            messageHelper.writeError(e.getMessage());
            return;
        }

        presenterWindowController.refresh(insertedMetric);
        Stage stage = (Stage) nameTextField.getScene().getWindow();
        stage.close();
    }

    public void init(PresenterWindowController presenterWindowController) {
        this.presenterWindowController = presenterWindowController;
    }

    public void init(PresenterWindowController presenterWindowController, Metric metric) {
       init(presenterWindowController);
       this.metric = metric;
       nameTextField.setText(metric.getDisplayName());
       queryTextField.setText(metric.getQuery());
    }
}
