package cz.mendelu.xjakubis.vida_cs_analytics_exporter;

import cz.mendelu.xjakubis.vida_cs_analytics_common.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnector {

    public static Connection getConnection() throws SQLException {
        Configuration configuration = Configuration.getInstance();
        final String DB_URL = configuration.getProperty("database.url");
        final String USER = configuration.getProperty("database.user");
        final String PASS = configuration.getProperty("database.password");
        Connection conn;
        Properties connectionProps = new Properties();
        connectionProps.put("user", USER);
        connectionProps.put("password", PASS);

        conn = DriverManager.getConnection(
                DB_URL,
                connectionProps);

        return conn;
    }
}
