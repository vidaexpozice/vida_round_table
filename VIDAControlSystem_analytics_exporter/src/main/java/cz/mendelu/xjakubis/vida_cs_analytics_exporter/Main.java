package cz.mendelu.xjakubis.vida_cs_analytics_exporter;

import cz.mendelu.xjakubis.vida_cs_analytics_exporter.controller.PresenterWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    PresenterWindowController presenterWindowController;
    
    @Override
    public void start(Stage primaryStage) throws Exception{
        String resources = "/cz/mendelu/xjakubis/vida_cs_analytics_exporter/";
        FXMLLoader loader = new FXMLLoader(getClass().getResource(resources + "view/PresenterWindow.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Analytics Exporter");
        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream(resources + "images/icon.png")));
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
        
        this.presenterWindowController = loader.getController();

        this.presenterWindowController.init(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
