package cz.mendelu.xjakubis.vida_cs_analytics_exporter;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DynamicTable {
    private final TableView tableview;
    private ResultSet resultSet;

    public DynamicTable(TableView tableView){
        this.tableview = tableView;
    }

    public ResultSet getResultSet() throws SQLException {
        this.resultSet.beforeFirst();
        return resultSet;
    }

    public void buildData(String sql, MessageHelper helper){
        tableview.getColumns().clear();
        tableview.getItems().clear();

        Thread thread = new Thread(() -> {
            helper.startProgress();
            Connection c ;
            ObservableList<ObservableList> data = FXCollections.observableArrayList();
            try{
                c = DbConnector.getConnection();
                resultSet = c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(sql);

                for(int i = 0; i< resultSet.getMetaData().getColumnCount(); i++){
                    final int j = i;
                    TableColumn col = new TableColumn(resultSet.getMetaData().getColumnName(i+1));
                    col.setCellValueFactory((Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>)
                            param -> {
                                if(param.getValue().get(j) != null) {
                                    return new SimpleStringProperty(param.getValue().get(j).toString());
                                }
                                else return null;
                            });

                    tableview.getColumns().addAll(col);
                }

                while(resultSet.next()){
                    //Iterate Row
                    ObservableList<String> row = FXCollections.observableArrayList();
                    for(int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
                        row.add(resultSet.getString(i));
                    }

                    data.add(row);
                }

                tableview.setItems(data);
                helper.writeSuccess("Data byla úspěšně načtena");
            }catch(Exception e){
                e.printStackTrace();
                helper.writeError(e.getMessage());
                System.out.println("Error on Building Data");
            }
            helper.stopProgress();
        });
       thread.run();
    }
}

