package cz.mendelu.xjakubis.vida_cs_analytics.controller;

import cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament.LawHistoryRepository;
import cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament.VotingResultRepository;
import cz.mendelu.xjakubis.vida_cs_analytics_common.api_model.VotingResultsStatistic;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.VotingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path="/votingResult")
public class VotingResultController {

    @Autowired
    private VotingResultRepository votingResultRepository;

    @Autowired
    private LawHistoryRepository lawHistoryRepository;

    @PostMapping(path = "/addAll")
    @ResponseBody
    public String addAll(@RequestBody List<VotingResult> votingResults){
        votingResultRepository.saveAll(votingResults);

        return "Response : Successfully added";
    }

    @PostMapping(path = "/add")
    @ResponseBody
    public String add(@RequestBody VotingResult votingResult){
        votingResultRepository.save(votingResult);

        return "Response : Successfully added";
    }

    @GetMapping(path = "/get/statistics/law/{id}")
    @ResponseBody
    public VotingResultsStatistic getStatisticsByLawId(@PathVariable int id){
        List<Object[]> resultsCount = votingResultRepository.findAllResultsByLawId(id);
        List<Object[]> lawHistoryCounts = lawHistoryRepository.findAllLawHistoryByLawId(id);

        Object[] resultRow = resultsCount.get(0);
        Object[] lawHistoryRow = lawHistoryCounts.get(0);

        long forCount = ((BigDecimal)resultRow[0]).longValue();
        long againstCount = ((BigDecimal)resultRow[1]).longValue();
        long approvedCount = ((BigDecimal)lawHistoryRow[0]).longValue();
        long rejectedCount = ((BigDecimal)lawHistoryRow[1]).longValue();
        LocalDateTime from = ((Timestamp)resultRow[2]).toLocalDateTime();
        LocalDateTime to = ((Timestamp)resultRow[3]).toLocalDateTime();

        return new VotingResultsStatistic(forCount, againstCount, from, to, approvedCount, rejectedCount);
    }
}
