package cz.mendelu.xjakubis.vida_cs_analytics.controller;

import cz.mendelu.xjakubis.vida_cs_analytics.repository.MetricRepository;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Metric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path="/metric")
public class MetricController {

    @Autowired
    MetricRepository metricRepository;

    @PostMapping(path = "/add")
    @ResponseBody
    public Metric add(@RequestBody Metric metric){
        return metricRepository.save(metric);
    }

    @GetMapping(path = "/getAll")
    @ResponseBody
    public Iterable<Metric> getAll(){
        return metricRepository.findAll();
    }


    @GetMapping(path = "/get/{id}")
    @ResponseBody
    public Optional<Metric> get(@PathVariable Long id){
        return metricRepository.findById(id);
    }

    @DeleteMapping(path = "/delete/{id}")
    public void delete(@PathVariable Long id){
        metricRepository.deleteById(id);
    }
}
