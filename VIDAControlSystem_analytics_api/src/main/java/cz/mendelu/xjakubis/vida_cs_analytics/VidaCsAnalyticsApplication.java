package cz.mendelu.xjakubis.vida_cs_analytics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {
        "cz.mendelu.xjakubis.vida_cs_analytics_common.model",
        "cz.mendelu.xjakubis.vida_cs_parlament_common.model"
})
public class VidaCsAnalyticsApplication {

    public static void main(String[] args) {
        SpringApplication.run(VidaCsAnalyticsApplication.class, args);
    }
}
