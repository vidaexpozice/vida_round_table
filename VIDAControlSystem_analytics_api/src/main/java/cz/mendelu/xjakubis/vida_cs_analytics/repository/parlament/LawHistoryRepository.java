package cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LawHistoryRepository extends CrudRepository<LawHistory, Long> {

    @Query(nativeQuery = true,
            value = "SELECT sum(IF( result = 1,1,0)) AS approved, sum(IF( result = 0,1,0)) AS rejected, COUNT(*) FROM law_history AS h WHERE law_id = ?1")
    List<Object[]> findAllLawHistoryByLawId(int lawId);
}

