package cz.mendelu.xjakubis.vida_cs_analytics.controller;

import cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament.LawHistoryRepository;
import cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament.LawRepository;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawHistory;
import cz.mendelu.xjakubis.vida_cs_parlament_common.utils.LawsLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/law")
public class LawController {

    @Autowired
    private LawRepository lawRepository;

    @Autowired
    private LawHistoryRepository lawHistoryRepository;

    @Transactional
    @PostMapping(path = "/addAll")
    @ResponseBody
    public String addAll(@RequestBody List<Law> laws){
        lawRepository. saveAll(laws);

        return "Response : Successfully added";
    }

    @Transactional
    @PostMapping(path = "/add")
    @ResponseBody
    public String add(@RequestBody Law law){
        lawRepository.save(law);

        return "Response : Successfully added";
    }

    @Transactional
    @PostMapping(path = "/history/addAll")
    @ResponseBody
    public String addAllLawHistory(@RequestBody List<LawHistory> lawsHistory){
        lawHistoryRepository.saveAll(lawsHistory);

        return "Response : Successfully added";
    }

    @Transactional
    @PostMapping(path = "/history/add")
    @ResponseBody
    public String addLawHistory(@RequestBody LawHistory lawHistory){
        lawHistoryRepository.save(lawHistory);

        return "Response : Successfully added";
    }

    @EventListener(ApplicationReadyEvent.class)
    public void insertOrUpdateLawRecords(){
        try {
            ArrayList<Law> laws = new LawsLoader().getLaws();
            lawRepository.saveAll(laws);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
