package cz.mendelu.xjakubis.vida_cs_analytics.controller;

import cz.mendelu.xjakubis.vida_cs_analytics.repository.GamePoolRepository;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.GamePool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/gamePool")
public class GamePoolController {

    @Autowired
    private GamePoolRepository gamePoolRepository;

    @PostMapping(path = "/save")
    @ResponseBody
    public GamePool save(@RequestBody GamePool gamePool) {
        return gamePoolRepository.save(gamePool);
    }
}
