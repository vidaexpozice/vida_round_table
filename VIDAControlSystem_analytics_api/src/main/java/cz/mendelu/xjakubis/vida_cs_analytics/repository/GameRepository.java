package cz.mendelu.xjakubis.vida_cs_analytics.repository;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Game;
import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Integer> {
}
