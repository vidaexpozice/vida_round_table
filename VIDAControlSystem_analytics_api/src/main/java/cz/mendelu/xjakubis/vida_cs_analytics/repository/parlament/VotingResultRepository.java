package cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.VotingResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotingResultRepository extends CrudRepository<VotingResult, Long> {

    @Query(nativeQuery = true,
            value = "SELECT sum(IF( result = 1,1,0)) AS pro, sum(IF( result = 0,1,0)) AS against, min(create_date_time), max(create_date_time) " +
                    "FROM voting_result AS v WHERE v.law_id = ?1")
    List<Object[]> findAllResultsByLawId(int lawId);
}
