package cz.mendelu.xjakubis.vida_cs_analytics.repository.parlament;


import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import org.springframework.data.repository.CrudRepository;

public interface LawRepository extends CrudRepository<Law, Integer> {}
