package cz.mendelu.xjakubis.vida_cs_analytics.repository;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Long> {
}
