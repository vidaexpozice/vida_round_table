package cz.mendelu.xjakubis.vida_cs_analytics.repository;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Metric;
import org.springframework.data.repository.CrudRepository;

public interface MetricRepository extends CrudRepository<Metric, Long> {
}
