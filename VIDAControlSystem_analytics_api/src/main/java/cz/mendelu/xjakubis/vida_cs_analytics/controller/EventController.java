package cz.mendelu.xjakubis.vida_cs_analytics.controller;

import cz.mendelu.xjakubis.vida_cs_analytics.repository.EventRepository;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/event")
public class EventController {

    @Autowired
    EventRepository eventRepository;

    @Transactional
    @PostMapping(path = "/add")
    @ResponseBody
    public Event add(@RequestBody Event event) {
        return eventRepository.save(event);
    }
}
