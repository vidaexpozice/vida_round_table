package cz.mendelu.xjakubis.vida_cs_analytics.controller;

import cz.mendelu.xjakubis.vida_cs_analytics.repository.GameRepository;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Game;
import cz.mendelu.xjakubis.vida_cs_analytics_common.loader.GameLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/game")
public class GameController {

    @Autowired
    private GameRepository gameRepository;

    @PersistenceContext
    private EntityManager em;

    @PostMapping(path = "/addAll")
    @ResponseBody
    public String addAll(@RequestBody List<Game> game){
        gameRepository.saveAll(game);

        return "Response : Successfully added";
    }

    @PostMapping(path = "/add")
    @ResponseBody
    public String add(@RequestBody Game game){
        gameRepository.save(game);

        return "Response : Successfully added";
    }

    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void insertOrUpdateGameRecords(){
        try {
            ArrayList<Game> games = new GameLoader().getGames();
            for(Game game: games){
                Optional<Game> record = gameRepository.findById(game.getGameIndex());

                if (record.isPresent()){
                    Game storedGame = record.get();
                    storedGame.setDescription(game.getDescription());
                    storedGame.setName(game.getName());
                    storedGame.setNumberOfPlayers(game.getNumberOfPlayers());
                    storedGame.setRunCommandClient(game.getRunCommandClient());
                    storedGame.setRunCommandServer(game.getRunCommandServer());
                    storedGame.setUsesProjector(game.isUsesProjector());
                    storedGame.setScreenPath(game.getScreenPath());
                    em.merge(storedGame);
                } else {
                    gameRepository.save(game);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
