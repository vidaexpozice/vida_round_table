package cz.mendelu.xjakubis.vida_cs_analytics.repository;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.GamePool;
import org.springframework.data.repository.CrudRepository;

public interface GamePoolRepository extends CrudRepository<GamePool, Long> {
}
