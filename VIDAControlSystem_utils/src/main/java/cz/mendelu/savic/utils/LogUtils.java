package cz.mendelu.savic.utils;

import org.apache.log4j.Logger;

/**
 * Created by david on 15.04.2017.
 */
public class LogUtils {
    private static String getIdentifier(Object classObject) {
        return "#" + String.valueOf(classObject.hashCode()).substring(0, 4) + ": ";
    }

    public static void logMethod(Object classObject, String methodName, Object... parameters) {
        String formatString = getIdentifier(classObject) + methodName + "(";

        for (Object parameter : parameters) {
            formatString += "%s, ";
        }

        formatString = ((parameters.length > 0) ?
                formatString.substring(0, formatString.length() - 2) :
                formatString) + ")";

        Logger.getLogger(classObject.getClass()).debug(String.format(formatString, parameters));
    }

    public static void logMethodEnd(Object classObject, String methodName) {
        Logger.getLogger(classObject.getClass()).debug(getIdentifier(classObject) + methodName + " finished -----------------");
    }

    public static void logException(Object classObject, Exception e) {
        Logger.getLogger(classObject.getClass()).error(getIdentifier(classObject) + "Exception:", e);
    }

    public static void logMessage(Object classObject, String message) {
        Logger.getLogger(classObject.getClass()).debug(getIdentifier(classObject) + "Message: " + message);
    }
}
