package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_client.TextHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextFlow;

import java.util.List;
import java.util.stream.Collectors;

public class ExtendedStartWindowController extends StartWindowController {

    public StackPane forBox;
    public StackPane againstBox;
    public StackPane unknownBox;
    public TextFlow unknownTextFlow;
    public TextFlow againstTextFlow;
    public TextFlow forTextFlow;
    public Tab unknownLabel;
    public Tab againstLabel;
    public Tab forLabel;
    public TabPane tabPane;

    public Label opinionsInfoLabel;
    public StackPane infoContainer;

    @FXML
    private InfoContainerController infoContainerController;

    public void init(ClientGame game) {
        super.init(game, 190);
        infoContainer.setVisible(false);
        infoContainer.managedProperty().bind(infoContainer.visibleProperty());
        opinionsInfoLabel.setText(I18N.get("label.opinionsInfo"));
        opinionsInfoLabel.setVisible(true);

        List<Law> filteredLaws = getLawForOpinionTable();
        fillOpinions(filteredLaws);
        if(filteredLaws.size() == 0){
            infoContainerController.init(I18N.get("label.allLawsVoted"));
            infoContainer.setVisible(true);
            opinionsInfoLabel.setVisible(false);
            lawContainer.setPrefHeight(600);
        }
    }

    private List<Law> getLawForOpinionTable() {
        return game.getLaws().stream()
                .filter(l -> l.getLawState() == LawState.Unknown
                        && !l.getName_cz().equals(game.getClientLaw().getName_cz())
                        && !l.getName_cz().equals(game.getClientLaw().getName_cz()))
                .collect(Collectors.toList());
    }

    private void fillOpinions(List<Law> laws){
        for (Law law: laws) {
            String localizedLawName = law.getName(I18N.getLocale());
            switch (law.getOpinion()){
                case Approved:
                    addLawToOpinionBox(forTextFlow, localizedLawName);
                    break;
                case Rejected:
                    addLawToOpinionBox(againstTextFlow, localizedLawName);
                    break;
                case Unknown:
                    addLawToOpinionBox(unknownTextFlow, localizedLawName);
                    break;
            }
        }
        setLabels();
        hideEmptyOpinionTabs();
    }

    private void hideEmptyOpinionTabs(){
        if(forTextFlow.getChildren().isEmpty()){
            tabPane.getTabs().remove(forLabel);
        }

        if(againstTextFlow.getChildren().isEmpty()){
            tabPane.getTabs().remove(againstLabel);
        }

        if(unknownTextFlow.getChildren().isEmpty()){
            tabPane.getTabs().remove(unknownLabel);
        }
    };

    private void addLawToOpinionBox(TextFlow textFlow, String lawName){
        TextHelper.addText(textFlow, "• "+ lawName, "text-description");
        TextHelper.addText(textFlow, System.lineSeparator());
    }

    private void setLabels(){
        forLabel.setText(I18N.get("column.for"));
        againstLabel.setText(I18N.get("column.against"));
        unknownLabel.setText(I18N.get("column.unknown"));
    }
}
