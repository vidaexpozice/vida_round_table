package cz.mendelu.xjakubis.vida_cs_parlament_client;

import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_parlament_client.controller.Client;
import cz.mendelu.xjakubis.vida_cs_parlament_client.controller.ClientWindowController;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;

public class MainLoader extends Application {
    private static String ipAddress;
    private static int port;
    private static boolean argumentationGameType;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setFullScreenExitHint("");
        primaryStage.setFullScreen(true);
        FXMLLoader clientWindowLoader = new FXMLLoader(
                getClass().getResource(ResourcesHelper.VIEW_PATH + "ClientWindow.fxml"));

        Parent clientWindowRoot = clientWindowLoader.load();

        ClientWindowController clientWindowController = clientWindowLoader.getController();

        Client client = new Client(clientWindowController);
        client.connect(ipAddress, port);
        if (!client.isConnected()) {
            System.out.println("Client couldn't connect to IP address " + ipAddress +
                    " and port " + port);
            Platform.exit();
            System.exit(0);
        }

        I18N.setLocale(new Locale("cz"));

        primaryStage.setScene(new Scene(clientWindowRoot));
        primaryStage.setTitle("Parlament");
        primaryStage.setMinWidth(1024);
        primaryStage.setMinHeight(768);
        primaryStage.setOnCloseRequest(event1 -> {
            Platform.exit();
            System.exit(0);
        });

        primaryStage.show();
        clientWindowController.init(primaryStage.getScene());
        setFocus(clientWindowRoot);
    }

    public void setFocus(Parent parent) {
        parent.requestFocus();
    }

    public static void main(String[] args) {
        if (args.length < 3) {
            return;
        }

        argumentationGameType = Boolean.parseBoolean(args[0]);
        port = Integer.parseInt(args[1]);
        ipAddress = args[2];

        if (args.length > 4) {
            int gameId = Integer.parseInt(args[3]);
            long gamePoolId = Long.parseLong(args[4]);
            GameAnalytics.initialize(gameId, gamePoolId);
        }

        launch(args);
    }

    public static boolean isArgumentationGameType() {
        return argumentationGameType;
    }
}
