package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;

public class LiteStartWindowController extends StartWindowController{

    public void init(ClientGame game){
        super.init(game, 320);
    }
}
