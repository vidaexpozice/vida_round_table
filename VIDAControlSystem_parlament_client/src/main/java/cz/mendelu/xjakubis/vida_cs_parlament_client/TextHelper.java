package cz.mendelu.xjakubis.vida_cs_parlament_client;

import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class TextHelper {

    public static Text addText(TextFlow textFlow, String content, Font font) {
        Text text = addText(textFlow, content);
        text.setFont(font);
        return text;
    }

    public static Text addText(TextFlow textFlow, String content, String styleClass) {
        Text text = addText(textFlow, content);
        text.getStyleClass().add(styleClass);
        return text;
    }

    public static Text addText(TextFlow textFlow, String content, String... styleClasses) {
        Text text = addText(textFlow, content);
        for (String styleClass : styleClasses) {
            text.getStyleClass().add(styleClass);
        }

        return text;
    }

    public static Text addText(TextFlow textFlow, String content ) {
        Text text = new Text(content);
        textFlow.getChildren().add(text);
        return text;
    }
}
