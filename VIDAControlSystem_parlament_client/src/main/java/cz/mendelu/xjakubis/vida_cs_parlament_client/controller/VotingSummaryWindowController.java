package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_client.ResourcesHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_client.table.CustomTableColumn;
import cz.mendelu.xjakubis.vida_cs_parlament_client.table.CustomTableView;
import cz.mendelu.xjakubis.vida_cs_parlament_client.table.DataGenerator;
import cz.mendelu.xjakubis.vida_cs_parlament_client.table.ImageTableCell;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Summary;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.util.List;

public class VotingSummaryWindowController {

    public VBox rootPane;
    public Label scoreLabel;
    public Label pointsLabel;
    public Label rankLabel;
    public VBox rankContainer;
    public Label rankTitleLabel;
    private ClientGame clientGame;
    private CustomTableView customTableView;

    public void init(ClientGame clientGame, Summary summary, double high, double width) {
        this.clientGame = clientGame;
        rootPane.getChildren().remove(customTableView);
        rootPane.setPrefSize(width,high);
        customTableView = createTable(summary.getLaws());
        rootPane.getChildren().add(customTableView);
        pointsLabel.setText(String.format("%d", summary.getPoints()));
        scoreLabel.setText(I18N.get("label.score"));
        rankContainer.managedProperty().bind(rankContainer.visibleProperty());
        rankTitleLabel.setText(I18N.get("label.rankTitle"));
        if (summary.getRank()>0) {
            setRankLabel(summary.getRank());
        }
    }

    private void setRankLabel(int rank) {
        rankContainer.setVisible(true);
        rankLabel.setText(rank + ".");
    }

    private CustomTableView<ObservableList<String>> createTable(List<Law> laws) {
        DataGenerator dataGenerator = new DataGenerator();
        CustomTableView tableView = new CustomTableView<>();
        // add columns
        List<String> columnNames = dataGenerator.getColumnNames(laws.get(0).getVotingResultList(),
                                                                    this.clientGame.getClientIds());

        for (int i = 0; i < columnNames.size(); i++) {
            String columnName = columnNames.get(i);
            if (columnName.equals(I18N.get("column.law"))) {
               createStringColumn(tableView, columnName, i);
            } else {
                createImageColumn(tableView, columnName, i);
            }
        }

        // add data
        for (Law law : laws) {
            tableView.getItems().add(
                    FXCollections.observableArrayList(
                            dataGenerator.getData(law, clientGame.getClientIds())
                    )
            );
        }
        tableView.fitContent();

        return tableView;
    }

    private void createImageColumn(CustomTableView tableView, String columnName, int finalIdx){
        CustomTableColumn<ObservableList<LawState>, ImageView> column = new CustomTableColumn<>(
                columnName
        );

        final Image falseImage = new Image(ResourcesHelper.CROSSMARK_ICON);
        final Image trueImage = new Image(ResourcesHelper.CHECKMARK_ICON);
        final Image unknownImage = new Image(ResourcesHelper.QUESTIONMARK_ICON);

        column.setCellValueFactory(c -> new SimpleObjectProperty(c.getValue().get(finalIdx)));

        column.setCellFactory(c -> new ImageTableCell(trueImage, falseImage, unknownImage));
        column.setMaxWidth(150);

        if (columnName.equals(I18N.get("column.result"))) {
            column.setMinWidth(110);
        }

        tableView.getColumns().add(column);
    }

    private void createStringColumn(CustomTableView tableView, String columnName, int finalIdx){
        CustomTableColumn<ObservableList<String>, String> column = new CustomTableColumn<>(
                columnName
        );
        column.setCellValueFactory(param ->
                new ReadOnlyObjectWrapper<>(param.getValue().get(finalIdx))
        );
        column.setMinWidth(250);
        column.setPrefWidth(250);
        tableView.getColumns().add(column);
    }
}

