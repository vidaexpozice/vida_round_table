package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.api_model.VotingResultsStatistic;
import cz.mendelu.xjakubis.vida_cs_parlament_client.TextHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_client.statistics.CustomChart;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class StatisticsController {

    public PieChart pieChart;
    public Label successByVotersLabel;
    public TextFlow successByVotersTextFlow;
    public Label totalSuccessLabel;
    public TextFlow totalSuccessTextFlow;
    public Label lawLabel;
    public Label lawNameLabel;

    public void init(VotingResultsStatistic votingResultsStatistic, Law myLaw, Client client)  {
        try {
            CustomChart.setPieChart(pieChart, votingResultsStatistic);
            initTextFields(votingResultsStatistic, myLaw);
        } catch (Exception e) {
            e.printStackTrace();
            client.close();
        }
    }

    private void initTextFields(VotingResultsStatistic votingResultsStatistic, Law law) {
        lawLabel.setText(I18N.get("column.law"));
        lawNameLabel.setText(law.getName(I18N.getLocale()));
        totalSuccessTextFlow.setLineSpacing(5);
        successByVotersTextFlow.setLineSpacing(5);
        fillSuccessByVoter(votingResultsStatistic);
        fillTotalSuccess(votingResultsStatistic);
    }

    private void fillSuccessByVoter(VotingResultsStatistic votingResultsStatistic){
        successByVotersLabel.setText(I18N.get("label.successByVoters"));
        long totalVoters = votingResultsStatistic.getProCount() + votingResultsStatistic.getAgainstCount();
        addStatistic("text.votersTotal", totalVoters, successByVotersTextFlow, true);
        addStatistic("text.votedPro",votingResultsStatistic.getProCount(), successByVotersTextFlow, true );
        addStatistic("text.votedAgainst",votingResultsStatistic.getAgainstCount(), successByVotersTextFlow, false);
    }

    private void fillTotalSuccess(VotingResultsStatistic votingResultsStatistic){
        totalSuccessLabel.setText(I18N.get("label.totalSuccess"));
        long totalVotes = votingResultsStatistic.getRejectedCount() + votingResultsStatistic.getApprovedCount();
        addStatistic("text.votedTotal",totalVotes, totalSuccessTextFlow, true);
        addStatistic("text.approved",votingResultsStatistic.getApprovedCount(), totalSuccessTextFlow, true);
        addStatistic("text.rejected",votingResultsStatistic.getRejectedCount(), totalSuccessTextFlow, false);
    }

    private void addStatistic(String localizedLabelKey, Object value, TextFlow textFlow, boolean newLine){
        TextHelper.addText(textFlow, I18N.get(localizedLabelKey), "card-content");
        TextHelper.addText(textFlow, String.valueOf(value), "card-content" );

        if(newLine) {
            textFlow.getChildren().add(new Text(System.lineSeparator()));
        }
    }
}
