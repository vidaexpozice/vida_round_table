package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_client.MainLoader;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Message;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Summary;
import javafx.application.Platform;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

/**
 * <p>Client for communicating with server and sending following message signals:</p>
 *
 * <p><b>Client -> Server:</b><br>
 *   <ul>
 *   <li>LANGUAGE C - Client is ready for game and its localization has been set.</li>
 *   <li>VOTING_RESULT R - I voted R.</li>GET_ALL_LAWS
 *   <li>GET_ALL_LAWS - send me all laws in this game</li>
 *   <li>START_VOTING L - start voting on my law L</li>
 *   </ul>
 * </p>
 *
 * <p><b>Server -> Client:</b>
 *   <ul>
 *   <li>INIT_GAME - Init laws, gamer identifier, opinions</li>
 *   <li>COUNTDOWN_LANGUAGE - Client, you have T seconds to choose language (default language: Czech)</li>
 *   <li>COUNTDOWN - Client you have T seconds to look at the current information</li>
 *   <li>VOTE - Show voting window</li>
 *   <li>END_VOTING - Close voting and return to the start window</li>
 *   <li>ALL_LAWS - Client there are all laws with the current state</li>
 *   <li>END_GAME - Client start end game sequence</li>
 *   <li>STATISTICS - Show statistics about your law</li>
 *   <li>SEND_ME_LANGUAGE - Client send me selected language</li>
 *   <li>START_WINDOW - Client go to start window</li>
 *   </ul>
 * </p>
 *
 * <p><b>Where:</b>
 *   <ul>
 *   <li>T is a text, usually number of seconds to countdown or message
 *      (used for countdown = 0 - e.g. Start!),</li>
 *   <li>L is Law</li>
 *   <li>R is instance of VotingResult containing selected option in voting</li>
 *   <li>C is language represented by class Locale</li> *
 *   </ul>
 * </p>
 *
 * <p><b>Sequence to start the game:</b><br>
 *   COUNTDOWN_LANGUAGE -> SEND_ME_LANGUAGE -> LANGUAGE -> INIT_GAME
 * </p>
 */

public class Client extends Thread {
    private final ClientWindowController clientWindowController;
    private Socket socket;
    private ObjectInputStream objectInput;
    private ObjectOutputStream objectOutput;
    private boolean connected = false;

    public Client(ClientWindowController clientWindowController) {
        this.clientWindowController = clientWindowController;
    }

    /**
     * Method for connection to the server.
     *
     * @param ipAddress server IP address
     * @return whether connection was successfully established
     */
    public boolean connect(String ipAddress, int port) {
        if (ipAddress == null) {
            return false;
        }

        try {
            socket = new Socket(ipAddress, port);
            socket.setKeepAlive(true);
            socket.setSoTimeout(Integer.MAX_VALUE);
            InputStream stream = socket.getInputStream();
            objectInput = new ObjectInputStream(stream);
            objectOutput = new ObjectOutputStream(socket.getOutputStream());
            start();
            connected = true;

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isConnected() {
        return connected;
    }

    /**
     * Overridden run method receiving messages from the server.
     */
    @Override
    public void run() {
        Message response;

        while (true) {
            try {
                Object object = objectInput.readObject();
                if(object instanceof Message)
                {
                    response = (Message)object;
                } else {
                    continue;
                }

                switch (response.message) {
                    case "INIT_GAME":
                        clientWindowController.initGame((ClientGame) response.object, this);
                        publish("GAME_TYPE", MainLoader.isArgumentationGameType());
                        break;
                    case "COUNTDOWN_LANGUAGE":
                    case "COUNTDOWN":
                        clientWindowController.updateCountdownLabel((int) response.object);
                        break;
                    case "VOTE":
                        clientWindowController.initVoteWindowController((Law) response.object);
                        break;
                    case "END_VOTING":
                        clientWindowController.closeVoting((Law) response.object);
                        break;
                    case "ALL_LAWS":
                        clientWindowController.openVotingSummary((Summary) response.object);
                        break;
                    case "END_GAME":
                        clientWindowController.showEndGameWindow((Summary) response.object);
                        break;
                    case "STATISTICS":
                        clientWindowController.showStatisticsWindow();
                        break;
                    case "SEND_ME_LANGUAGE":
                        publish("LANGUAGE", I18N.getLocale());
                        break;
                    case "START_WINDOW":
                        clientWindowController.initStartWindowController();
                        break;
                }
            } catch (SocketException e) {
                // server disconnected
                close();
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void publish(String actionName, Object object) {
        publish(new Message(actionName, object));
    }

    public void publish(String actionName) {
        publish(new Message(actionName));
    }

    public void publish(Message message) {
        Thread thread = new Thread(() -> {
            try {
                objectOutput.writeObject(message);
                objectOutput.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
       thread.start();
    }

    public void close(){
        try {
            socket.close();
            Platform.exit();
            System.exit(0);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
