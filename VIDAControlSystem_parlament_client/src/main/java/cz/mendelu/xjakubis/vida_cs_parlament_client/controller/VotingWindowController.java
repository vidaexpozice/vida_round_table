package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.VotingResult;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;

import java.time.LocalDateTime;

public class VotingWindowController {

    public Button proButton;
    public Button againstButton;
    public StackPane lawContainer;

    @FXML
    public LawContainerController lawContainerController;

    private Law law;
    private Client client;
    private Character id;

    public void init(Law law, Client client, Character id) {
        this.law = law;
        this.client = client;
        this.id = id;
        this.proButton.setText(I18N.get("column.for"));
        this.againstButton.setText(I18N.get("column.against"));
        lawContainerController.init(law.getName(I18N.getLocale()),
                law.getDescription(I18N.getLocale()));
    }

    public void handleAgainstButton() {
        disableVotingButtons();
        GameAnalytics.newEvent("votingResult", "button", "against");
        client.publish("VOTING_RESULT",
                new VotingResult(law, id, LawState.Rejected, LocalDateTime.now()));
    }

    public void handleForButton() {
        disableVotingButtons();
        GameAnalytics.newEvent("votingResult", "button", "for");
        client.publish("VOTING_RESULT",
                new VotingResult(law, id, LawState.Approved, LocalDateTime.now()));
    }

    private void disableVotingButtons(){
        proButton.setDisable(true);
        againstButton.setDisable(true);
    }
}
