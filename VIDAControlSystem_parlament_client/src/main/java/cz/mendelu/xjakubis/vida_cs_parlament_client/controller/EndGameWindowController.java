package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Summary;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;

public class EndGameWindowController {

    public StackPane votingSummaryWindow;
    public StackPane infoContainer;

    @FXML
    private VotingSummaryWindowController votingSummaryWindowController;
    @FXML
    private InfoContainerController infoContainerController;

    public void init(ClientGame game, Summary endGameSummary) {
        GameAnalytics.newEvent("gameState", "application", "endGame");
        votingSummaryWindowController.init(game, endGameSummary,300, 500);
        infoContainerController.init(I18N.get("text.endGameInfo"));
    }
}
