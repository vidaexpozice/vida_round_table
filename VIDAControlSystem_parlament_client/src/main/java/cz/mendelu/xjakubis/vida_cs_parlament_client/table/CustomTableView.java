package cz.mendelu.xjakubis.vida_cs_parlament_client.table;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.TableView;

public class CustomTableView<S> extends TableView<S> {

    private final int maxTableHeight = 500;

    public CustomTableView() {
        super();
        this.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this.setEditable(false);
    }

    public void fitContent() {
        this.setFixedCellSize(40);
        DoubleExpression prefHeightPropertyBinding = this.fixedCellSizeProperty().multiply(Bindings.size(this.getItems()).add(1.7));
        int prefHeight = prefHeightPropertyBinding.intValue();

        if (prefHeight > maxTableHeight) {
            this.prefHeightProperty().bind(new SimpleIntegerProperty(maxTableHeight).asObject());
        }
        else {
            this.prefHeightProperty().bind(prefHeightPropertyBinding);
        }
        this.minHeightProperty().bind(this.prefHeightProperty());
        this.maxHeightProperty().bind(this.prefHeightProperty());
    }
}

