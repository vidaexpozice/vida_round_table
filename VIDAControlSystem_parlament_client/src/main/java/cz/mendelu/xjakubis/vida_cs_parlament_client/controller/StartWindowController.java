package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

public class StartWindowController {

    public ClientGame game;
    @FXML
    public LawContainerController lawContainerController;
    public StackPane lawContainer;

    public void init(ClientGame game) {}

    public void init(ClientGame game, int identifierFontSize) {
        this.game = game;
        Law clientLaw = game.getClientLaw();
        lawContainerController.init(clientLaw.getName(I18N.getLocale()),
                clientLaw.getDescription(I18N.getLocale()),
                game.getClientId(),
                identifierFontSize );
    }
}
