package cz.mendelu.xjakubis.vida_cs_parlament_client.table;

import javafx.scene.control.TableColumn;

public class CustomTableColumn<S, T> extends TableColumn<S, T> {

    public CustomTableColumn() {
        super();
        this.setSortable(false);
        this.setMinWidth(50);
    }

    public CustomTableColumn(String s) {
        super(s);
        this.setSortable(false);
        this.setMinWidth(60);
    }
}
