package cz.mendelu.xjakubis.vida_cs_parlament_client;

public class ResourcesHelper {
    public static final String RESOURCES_PATH = "/cz/mendelu/xjakubis/vida_cs_parlament_client/";
    public static final String IMAGES_PATH = RESOURCES_PATH +"images/";
    public static final String VIEW_PATH = RESOURCES_PATH +"view/";
    public static final String CHECKMARK_ICON = IMAGES_PATH + "checkmark_36dp.png";
    public static final String CROSSMARK_ICON = IMAGES_PATH + "crossmark_36dp.png";
    public static final String QUESTIONMARK_ICON = IMAGES_PATH + "questionmark_36dp.png";
    public static final String CZECH_FLAG = IMAGES_PATH + "czech-republic.png";
    public static final String ENGLISH_FLAG = IMAGES_PATH + "united-kingdom.png";
    public static final String INFO_ICON = IMAGES_PATH + "info_icon.png";
    public static final String SPEAKER = IMAGES_PATH + "speaker.png";
    public static final String CHECKMARK_ICON_255 = IMAGES_PATH + "checkmark_255dp.png";
    public static final String CROSSMARK_ICON_255 = IMAGES_PATH + "crossmark_255dp.png";
}
