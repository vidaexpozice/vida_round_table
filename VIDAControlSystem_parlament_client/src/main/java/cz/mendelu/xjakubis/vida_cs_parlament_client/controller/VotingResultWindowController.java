package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_client.ResourcesHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_client.TextHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.TextFlow;

public class VotingResultWindowController {

    @FXML
    public TextFlow resultsTextFlow;
    @FXML
    public TextFlow approvedTextFlow;
    @FXML
    public TextFlow rejectedTextFlow;

    public Label forLabel;
    public Label againstLabel;
    public ImageView imageView;

    public void init(Law law) {
        setViewByResult(law.getLawState());
        int approvedNumber = law.getNumberOfVotingResultsByType(LawState.Approved);
        int rejectedNumber = law.getNumberOfVotingResultsByType(LawState.Rejected);

        int votingResultsCount = law.getVotingResultList().size();
        String approvedText = approvedNumber + " / " + votingResultsCount;
        String rejectedText = rejectedNumber + " / " + votingResultsCount;

        forLabel.setText(I18N.get("label.for"));
        againstLabel.setText(I18N.get("label.against"));
        TextHelper.addText(approvedTextFlow, approvedText,"text-description", "status-title");
        TextHelper.addText(rejectedTextFlow, rejectedText, "text-description", "status-title");
    }

    private void setViewByResult(LawState state){
        String resultDescription;
        Image image;
        if(state == LawState.Approved){
            resultDescription =  I18N.get("label.lawApproved");
            image = new Image(ResourcesHelper.CHECKMARK_ICON_255);
        }else{
            resultDescription = I18N.get("label.lawNotApproved");
            image = new Image(ResourcesHelper.CROSSMARK_ICON_255);
        }

        imageView.setImage(image);
        TextHelper.addText(resultsTextFlow, resultDescription,"text-description", "status-title");
    }
}
