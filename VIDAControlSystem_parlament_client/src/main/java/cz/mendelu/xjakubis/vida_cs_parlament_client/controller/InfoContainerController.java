package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import javafx.scene.control.Label;

public class InfoContainerController {
    public Label infoMessageLabel;

    public void init(String infoMessage){
        infoMessageLabel.setText(infoMessage);
    };
}
