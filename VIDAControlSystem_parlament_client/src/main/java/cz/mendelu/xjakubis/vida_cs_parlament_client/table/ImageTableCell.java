package cz.mendelu.xjakubis.vida_cs_parlament_client.table;

import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageTableCell<T> extends TableCell<T, LawState> {
    private final ImageView imageView;
    private final Image approvedImage;
    private final Image rejectedImage;
    private final Image unknownImage;

    public ImageTableCell(Image trueImage, Image falseImage, Image unknownImage) {
        imageView = new ImageView();

        setGraphic(imageView);
        this.approvedImage = trueImage;
        this.rejectedImage = falseImage;
        this.unknownImage = unknownImage;
    }

    @Override
    protected void updateItem(LawState item, boolean empty) {
        super.updateItem(item, empty);
        Image image = unknownImage;
        if (empty || item == null) {
            imageView.setImage(null);
        } else {
            switch (item){
                case Approved: image = approvedImage;
                    break;
                case Rejected: image = rejectedImage;
                    break;
            }
            imageView.setImage(image);
            imageView.setFitHeight(28);
            imageView.setFitWidth(28);
        }
    }
}
