package cz.mendelu.xjakubis.vida_cs_parlament_client.table;

import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.VotingResult;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class DataGenerator {

    public List<Object> getData(Law law, List<Character> clientIds) {
        List<Object> data = new ArrayList<>();
        List<VotingResult> orderedResults = new ArrayList<>(law.getVotingResultList());
        data.add(law.getName(I18N.getLocale()));

        if (law.getVotingResultList().size() > 0) {
            orderedResults.sort(Comparator.comparing(VotingResult:: getVoter));

            for (VotingResult res: orderedResults) {
                data.add(res.getResult());
            }
        } else {
            for (int i=0; clientIds.size() > i; i++) {
                data.add(LawState.Unknown);
            }
        }

        data.add(law.getLawState());
        return data;
    }

    public List<String> getColumnNames(List<VotingResult> results, List<Character> clientIds){
        List<String> columnNames = new ArrayList<>();
        columnNames.add(I18N.get("column.law"));

        if(!results.isEmpty()) {
            List<VotingResult> orderedResults = new ArrayList<>(results);
            orderedResults.sort(Comparator.comparing(VotingResult::getVoter));

            for (VotingResult res : orderedResults) {
                columnNames.add(res.getVoter().toString());
            }
        } else {
            for (Character clientId: clientIds) {
                columnNames.add(clientId.toString());
            }
        }

        columnNames.add(I18N.get("column.result"));
        return columnNames;
    }
}
