package cz.mendelu.xjakubis.vida_cs_parlament_client.statistics;

import cz.mendelu.xjakubis.vida_cs_analytics_common.api_model.VotingResultsStatistic;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;

public class CustomChart {

    public static void setPieChart(PieChart pieChart, VotingResultsStatistic votingStatistics) {
        ObservableList<PieChart.Data> pieChartData = getPieChartData(votingStatistics);
        pieChart.setData(pieChartData);
        pieChart.setLegendVisible(false);
        applyCustomColorSequence(pieChart.getData());
    }

    private static ObservableList<PieChart.Data> getPieChartData(VotingResultsStatistic votingStatistics) {
        ObservableList<PieChart.Data> pieChartData = null;

        long proCount = votingStatistics.getProCount();
        long againstCount = votingStatistics.getAgainstCount();

        pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data(I18N.get("label.for"), proCount),
                        new PieChart.Data(I18N.get("label.against"), againstCount));

        pieChartData = pieChartData.filtered(data -> data.getPieValue() > 0);
        return pieChartData;
    }

    private static void applyCustomColorSequence(ObservableList<PieChart.Data> pieChartData) {
        final String localizedPro = I18N.get("label.for");
        for (PieChart.Data data : pieChartData) {
            Node node = data.getNode();
            String color;

            if (data.getName().equals(localizedPro)) {
                color = "green";
            } else {
                color = "red";
            }

            node.setStyle("-fx-pie-color: " + color  + ";");
        }
    }
}
