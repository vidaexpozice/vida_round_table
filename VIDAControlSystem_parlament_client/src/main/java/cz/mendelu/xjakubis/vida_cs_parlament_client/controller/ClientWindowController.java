package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.api_model.VotingResultsStatistic;
import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_parlament_client.MainLoader;
import cz.mendelu.xjakubis.vida_cs_parlament_client.ResourcesHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.ClientGame;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Summary;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.concurrent.Future;

public class ClientWindowController {

    @FXML
    public FlowPane rootGame;
    public Label title;
    public Button votingSummaryBtn;
    public StackPane infoWindow;
    public StackPane votingSummaryWindow;
    public Button infoButton;
    public Label countdown;
    public Button voteButton;
    public VBox votingSummaryWindowContainer;
    public VBox infoWindowContainer;
    public StackPane root;

    @FXML
    private InfoWindowController infoWindowController;
    @FXML
    private VotingSummaryWindowController votingSummaryWindowController;
    @FXML
    private LanguagePickerController languagePickerController;

    private ClientGame game;
    private StartWindowController startWindowController;
    private VotingWindowController votingWindowController;
    private Client client;
    private VotingResultWindowController votingResultWindowController;
    private Future<VotingResultsStatistic> statisticFuture;

    private String startWindowResource = "LiteStartWindow.fxml";

    public void init(Scene scene) {
        Platform.runLater(() -> {
                    Stage stage = (Stage) scene.getWindow();
                    stage.getScene().setOnMouseClicked(this::handleTouchPressed);
                }
        );
    }

    public void initGame(ClientGame game, Client client) {
        this.game = game;
        this.client = client;
        startWindowResource = MainLoader.isArgumentationGameType() ? "ExtendedStartWindow.fxml": "LiteStartWindow.fxml";
        initClientWindow();
        initStartWindowController();
    }

    public void updateCountdownLabel(int num) {
        Platform.runLater(() -> {
            countdown.setText(String.valueOf(num));
        });
    }

    public void handleSummaryBtnClick() {
        if(!votingSummaryWindowContainer.isVisible()) {
            client.publish("GET_ALL_LAWS");
            GameAnalytics.newEvent("summary", "button", "votingSummary" );
        }
    }

    public void handleTouchPressed(javafx.scene.input.MouseEvent event) {
        if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
            if(infoWindowContainer.isVisible()){
                infoWindowContainer.setVisible(false);
            }
        }

        if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
            if(votingSummaryWindowContainer.isVisible()) {
                votingSummaryWindowContainer.setVisible(false);
            }
        }
    }

    public void initStartWindowController() {
        Platform.runLater(() -> {
            try {
                resetDefaultVisibilities();
                infoButton.setVisible(true);
                votingSummaryBtn.setText(I18N.get("button.votingSummary"));
                votingSummaryBtn.setVisible(true);
                FXMLLoader loader = getLoader(startWindowResource);
                Parent startWindowRoot = loader.load();
                startWindowController = loader.getController();
                startWindowController.init(this.game);
                rootGame.getChildren().add(startWindowRoot);
                voteButton.setVisible(!game.isClientLawVoted());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
    }

    public void initVoteWindowController(Law law) {
        Platform.runLater(() -> {
            try {
                resetDefaultVisibilities();
                setTitle(I18N.get("label.voting"));
                FXMLLoader loader = getLoader("VotingWindow.fxml");
                Parent voteWindowRoot = loader.load();
                votingWindowController = loader.getController();
                votingWindowController.init(law, client, game.getClientId());
                rootGame.getChildren().add(voteWindowRoot);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
    }

    private void showVotingResults(Law law) {
        Platform.runLater(() -> {
            try {
                resetDefaultVisibilities();
                countdown.setVisible(true);
                FXMLLoader loader = getLoader("VotingResultWindow.fxml");
                Parent voteWindowRoot = loader.load();
                votingResultWindowController = loader.getController();
                votingResultWindowController.init(law);
                rootGame.getChildren().add(voteWindowRoot);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void closeVoting(Law law) {
        game.getLawById(law.getLawId()).setLawState(law.getLawState());
        showVotingResults(law);
    }

    public void openVotingSummary(Summary summary) {
        Platform.runLater(() -> {
            votingSummaryWindowController.init(game, summary,400,600);
            votingSummaryWindowContainer.setVisible(true);
            infoWindowContainer.setVisible(false);
        });
    }

    public void showEndGameWindow(Summary endGameSummary) {
        Platform.runLater(() -> {
            try {
                resetDefaultVisibilities();
                root.setVisible(true);
                countdown.setVisible(true);
                FXMLLoader loader = getLoader("EndGameWindow.fxml");
                Parent windowRoot = loader.load();
                EndGameWindowController endGameWindowController = loader.getController();
                endGameWindowController.init(game, endGameSummary);
                root.getChildren().add(windowRoot);
                loadStatistics(game.getClientLaw());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showStatisticsWindow() {
        VotingResultsStatistic votingResultsStatistic = tryGetStatistics();

        if (votingResultsStatistic == null) {
            client.publish("END_GAME");
            return;
        }

        Platform.runLater(() -> {
            try {
                resetDefaultVisibilities();
                countdown.setVisible(true);
                setTitle(I18N.get("header.statistics"));

                FXMLLoader loader = getLoader("Statistics.fxml");
                Parent windowRoot = loader.load();
                StatisticsController statisticsController = loader.getController();
                statisticsController.init(votingResultsStatistic, game.getClientLaw(), client);
                rootGame.getChildren().add(windowRoot);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private FXMLLoader getLoader(String viewName) {
        return new FXMLLoader(
                getClass().getResource(ResourcesHelper.VIEW_PATH + viewName));
    }

    private void setTitle(String text) {
        title.setVisible(true);
        title.setText(text);
    }

    public void handleInfoBtnClick() {
        if (!infoWindowContainer.isVisible()) {
            infoWindowContainer.setVisible(true);
            votingSummaryWindowContainer.setVisible(false);
            GameAnalytics.newEvent("buttonClick", "button", "info" );
        }
    }

    @FXML
    public void initialize() {
        Image img = new Image(ResourcesHelper.INFO_ICON);
        ImageView englishView = new ImageView(img);
        infoButton.setGraphic(englishView);
    }

    public void handleVoteButton() {
        client.publish("START_VOTING", game.getClientLaw());
        GameAnalytics.newEvent("buttonClick", "button", "vote");
        this.game.setClientLawVoted(true);
    }

    private void resetDefaultVisibilities(){
        voteButton.setVisible(false);
        title.setVisible(false);
        infoButton.setVisible(false);
        countdown.setVisible(false);
        votingSummaryBtn.setVisible(false);
        root.setVisible(false);
        rootGame.getChildren().clear();
        root.getChildren().clear();
    }

    private void loadStatistics(Law myLaw){
        statisticFuture = GameAnalytics.getRestService()
                .at("votingResult")
                .at("get")
                .at("statistics")
                .at("law", myLaw.getId().toString())
                .get(VotingResultsStatistic.class);
    }

    private VotingResultsStatistic tryGetStatistics() {
        try{
            if(statisticFuture.isDone()){
                return statisticFuture.get();}
            else { return  null;}
        }catch(Exception e){
            return null;
        }
    }

    private void initClientWindow(){
        Platform.runLater(()->{
            infoWindowController.init();
            voteButton.setText(I18N.get("button.startVoting"));
        });
    }
}
