package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import com.sun.javafx.tk.FontMetrics;
import com.sun.javafx.tk.Toolkit;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class LawContainerController {

    public Label goalInfoLabel;
    public Label identifierLabel;
    public Label lawLabel;
    public Label lawDescriptionLabel;

    public void init(String lawName, String lawDescription, Character gamerId, int fontSize) {
        lawDescriptionLabel.setText(lawDescription);
        lawLabel.setText(lawName);
        goalInfoLabel.setText(I18N.get("label.goal"));
        goalInfoLabel.setVisible(true);

        if(gamerId != null){
            identifierLabel.setVisible(true);
            identifierLabel.setText(gamerId.toString());
            identifierLabel.setFont(new Font(fontSize));
            removeExtraIdentifierPadding();
        }
    }

    public void init(String lawName, String lawDescription) {
        lawDescriptionLabel.setText(lawDescription);
        lawLabel.setText(lawName);
        goalInfoLabel.managedProperty().bind(goalInfoLabel.visibleProperty());
    }

    private void removeExtraIdentifierPadding(){
        FontMetrics metrics = Toolkit.getToolkit().getFontLoader().getFontMetrics(identifierLabel.getFont());
        identifierLabel.setPadding(new Insets(0, 0, -metrics.getDescent()*1.5, 0));
    }
}
