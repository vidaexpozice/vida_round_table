package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;
import cz.mendelu.xjakubis.vida_cs_parlament_client.ResourcesHelper;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Locale;

public class LanguagePickerController {
    public Button englishButton;
    public Button czechButton;

    @FXML
    public void initialize() {
        Image englishFlagImg = new Image(ResourcesHelper.ENGLISH_FLAG);
        ImageView englishView = new ImageView(englishFlagImg);
        englishButton.setGraphic(englishView);

        Image czechFlagImg = new Image(ResourcesHelper.CZECH_FLAG);
        ImageView czechView = new ImageView(czechFlagImg);
        czechButton.setGraphic(czechView);
    }

    public void handleOnEnglishButtonClick() {
        I18N.setLocale(Locale.ENGLISH);
        GameAnalytics.newEvent("language", "button", "english" );
    }

    public void handleOnCzechButtonClick() {
        Locale locale = new Locale("cz");
        I18N.setLocale(locale);
        GameAnalytics.newEvent("language", "button", "czech" );
    }
}
