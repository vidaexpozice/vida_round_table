package cz.mendelu.xjakubis.vida_cs_parlament_client.controller;

import cz.mendelu.xjakubis.vida_cs_parlament_client.MainLoader;
import cz.mendelu.xjakubis.vida_cs_parlament_client.localization.I18N;
import cz.mendelu.xjakubis.vida_cs_parlament_client.TextHelper;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class InfoWindowController {
    public Label infoLabel;
    public Label subInfoLabel;
    public TextFlow votingInstructionsTextFlow;

    public void init(){
        infoLabel.setText( I18N.get("label.argumentation"));
        subInfoLabel.setText( I18N.get("label.argumentationInfo"));
        TextHelper.addText(votingInstructionsTextFlow, I18N.get("label.lawInfo"), "text-info");
        votingInstructionsTextFlow.getChildren().add(new Text(System.lineSeparator()));
        TextHelper.addText(votingInstructionsTextFlow, I18N.get("label.lawApprovedInfo"), "text-info");
        votingInstructionsTextFlow.getChildren().add(new Text(System.lineSeparator()));
        TextHelper.addText(votingInstructionsTextFlow, I18N.get("text.votingInstructionsInfo"), "text-info");
        votingInstructionsTextFlow.getChildren().add(new Text(System.lineSeparator()));
        if(MainLoader.isArgumentationGameType()) {
            TextHelper.addText(votingInstructionsTextFlow, I18N.get("text.argueScoreInfo"), "text-info");
        } else {
            TextHelper.addText(votingInstructionsTextFlow, I18N.get("text.scoreInfo"), "text-info");
        }

    }
}
