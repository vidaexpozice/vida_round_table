package cz.savic.socket_tester;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by david on 06.11.2017.
 */
public class Server extends Thread {
    private ServerSocket serverSocket;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    @Override
    public synchronized void start() {
        super.start();
        System.out.println("�ek�m a� se p�ipoj� klienti");
    }

    @Override
    public void run() {
        while (true) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
                socket.setKeepAlive(true);
                socket.setSoTimeout(Integer.MAX_VALUE);
                System.out.println("P�ijal jsem klienta na portu " + socket.getPort() +
                        " a IP adrese " + socket.getInetAddress().toString().replace("/", ""));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server(8888);
        server.start();
    }
}
