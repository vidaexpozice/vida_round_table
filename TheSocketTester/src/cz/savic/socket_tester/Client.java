package cz.savic.socket_tester;

import javafx.application.Platform;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by david on 06.11.2017.
 */
public class Client {
    private Socket socket;

    /**
     * Metoda pro p�ipojen� k serveru.
     *
     * @param ipAddress IP adresa serveru
     * @return zdali se poda�ilo p�ipojit
     */
    public boolean connect(String ipAddress, int port) {
        if (ipAddress == null) {
            return false;
        }

        try {
            socket = new Socket(ipAddress, port);
            socket.setKeepAlive(true);
            socket.setSoTimeout(Integer.MAX_VALUE);

            System.out.println("connected on ip address: " + ipAddress + ", on port: " + port);

            return true;
        } catch (Exception e) {
            System.out.println("could not connect");
            return false;
        }
    }

    public void disconnect() throws IOException {
        socket.close();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ArrayList<Client> clients = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Client client = new Client();
            client.connect("192.168.0.100", 8888);
            clients.add(client);
        }
        Thread.sleep(5000);
        clients.stream().forEach(client -> {
            try {
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
