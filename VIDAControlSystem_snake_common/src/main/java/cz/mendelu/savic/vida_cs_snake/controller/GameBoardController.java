/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.controller;

import cz.mendelu.savic.vida_cs.model.ColorHandler;
import cz.mendelu.savic.vida_cs.model.TransitionFactory;
import cz.mendelu.savic.vida_cs_snake.model.*;
import javafx.animation.ParallelTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * FXML Controller class
 *
 * @author david
 */
public class GameBoardController {

    private static final double NUMBER_OF_FIELDS = 25;

    @FXML
    private StackPane root;
    @FXML
    private Pane boardPane;
    @FXML
    private Label countdownLabel;
    @FXML
    private Label endMessageLabel;
    private ArrayList<Snake> snakes;
    private ArrayList<SnakePart> foodList;
    private double fieldSize;
    private List<Color> snakeColors;
    private ParallelTransition countdownTransition;
    // true if all snakes except one (the winner) are dead
    // (if the game was started with just one snake, then it's true when it's dead)
    private boolean allDead = false;
    private List<EndGameListener> endGameListeners = new ArrayList<>();
    private Node endDialog = null;
    private GameEndDialogController endDialogController = null;
    private String aliveColorName = null;

    public void init(Application mainLoader, int backgroundNumber, int numberOfSnakes,
                     List<Color> snakeColors, double windowSize) throws IOException {
        this.fieldSize = windowSize / NUMBER_OF_FIELDS;
        this.snakes = new ArrayList<>();
        this.boardPane.setPrefSize(windowSize, windowSize);
        this.foodList = new ArrayList<>();
        this.snakeColors = snakeColors;

        countdownLabel.setVisible(false);

        countdownTransition = TransitionFactory.createAppearingNode(countdownLabel);

        String imageString = mainLoader.getClass().getResource("/backgrounds/snake_background"
                + backgroundNumber + ".jpg").toExternalForm();
        boardPane.setStyle("-fx-background-image: url('" + imageString + "');-fx-background-size: cover;-fx-background-position: center center;-fx-border-width: 4; -fx-border-color: firebrick;");

        addSnakes(numberOfSnakes);
    }

    public void highlightSnake(int snakeIndex) {
        for (Node snakePart : snakes.get(snakeIndex).getSnakeElements()) {
            TransitionFactory.createShortTimeFadingNode(snakePart).play();
        }
    }

    public void addSnake(DirectionEnum startDirection, double x, double y, double width,
                         Color color, GameBoardController gameBoardController) throws IOException {
        Snake snake = new Snake(startDirection, x, y, width, color, gameBoardController, NUMBER_OF_FIELDS);
        boardPane.getChildren().addAll(snake.getSnakeElements());
        snakes.add(snake);
    }

    public synchronized void addSnakes(int numberOfSnakes) throws IOException {
        double halfOfBoardPosition = (int) (NUMBER_OF_FIELDS / 2);
        double quarterOfBoardPosition = (int) (NUMBER_OF_FIELDS / 4);
        boolean oddNumberOfFields = (NUMBER_OF_FIELDS % 2 == 1);

        addSnake(DirectionEnum.UP, halfOfBoardPosition + (oddNumberOfFields ? 1 : 0), NUMBER_OF_FIELDS - 5, fieldSize, snakeColors.get(0), this);
        if (numberOfSnakes >= 2) {
            addSnake(DirectionEnum.DOWN, halfOfBoardPosition - 1, 4, fieldSize, snakeColors.get(1), this);
        }
        if (numberOfSnakes >= 3) {
            addSnake(DirectionEnum.RIGHT, 4, halfOfBoardPosition + (oddNumberOfFields ? 1 : 0), fieldSize, snakeColors.get(2), this);
        }
        if (numberOfSnakes >= 4) {
            addSnake(DirectionEnum.LEFT, NUMBER_OF_FIELDS - 5, halfOfBoardPosition - 1, fieldSize, snakeColors.get(3), this);
        }
        if (numberOfSnakes >= 5) {
            addSnake(DirectionEnum.UP, NUMBER_OF_FIELDS - quarterOfBoardPosition, NUMBER_OF_FIELDS - 6, fieldSize, snakeColors.get(4), this);
        }
        if (numberOfSnakes >= 6) {
            addSnake(DirectionEnum.DOWN, quarterOfBoardPosition - 1, 5, fieldSize, snakeColors.get(5), this);
        }
        if (numberOfSnakes >= 7) {
            addSnake(DirectionEnum.RIGHT, 5, NUMBER_OF_FIELDS - quarterOfBoardPosition, fieldSize, snakeColors.get(6), this);
        }
        if (numberOfSnakes >= 8) {
            addSnake(DirectionEnum.LEFT, NUMBER_OF_FIELDS - 6, quarterOfBoardPosition - 1, fieldSize, snakeColors.get(7), this);
        }
    }

    public void setDirection(Snake snake, DirectionEnum direction) {
        snake.setDirection(direction);
    }

    public void setDirection(int snakeIndex, DirectionEnum direction) {
        snakes.get(snakeIndex).setDirection(direction);
    }

    public synchronized void moveSnakes() {
        if (allDead) {
            return;
        }

        int aliveSnakes = snakes.size();
        Snake aliveSnake = null;

        Set<Node> nodesToAdd = new HashSet<>();

        try {
            for (Snake snake : snakes) {
                if (snake.move(snakes, foodList) != MoveValidity.VALID) {
                    aliveSnakes--;
                } else {
                    aliveSnake = snake;
                }
            }

            if ((aliveSnakes <= 1 && snakes.size() > 1) || aliveSnakes == 0) {
                allDead = true;

                if (aliveSnake != null) {
                    aliveColorName = ColorNames.getColorName(aliveSnake.getColor());
                } else {
                    aliveColorName = null;
                }

                for (EndGameListener listener : endGameListeners) {
                    listener.endGame(aliveColorName);
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        nodesToAdd.addAll(foodList.stream().map(foodList -> foodList.getElement()).collect(Collectors.toList()));
        for (Snake snake : snakes) {
            nodesToAdd.addAll(snake.getSnakeElements());
        }

        Platform.runLater(() -> {
            boardPane.getChildren().clear();

            for (Snake snake : snakes) {
                snake.relocateSnake();
            }

            try {
                boardPane.getChildren().addAll(nodesToAdd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public Pane getBoardPane() {
        return boardPane;
    }

    public StackPane getRoot() {
        return root;
    }

    public synchronized SnakePart addFood(Point2D pointWhere, Color color) {
        SnakePart foodPart = new SnakePart(SnakePartEnum.FOOD, pointWhere.getX(),
                pointWhere.getY(), fieldSize, color);
        foodList.add(foodPart);
        return foodPart;
    }

    public synchronized SnakePart addRandomFood() {
        ArrayList<Point2D> freeSpaces = new ArrayList<>();
        for (double j = 0; j < NUMBER_OF_FIELDS; j++) {
            for (double i = 0; i < NUMBER_OF_FIELDS; i++) {
                freeSpaces.add(new Point2D(i, j));
            }
        }

        Iterator<SnakePart> iterator;
        SnakePart snakePart;
        ArrayList<SnakePart> snakeParts;

        for (Snake snake : snakes) {
            snakeParts = (ArrayList<SnakePart>) snake.getSnakeParts().clone();
            iterator = snakeParts.iterator();
            while (iterator.hasNext()) {
                snakePart = iterator.next();
                freeSpaces.remove(new Point2D(snakePart.getX(), snakePart.getY()));
            }
        }

        for (SnakePart food : foodList) {
            freeSpaces.remove(new Point2D(food.getX(), food.getY()));
        }

        Random random = new Random(System.currentTimeMillis());
        if (freeSpaces.size() <= 0) {
            return null;
        }
        Point2D randomFreeSpace = freeSpaces.get(random.nextInt(freeSpaces.size()));
        return addFood(randomFreeSpace, ColorHandler.getRandomDarkBasicColor(0.8));
    }

    public ArrayList<SnakePart> getFoodList() {
        return foodList;
    }

    public void addListener(EndGameListener listener) {
        endGameListeners.add(listener);
    }

    public void showCountdown(int number) {
        if (number != 0) {
            showCountdownText(String.valueOf(number));
        } else {
            showCountdownText(String.valueOf("Start!"));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            hideCountdownLabel();
        }
    }

    public String getAliveColorName() {
        return aliveColorName;
    }

    private void showCountdownText(String text) {
        Platform.runLater(() -> {
            countdownLabel.setVisible(true);
            countdownLabel.setText(text);
            countdownTransition.play();
        });
    }

    public void hideCountdownLabel() {
        Platform.runLater(() -> {
            countdownLabel.setVisible(false);
        });
    }

    public String getSnakeDirectionsString() {
        String result = "";
        for (Snake snake : snakes) {
            result += snake.getDirection().name().substring(0, 1);
        }

        return result;
    }

    private Node getEndDialog() throws IOException {
        if (endDialog == null) {
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource("/cz/mendelu/savic/vida_cs_snake/view/GameEndDialog.fxml"));

            endDialog = loader.load();
            endDialogController = loader.getController();
            root.getChildren().add(endDialog);
        }

        return endDialog;
    }

    public void showEndDialog(String winnerMessage, int countdownNumber) {
        Platform.runLater(() -> {
            try {
                endDialog = getEndDialog();
                endDialogController.setWinnerMessageLabelText(winnerMessage);
                endDialogController.setPlayAgainLabelText("Hr�t znovu? (" + countdownNumber + ")");
                endDialog.setVisible(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void showSimpleEndDialog(String winnerMessage, int countdownNumber) {
        Platform.runLater(() -> {
            try {
                endDialog = getEndDialog();
                endDialogController.setWinnerMessageLabelText(winnerMessage);
                endDialogController.setPlayAgainLabelText("Bude se hr�t znovu? (" + countdownNumber + ")");
                endDialogController.hideButtons();
                endDialog.setVisible(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void hideEndDialog() {
        Platform.runLater(() -> {
            endDialog.setVisible(false);
        });
    }

    public void showEndMessage(String message) {
        Platform.runLater(() -> {
            endMessageLabel.setText(message);
            endMessageLabel.setVisible(true);
        });
    }

    public void hideEndMessage() {
        Platform.runLater(() -> {
            endMessageLabel.setVisible(false);
        });
    }
}
