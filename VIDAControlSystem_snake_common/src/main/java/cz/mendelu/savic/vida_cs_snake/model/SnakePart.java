/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.model;

import cz.mendelu.savic.vida_cs_snake.controller.SnakePartElementsResizer;

import java.io.IOException;

import javafx.scene.Group;
import javafx.scene.paint.Color;

/**
 * @author david
 */
public class SnakePart {
    private Group element;
    private double x;
    private double y;
    private SnakePartEnum partEnum;
    private Color color;
    private double width;

    public SnakePart(SnakePartEnum part, double x, double y, double width, Color color) {
        this.x = x;
        this.y = y;
        this.partEnum = part;
        this.color = color;
        this.width = width;

        SnakePartElementsResizer elementsResizer = new SnakePartElementsResizer();
        try {
            element = elementsResizer.getElement(part, x, y, width, color);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // constructor just for making copies
    private SnakePart(Group element, SnakePartEnum part, double x, double y, double width, Color color) {
        this.x = x;
        this.y = y;
        this.partEnum = part;
        this.color = color;
        this.width = width;
        this.element = element;
    }

    public Group getElement() {
        return element;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public SnakePartEnum getPartEnum() {
        return partEnum;
    }

    public Color getColor() {
        return color;
    }

    // do in JavaFX thread
    public void relocateElement() {
        element.relocate(x * width, y * width);
    }

    public SnakePart getCopy() {
        return new SnakePart(element, partEnum, x, y, width, color);
    }
}
