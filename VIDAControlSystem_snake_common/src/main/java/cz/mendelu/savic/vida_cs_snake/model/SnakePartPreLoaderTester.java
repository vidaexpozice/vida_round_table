package cz.mendelu.savic.vida_cs_snake.model;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by david on 26.11.2017.
 */
public class SnakePartPreLoaderTester extends Application {
    private static final double NUMBER_OF_FIELDS = 25;
    private static final double WIDTH = 700/NUMBER_OF_FIELDS;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane mainPane = new Pane();
        mainPane.setPrefSize(700, 700);
        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });

        GameBoardController controller = Mockito.mock(GameBoardController.class);
        Snake snake = new Snake(DirectionEnum.UP, 10, 10, WIDTH, Color.RED, controller, NUMBER_OF_FIELDS);
        mainPane.getChildren().addAll(snake.getSnakeElements());
        Snake snake2 = new Snake(DirectionEnum.RIGHT, 11, 5, WIDTH, Color.GREEN, controller, NUMBER_OF_FIELDS);
        mainPane.getChildren().addAll(snake2.getSnakeElements());
        ArrayList<Snake> snakeList = new ArrayList<>();
        snakeList.add(snake);
        snakeList.add(snake2);

        ArrayList<SnakePart> foodList = new ArrayList<>();

        mainPane.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case UP:
                        snake.setDirection(DirectionEnum.UP);
                        break;
                    case RIGHT:
                        snake.setDirection(DirectionEnum.RIGHT);
                        break;
                    case DOWN:
                        snake.setDirection(DirectionEnum.DOWN);
                        break;
                    case LEFT:
                        snake.setDirection(DirectionEnum.LEFT);
                        break;
                }

                try {
                    snake.move(snakeList, foodList);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mainPane.getChildren().clear();
                snake.relocateSnake();
                snake2.relocateSnake();
                mainPane.getChildren().addAll(snake.getSnakeElements());
                mainPane.getChildren().addAll(snake2.getSnakeElements());
            }
        });
        mainPane.requestFocus();

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}