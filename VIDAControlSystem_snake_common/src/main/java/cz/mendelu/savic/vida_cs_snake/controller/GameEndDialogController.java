package cz.mendelu.savic.vida_cs_snake.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Created by david on 10.04.2017.
 */
public class GameEndDialogController {
    @FXML
    private Label winnerMessageLabel;
    @FXML
    private Label playAgainLabel;
    @FXML
    private Button yesButton;
    @FXML
    private Button noButton;

    public void setWinnerMessageLabelText(String text) {
        Platform.runLater(() -> {
            this.winnerMessageLabel.setText(text);
        });
    }

    public void setPlayAgainLabelText(String text) {
        Platform.runLater(() -> {
            this.playAgainLabel.setText(text);
        });
    }

    public void hideButtons() {
        Platform.runLater(() -> {
            this.yesButton.setVisible(false);
            this.noButton.setVisible(false);
        });
    }
}
