/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.controller;

import javafx.scene.paint.Color;

/**
 *
 * @author david
 */
public class ColorUtils {
    public static String getWebStringFromColor(Color color) {
        return String.format("#%02X%02X%02X", (int)(color.getRed() * 255),
            (int)(color.getGreen() * 255), (int)(color.getBlue() * 255 ));
    }
    
    public static Color getColorFromWebString(String webString) {
        return Color.web(webString);
    }
}
