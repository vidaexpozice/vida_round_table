package cz.mendelu.savic.vida_cs_snake;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake.model.DirectionEnum;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by david on 27.03.2017.
 */
public class MainTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader gameBoardLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_snake/view/GameBoard.fxml"));

        Parent gameBoardRoot = gameBoardLoader.load();
        GameBoardController gameBoardController = gameBoardLoader.getController();
        ArrayList<Color> colors = new ArrayList<>();
        colors.add(Color.RED);
        gameBoardController.init(this, 1, 1, colors, 700);
        primaryStage.setScene(new Scene(gameBoardRoot));
        primaryStage.show();

        for (int i = 0; i < 20; i++) {
            gameBoardController.addRandomFood();
        }

        /*Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                gameBoardController.moveSnake(0);
            }
        };
        timer.scheduleAtFixedRate(task, 1000, 1000);*/
        gameBoardRoot.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case UP:
                        gameBoardController.setDirection(0, DirectionEnum.UP);
                        break;
                    case RIGHT:
                        gameBoardController.setDirection(0, DirectionEnum.RIGHT);
                        break;
                    case DOWN:
                        gameBoardController.setDirection(0, DirectionEnum.DOWN);
                        break;
                    case LEFT:
                        gameBoardController.setDirection(0, DirectionEnum.LEFT);
                        break;
                }
                gameBoardController.moveSnakes();
            }
        });
        gameBoardRoot.requestFocus();

        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
