/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.controller;

import cz.mendelu.savic.vida_cs_snake.model.SnakePartEnum;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

/**
 *
 * @author david
 */
public class SnakePartElementsResizer {

    public Group getElement(SnakePartEnum part, double x, double y, double width, Color color) throws IOException {
        SnakePartElementsController elementsController = null;

        if (part != SnakePartEnum.FOOD) {
            FXMLLoader elementsLoader = new FXMLLoader(
                    getClass().getResource("/cz/mendelu/savic/vida_cs_snake/view/SnakePartElements.fxml"));

            elementsLoader.load();
            elementsController = elementsLoader.getController();
            elementsController.skinIt(color);
        }

        Group group;

        switch (part) {
            case FOOD:
                group = getFood(color);
                break;
            case HEAD_UP:
                group = elementsController.getHead();
                break;
            case DEAD_HEAD_UP:
                group = elementsController.getDeadHead();
                break;
            case HEAD_RIGHT:
                group = elementsController.getHead();
                group.setRotate(90);
                break;
            case DEAD_HEAD_RIGHT:
                group = elementsController.getDeadHead();
                group.setRotate(90);
                break;
            case HEAD_DOWN:
                group = elementsController.getHead();
                group.setRotate(180);
                break;
            case DEAD_HEAD_DOWN:
                group = elementsController.getDeadHead();
                group.setRotate(180);
                break;
            case HEAD_LEFT:
                group = elementsController.getHead();
                group.setRotate(270);
                break;
            case DEAD_HEAD_LEFT:
                group = elementsController.getDeadHead();
                group.setRotate(270);
                break;
            case END_UP:
                group = elementsController.getTail();
                break;
            case END_RIGHT:
                group = elementsController.getTail();
                group.setRotate(90);
                break;
            case END_DOWN:
                group = elementsController.getTail();
                group.setRotate(180);
                break;
            case END_LEFT:
                group = elementsController.getTail();
                group.setRotate(270);
                break;
            case CORNER_UP_RIGHT:
                group = elementsController.getCorner();
                break;
            case CORNER_DOWN_RIGHT:
                group = elementsController.getCorner();
                group.setRotate(90);
                break;
            case CORNER_DOWN_LEFT:
                group = elementsController.getCorner();
                group.setRotate(180);
                break;
            case CORNER_UP_LEFT:
                group = elementsController.getCorner();
                group.setRotate(270);
                break;
            case BODY_VERTICAL:
                group = elementsController.getBody();
                break;
            case BODY_HORIZONTAL:
                group = elementsController.getBody();
                group.setRotate(90);
                break;
            default:
                return null;
        }

        scaleElements(group, width);
        group.relocate(x * width, y * width);
        return group;
    }

    private Group scaleElements(Group group, double width) {
        group.setScaleX(width / 200);
        group.setScaleY(width / 200);
        group.setTranslateX(-100 + (width / 2));
        group.setTranslateY(-100 + (width / 2));

        return group;
    }

    private Group getFood(Color color) {

        Sphere ball = new Sphere(100);
        final PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor(color);
        redMaterial.setSpecularColor(Color.WHEAT);

        ball.setMaterial(redMaterial);

        return new Group(ball);
    }
}
