/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.model;

import java.util.ArrayList;
import javafx.scene.paint.Color;

/**
 *
 * @author david
 */
public class SnakePartPreLoader {
    private final SnakePart headUp;
    private final SnakePart headRight;
    private final SnakePart headDown;
    private final SnakePart headLeft;
    private final SnakePart endUp;
    private final SnakePart endRight;
    private final SnakePart endDown;
    private final SnakePart endLeft;
    private final ArrayList<SnakePart> cornersUpRight;
    private final ArrayList<SnakePart> cornersDownRight;
    private final ArrayList<SnakePart> cornersDownLeft;
    private final ArrayList<SnakePart> cornersUpLeft;
    private final ArrayList<SnakePart> bodiesVertical;
    private final ArrayList<SnakePart> bodiesHorizontal;
    private final double width;
    private final Color color;
    
    public SnakePartPreLoader(double width, Color color) {
        this.width = width;
        this.color = color;
        headUp = new SnakePart(SnakePartEnum.HEAD_UP, 0, 0, width, color);
        headRight = new SnakePart(SnakePartEnum.HEAD_RIGHT, 0, 0, width, color);
        headDown = new SnakePart(SnakePartEnum.HEAD_DOWN, 0, 0, width, color);
        headLeft = new SnakePart(SnakePartEnum.HEAD_LEFT, 0, 0, width, color);
        endUp = new SnakePart(SnakePartEnum.END_UP, 0, 0, width, color);
        endRight = new SnakePart(SnakePartEnum.END_RIGHT, 0, 0, width, color);
        endDown = new SnakePart(SnakePartEnum.END_DOWN, 0, 0, width, color);
        endLeft = new SnakePart(SnakePartEnum.END_LEFT, 0, 0, width, color);
        cornersUpRight = new ArrayList<>();
        cornersUpRight.add(new SnakePart(SnakePartEnum.CORNER_UP_RIGHT, 0, 0, width, color));
        cornersDownRight = new ArrayList<>();
        cornersDownRight.add(new SnakePart(SnakePartEnum.CORNER_DOWN_RIGHT, 0, 0, width, color));
        cornersDownLeft = new ArrayList<>();
        cornersDownLeft.add(new SnakePart(SnakePartEnum.CORNER_DOWN_LEFT, 0, 0, width, color));
        cornersUpLeft = new ArrayList<>();
        cornersUpLeft.add(new SnakePart(SnakePartEnum.CORNER_UP_LEFT, 0, 0, width, color));
        bodiesVertical = new ArrayList<>();
        bodiesVertical.add(new SnakePart(SnakePartEnum.BODY_VERTICAL, 0, 0, width, color));
        bodiesHorizontal = new ArrayList<>();
        bodiesHorizontal.add(new SnakePart(SnakePartEnum.BODY_HORIZONTAL, 0, 0, width, color));
    }
    
    private void updateCoordinates (SnakePart snakePart, double x, double y) {
        snakePart.setX(x);
        snakePart.setY(y);
    }

    public SnakePart getHeadUp(double x, double y) {
        updateCoordinates(headUp, x, y);
        return headUp;
    }

    public SnakePart getHeadRight(double x, double y) {
        updateCoordinates(headRight, x, y);
        return headRight;
    }

    public SnakePart getHeadDown(double x, double y) {
        updateCoordinates(headDown, x, y);
        return headDown;
    }

    public SnakePart getHeadLeft(double x, double y) {
        updateCoordinates(headLeft, x, y);
        return headLeft;
    }

    public SnakePart getEndUp(double x, double y) {
        updateCoordinates(endUp, x, y);
        return endUp;
    }

    public SnakePart getEndRight(double x, double y) {
        updateCoordinates(endRight, x, y);
        return endRight;
    }

    public SnakePart getEndDown(double x, double y) {
        updateCoordinates(endDown, x, y);
        return endDown;
    }

    public SnakePart getEndLeft(double x, double y) {
        updateCoordinates(endLeft, x, y);
        return endLeft;
    }

    public SnakePart getCornerUpRight(double x, double y) {
        SnakePart snakePart = cornersUpRight.get(0);
        cornersUpRight.remove(0);
        if (cornersUpRight.isEmpty()) {
            cornersUpRight.add(new SnakePart(SnakePartEnum.CORNER_UP_RIGHT, 0, 0, width, color));
        }
        updateCoordinates(snakePart, x, y);
        return snakePart;
    }
    
    public SnakePart getCornerUpLeft(double x, double y) {
        SnakePart snakePart = cornersUpLeft.get(0);
        cornersUpLeft.remove(0);
        if (cornersUpLeft.isEmpty()) {
            cornersUpLeft.add(new SnakePart(SnakePartEnum.CORNER_UP_LEFT, 0, 0, width, color));
        }
        updateCoordinates(snakePart, x, y);
        return snakePart;
    }

    public SnakePart getCornerDownLeft(double x, double y) {
        SnakePart snakePart = cornersDownLeft.get(0);
        cornersDownLeft.remove(0);
        if (cornersDownLeft.isEmpty()) {
            cornersDownLeft.add(new SnakePart(SnakePartEnum.CORNER_DOWN_LEFT, 0, 0, width, color));
        }
        updateCoordinates(snakePart, x, y);
        return snakePart;
    }

    public SnakePart getCornerDownRight(double x, double y) {
        SnakePart snakePart = cornersDownRight.get(0);
        cornersDownRight.remove(0);
        if (cornersDownRight.isEmpty()) {
            cornersDownRight.add(new SnakePart(SnakePartEnum.CORNER_DOWN_RIGHT, 0, 0, width, color));
        }
        updateCoordinates(snakePart, x, y);
        return snakePart;
    }

    public SnakePart getBodyVertical(double x, double y) {
        SnakePart snakePart = bodiesVertical.get(0);
        bodiesVertical.remove(0);
        if (bodiesVertical.isEmpty()) {
            bodiesVertical.add(new SnakePart(SnakePartEnum.BODY_VERTICAL, 0, 0, width, color));
        }
        updateCoordinates(snakePart, x, y);
        return snakePart;
    }

    public SnakePart getBodyHorizontal(double x, double y) {
        SnakePart snakePart = bodiesHorizontal.get(0);
        bodiesHorizontal.remove(0);
        if (bodiesHorizontal.isEmpty()) {
            bodiesHorizontal.add(new SnakePart(SnakePartEnum.BODY_HORIZONTAL, 0, 0, width, color));
        }
        updateCoordinates(snakePart, x, y);
        return snakePart;
    }
    
    public void removePart(SnakePart part) {
        switch (part.getPartEnum()) {
            case CORNER_UP_RIGHT:
                removeCornerUpRight(part);
                break;
            case CORNER_DOWN_RIGHT:
                removeCornerDownRight(part);
                break;
            case CORNER_DOWN_LEFT:
                removeCornerDownLeft(part);
                break;
            case CORNER_UP_LEFT:
                removeCornerUpLeft(part);
                break;
            case BODY_VERTICAL:
                removeBodyVertical(part);
                break;
            case BODY_HORIZONTAL:
                removeBodyHorizontal(part);
                break;
        }
    }
    
    public void removeCornerUpRight(SnakePart corner) {
        cornersUpRight.add(corner);
    }

    public void removeCornerDownRight(SnakePart corner) {
        cornersDownRight.add(corner);
    }

    public void removeCornerDownLeft(SnakePart corner) {
        cornersDownLeft.add(corner);
    }

    public void removeCornerUpLeft(SnakePart corner) {
        cornersUpLeft.add(corner);
    }

    public void removeBodyVertical(SnakePart body) {
        bodiesVertical.add(body);
    }

    public void removeBodyHorizontal(SnakePart body) {
        bodiesHorizontal.add(body);
    }
}
