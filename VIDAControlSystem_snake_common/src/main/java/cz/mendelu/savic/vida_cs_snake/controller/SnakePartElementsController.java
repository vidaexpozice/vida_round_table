/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.controller;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

/**
 * FXML Controller class
 *
 * @author david
 */
public class SnakePartElementsController {
    @FXML
    private Circle circle1;
    @FXML
    private Circle circle2;
    @FXML
    private Circle circle3;
    @FXML
    private Rectangle rectangle1;
    @FXML
    private Rectangle rectangle2;
    @FXML
    private Rectangle rectangle3;
    @FXML
    private Rectangle rectangle4;
    @FXML
    private Rectangle rectangle5;
    @FXML
    private Rectangle rectangle6;
    @FXML
    private Polygon polygon;
    @FXML
    private Group head;
    @FXML
    private Group tail;
    @FXML
    private Group body;
    @FXML
    private Group corner;
    @FXML
    private Group aliveFace;
    @FXML
    private Group deadFace;
    private final Color borderColor = Color.BLACK;

    public void skinIt(Color color) {
        LinearGradient linearGradient = new LinearGradient(0, 0, 100, 0, false, CycleMethod.REFLECT, new Stop(0, borderColor), new Stop(0.7, color));
        RadialGradient radialGradient1 = new RadialGradient(0, 0, circle1.getCenterX(), circle1.getCenterY(), 100, false, CycleMethod.REFLECT, new Stop(1-0.7, color), new Stop(1, borderColor));
        RadialGradient radialGradient2 = new RadialGradient(0, 0, circle2.getCenterX(), circle2.getCenterY(), 100, false, CycleMethod.REFLECT, new Stop(1-0.7, color), new Stop(1, borderColor));
        RadialGradient radialGradient3 = new RadialGradient(0, 0, circle3.getCenterX(), circle3.getCenterY(), 100, false, CycleMethod.REFLECT, new Stop(1-0.7, color), new Stop(1, borderColor));
        rectangle1.setFill(linearGradient);
        rectangle2.setFill(linearGradient);
        rectangle3.setFill(linearGradient);
        rectangle4.setFill(linearGradient);
        rectangle5.setFill(linearGradient);
        rectangle6.setFill(linearGradient);
        polygon.setFill(linearGradient);
        rectangle5.setRotate(90);
        rectangle6.setRotate(270);
        circle1.setFill(radialGradient1);
        circle2.setFill(radialGradient2);
        circle3.setFill(radialGradient3);
        rectangle1.setStroke(null);
        rectangle2.setStroke(null);
        rectangle3.setStroke(null);
        rectangle4.setStroke(null);
        rectangle5.setStroke(null);
        rectangle6.setStroke(null);
        polygon.setStroke(null);
        circle1.setStroke(null);
        circle2.setStroke(null);
        circle3.setStroke(null);
    }

    public Group getHead() {
        aliveFace.setVisible(true);
        deadFace.setVisible(false);
        return head;
    }
    
    public Group getDeadHead() {
        aliveFace.setVisible(false);
        deadFace.setVisible(true);
        return head;
    }

    public Group getTail() {
        return tail;
    }

    public Group getBody() {
        return body;
    }

    public Group getCorner() {
        return corner;
    }
}
