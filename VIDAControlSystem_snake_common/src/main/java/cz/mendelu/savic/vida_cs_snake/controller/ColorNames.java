package cz.mendelu.savic.vida_cs_snake.controller;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by david on 08.04.2017.
 */
public class ColorNames {
    private class ColorWithName {
        public final String name;
        public final Color color;

        public ColorWithName(String name, Color color) {
            this.name = name;
            this.color = color;
        }
    }

    private final ArrayList<ColorWithName> COLORS;
    private static ColorNames instance = new ColorNames();

    private ColorNames() {
        COLORS = new ArrayList<>();
        COLORS.addAll(Arrays.asList(
                new ColorWithName("�erven�", new Color(0.80, 0.00, 0.20, 1.00)),
                new ColorWithName("�erven�", new Color(0.80, 0.22, 0.20, 1.00)),
                new ColorWithName("oran�ov�", new Color(0.80, 0.25, 0.20, 1.00)),
                new ColorWithName("oran�ov�", new Color(0.80, 0.58, 0.20, 1.00)),
                new ColorWithName("�lut�", new Color(0.99, 0.80, 0.20, 1.00)),
                new ColorWithName("�lut�", new Color(0.81, 0.80, 0.20, 1.00)),
                new ColorWithName("zelen�", new Color(0.77, 0.80, 0.20, 1.00)),
                new ColorWithName("zelen�", new Color(0.20, 0.80, 0.38, 1.00)),
                new ColorWithName("zeleno-modr�", new Color(0.20, 0.80, 0.42, 1.00)),
                new ColorWithName("zeleno-modr�", new Color(0.20, 0.80, 0.56, 1.00)),
                new ColorWithName("sv�tle modr�", new Color(0.20, 1.00, 0.80, 1.00)),
                new ColorWithName("sv�tle modr�", new Color(0.20, 0.82, 0.80, 1.00)),
                new ColorWithName("modr�", new Color(0.20, 0.78, 0.80, 1.00)),
                new ColorWithName("modr�", new Color(0.20, 0.42, 0.80, 1.00)),
                new ColorWithName("tmav� modr�", new Color(0.01, 0.20, 0.80, 1.00)),
                new ColorWithName("tmav� modr�", new Color(0.16, 0.20, 0.80, 1.00)),
                new ColorWithName("fialov�", new Color(0.19, 0.20, 0.80, 1.00)),
                new ColorWithName("fialov�", new Color(0.59, 0.20, 0.80, 1.00)),
                new ColorWithName("r��ov�", new Color(0.80, 0.20, 0.98, 1.00)),
                new ColorWithName("r��ov�", new Color(0.80, 0.20, 0.44, 1.00))
        ));
    }

    private static ColorNames getInstance() {
        if (instance == null) {
            instance = new ColorNames();
        }

        return instance;
    }

    public static String getColorName(Color color) {
        ColorNames instance = ColorNames.getInstance();
        return instance.getColorNamePrivate(color);
    }

    private String getColorNamePrivate(Color color) {
        ColorWithName bestColorName = null;
        double bestColorDifference = Double.MAX_VALUE;
        double difference;

        for (ColorWithName namedColor : COLORS) {
            difference = getColorDifference(namedColor.color, color);
            if (bestColorName == null || difference < bestColorDifference) {
                bestColorName = namedColor;
                bestColorDifference = difference;
            }
        }

        return bestColorName.name;
    }

    private double getColorDifference(Color color1, Color color2) {
        double difference = 0;

        difference += Math.abs(color1.getRed() - color2.getRed());
        difference += Math.abs(color1.getGreen() - color2.getGreen());
        difference += Math.abs(color1.getBlue() - color2.getBlue());

        return difference;
    }
}
