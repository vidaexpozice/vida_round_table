/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.model;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.paint.Color;

/**
 *
 * @author david
 */
public class Snake {

    private ArrayList<SnakePart> snakeParts;
    private final double width;
    private final Color color;
    private GameBoardController gameBoardController;
    private DirectionEnum direction;
    private boolean alive;
    private boolean grow;
    private boolean canChangeDirection;
    private final SnakePartPreLoader preloader;
    private double numberOfFields;

    public Snake(DirectionEnum startDirection, double x, double y, double width, Color color,
            GameBoardController gameBoardController, double numberOfFields) throws IOException {
        snakeParts = new ArrayList<>();
        preloader = new SnakePartPreLoader(width, color);
        this.width = width;
        this.color = color;
        this.gameBoardController = gameBoardController;
        this.direction = startDirection;
        this.alive = true;
        this.canChangeDirection = true;
        this.numberOfFields = numberOfFields;

        switch (startDirection) {
            case UP:
                snakeParts.add(preloader.getHeadUp(x, y));
                snakeParts.add(preloader.getBodyVertical(x, y + 1));
                snakeParts.add(preloader.getBodyVertical(x, y + 2));
                snakeParts.add(preloader.getEndDown(x, y + 3));
                break;
            case RIGHT:
                snakeParts.add(preloader.getHeadRight(x, y));
                snakeParts.add(preloader.getBodyHorizontal(x - 1, y));
                snakeParts.add(preloader.getBodyHorizontal(x - 2, y));
                snakeParts.add(preloader.getEndLeft(x - 3, y));
                break;
            case DOWN:
                snakeParts.add(preloader.getHeadDown(x, y));
                snakeParts.add(preloader.getBodyVertical(x, y - 1));
                snakeParts.add(preloader.getBodyVertical(x, y - 2));
                snakeParts.add(preloader.getEndUp(x, y - 3));
                break;
            case LEFT:
                snakeParts.add(preloader.getHeadLeft(x, y));
                snakeParts.add(preloader.getBodyHorizontal(x + 1, y));
                snakeParts.add(preloader.getBodyHorizontal(x + 2, y));
                snakeParts.add(preloader.getEndRight(x + 3, y));
                break;
        }

        Platform.runLater(() -> {
            relocateSnake();
        });
    }

    public ArrayList<SnakePart> getSnakeParts() {
        return snakeParts;
    }

    public ArrayList<Group> getSnakeElements() {
        return snakeParts.stream().map(snakePart ->
                snakePart.getElement()).collect(Collectors.toCollection(ArrayList::new));
    }

    public MoveValidity canMove(double x, double y, DirectionEnum direction, ArrayList<Snake> snakes) {
        double newX = x;
        double newY = y;

        switch (direction) {
            case UP:
                newY--;
                break;
            case RIGHT:
                newX++;
                break;
            case DOWN:
                newY++;
                break;
            case LEFT:
                newX--;
                break;
        }

        for (Snake snake : snakes) {
            for (SnakePart part : snake.getSnakeParts()) {
                if (part.getX() == newX && part.getY() == newY) {
                    return MoveValidity.HIT_SNAKE;
                }
            }
        }

        if (newX < 0 || newY < 0 || newX >= numberOfFields || newY >= numberOfFields) {
            return MoveValidity.HIT_WALL;
        }

        return MoveValidity.VALID;
    }

    public synchronized MoveValidity move(ArrayList<Snake> snakes, ArrayList<SnakePart> foodList) throws IOException {
        // check alive
        if (!alive) {
            return MoveValidity.IS_ALREADY_DEAD;
        }

        SnakePart oldHead = snakeParts.get(0);
        SnakePart beforeLastBodyPart = snakeParts.get(snakeParts.size() - 3);
        SnakePart lastBodyPart = snakeParts.get(snakeParts.size() - 2);
        SnakePart oldTail = snakeParts.get(snakeParts.size() - 1);

        // get move result
        MoveValidity canMoveResult = this.canMove(oldHead.getX(),
                oldHead.getY(), direction, snakes);

        // if move result is valid
        if (canMoveResult != MoveValidity.VALID) {
            alive = false;
            setDeadHead();
            return canMoveResult;
        }

        // get new first body
        final SnakePart newFirstBody = getNewFirstBody(direction);
        if (newFirstBody == null) {
            return MoveValidity.NOT_VALID;
        }

        // get new head
        final SnakePart newHead = getNewHead(direction);
        if (newHead == null) {
            return MoveValidity.NOT_VALID;
        }

        // check grow
        grow = false;
        for (int i = 0; i < foodList.size(); i++) {
            if (foodList.get(i).getX() == newHead.getX() && foodList.get(i).getY() == newHead.getY()) {
                grow = true;
                foodList.remove(foodList.get(i));
                break;
            }
        }

        // add/set new head and new first body
        snakeParts.set(0, newHead);
        snakeParts.add(1, newFirstBody);

        // if snake doesn't grow
        if (!grow) {
            // add new tail and remove old last body part and old tail
            final SnakePart newTail = getNewTail(lastBodyPart.getX(), lastBodyPart.getY(),
                    beforeLastBodyPart.getX(), beforeLastBodyPart.getY());
            preloader.removePart(lastBodyPart);
            snakeParts.remove(lastBodyPart);
            snakeParts.remove(oldTail);
            snakeParts.add(newTail);
        }

        // finalize
        canChangeDirection = true;
        return MoveValidity.VALID;
    }

    private void setDeadHead() {
        SnakePart oldHead = snakeParts.get(0);
        SnakePart newHead;

        SnakePartEnum deadPart = null;
        switch (oldHead.getPartEnum()) {
            case HEAD_UP:
                deadPart = SnakePartEnum.DEAD_HEAD_UP;
                break;
            case HEAD_RIGHT:
                deadPart = SnakePartEnum.DEAD_HEAD_RIGHT;
                break;
            case HEAD_DOWN:
                deadPart = SnakePartEnum.DEAD_HEAD_DOWN;
                break;
            case HEAD_LEFT:
                deadPart = SnakePartEnum.DEAD_HEAD_LEFT;
                break;
        }

        newHead = new SnakePart(deadPart, oldHead.getX(), oldHead.getY(), width,
                color);

        snakeParts.remove(oldHead);
        snakeParts.add(0, newHead);
    }

    private SnakePart getNewFirstBody(DirectionEnum direction) throws IOException {
        SnakePart oldHead = snakeParts.get(0);
        SnakePart oldFirstBody = snakeParts.get(1);
        SnakePart newFirstBody = null;
        DirectionEnum secondDirection;

        double shiftX = oldHead.getX() - oldFirstBody.getX();
        double shiftY = oldHead.getY() - oldFirstBody.getY();

        if (shiftX == 0 && shiftY == 1) {
            secondDirection = DirectionEnum.UP;
        } else if (shiftX == 0 && shiftY == -1) {
            secondDirection = DirectionEnum.DOWN;
        } else if (shiftX == 1 && shiftY == 0) {
            secondDirection = DirectionEnum.LEFT;
        } else {
            secondDirection = DirectionEnum.RIGHT;
        }

        switch (direction) {
            case UP:
                switch (secondDirection) {
                    case UP:
                        return null;
                    case RIGHT:
                        newFirstBody = preloader.getCornerUpRight(oldHead.getX(), oldHead.getY());
                        break;
                    case DOWN:
                        newFirstBody = preloader.getBodyVertical(oldHead.getX(), oldHead.getY());
                        break;
                    case LEFT:
                        newFirstBody = preloader.getCornerUpLeft(oldHead.getX(), oldHead.getY());
                        break;
                }
                break;
            case RIGHT:
                switch (secondDirection) {
                    case UP:
                        newFirstBody = preloader.getCornerUpRight(oldHead.getX(), oldHead.getY());
                        break;
                    case RIGHT:
                        return null;
                    case DOWN:
                        newFirstBody = preloader.getCornerDownRight(oldHead.getX(), oldHead.getY());
                        break;
                    case LEFT:
                        newFirstBody = preloader.getBodyHorizontal(oldHead.getX(), oldHead.getY());
                        break;
                }
                break;
            case DOWN:
                switch (secondDirection) {
                    case UP:
                        newFirstBody = preloader.getBodyVertical(oldHead.getX(), oldHead.getY());
                        break;
                    case RIGHT:
                        newFirstBody = preloader.getCornerDownRight(oldHead.getX(), oldHead.getY());
                        break;
                    case DOWN:
                        return null;
                    case LEFT:
                        newFirstBody = preloader.getCornerDownLeft(oldHead.getX(), oldHead.getY());
                        break;
                }
                break;
            case LEFT:
                switch (secondDirection) {
                    case UP:
                        newFirstBody = preloader.getCornerUpLeft(oldHead.getX(), oldHead.getY());
                        break;
                    case RIGHT:
                        newFirstBody = preloader.getBodyHorizontal(oldHead.getX(), oldHead.getY());
                        break;
                    case DOWN:
                        newFirstBody = preloader.getCornerDownLeft(oldHead.getX(), oldHead.getY());
                        break;
                    case LEFT:
                        return null;

                }
                break;
        }

        return newFirstBody;
    }

    private SnakePart getNewTail(double newTailX, double newTailY,
                                 double newLastBodyX, double newLastBodyY) throws IOException {
        SnakePart newTail;

        double tailShiftX = newLastBodyX - newTailX;
        double tailShiftY = newLastBodyY - newTailY;

        if (tailShiftX == 0 && tailShiftY == 1) {
            newTail = preloader.getEndUp(newTailX, newTailY);
        } else if (tailShiftX == 0 && tailShiftY == -1) {
            newTail = preloader.getEndDown(newTailX, newTailY);
        } else if (tailShiftX == 1 && tailShiftY == 0) {
            newTail = preloader.getEndLeft(newTailX, newTailY);
        } else {
            newTail = preloader.getEndRight(newTailX, newTailY);
        }

        return newTail;
    }

    private SnakePart getNewHead(DirectionEnum direction) throws IOException {
        SnakePart oldHead = snakeParts.get(0);
        SnakePart newHead;

        switch (direction) {
            case UP:
                newHead = preloader.getHeadUp(oldHead.getX(), oldHead.getY() - 1);
                break;
            case RIGHT:
                newHead = preloader.getHeadRight(oldHead.getX() + 1, oldHead.getY());
                break;
            case DOWN:
                newHead = preloader.getHeadDown(oldHead.getX(), oldHead.getY() + 1);
                break;
            case LEFT:
                newHead = preloader.getHeadLeft(oldHead.getX() - 1, oldHead.getY());
                break;
            default:
                newHead = null;
                break;
        }

        return newHead;
    }

    private Position getNewHeadPosition(DirectionEnum direction) throws IOException {
        SnakePart oldHead = snakeParts.get(0);

        switch (direction) {
            case UP:
                return new Position(oldHead.getX(), oldHead.getY() - 1);
            case RIGHT:
                return new Position(oldHead.getX() + 1, oldHead.getY());
            case DOWN:
                return new Position(oldHead.getX(), oldHead.getY() + 1);
            case LEFT:
                return new Position(oldHead.getX() - 1, oldHead.getY());
            default:
                return null;
        }
    }

    public void setDirection(DirectionEnum newDirection) {
        if (!canChangeDirection) {
            return;
        }

        if (newDirection == DirectionEnum.UP && this.direction == DirectionEnum.DOWN) {
            return;
        }
        if (newDirection == DirectionEnum.RIGHT && this.direction == DirectionEnum.LEFT) {
            return;
        }
        if (newDirection == DirectionEnum.DOWN && this.direction == DirectionEnum.UP) {
            return;
        }
        if (newDirection == DirectionEnum.LEFT && this.direction == DirectionEnum.RIGHT) {
            return;
        }
        this.direction = newDirection;
        this.canChangeDirection = false;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public Color getColor() {
        return color;
    }

    // do in JavaFX thread
    public void relocateSnake() {
        for (SnakePart snakePart : snakeParts) {
            snakePart.relocateElement();
        }
    }

}
