/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.model;

/**
 *
 * @author david
 */
public enum MoveValidity {
    VALID,
    HIT_SNAKE,
    HIT_WALL,
    NOT_VALID,
    IS_ALREADY_DEAD;
}
