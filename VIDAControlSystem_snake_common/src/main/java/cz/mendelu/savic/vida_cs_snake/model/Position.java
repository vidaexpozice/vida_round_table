package cz.mendelu.savic.vida_cs_snake.model;

/**
 * Created by david on 17.04.2017.
 */
public class Position {
    private final double x;
    private final double y;

    public Position(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
