package cz.mendelu.savic.vida_cs_snake.controller;

/**
 * Created by david on 01.04.2017.
 */
public interface EndGameListener {
    public void endGame(String colorName);
}
