/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class MainLoader extends Application {

    private Stage primaryStage;
    private Parent gameBoardRoot;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        
        FXMLLoader gameBoardLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_snake/view/GameBoard.fxml"));
        
        gameBoardRoot = gameBoardLoader.load();

        Random random = new Random(System.currentTimeMillis());
        int randomBackgroundNumber = random.nextInt(4) + 1;
        
        List<Color> colors = new ArrayList<>();
        colors.add(Color.RED);
        colors.add(Color.BLUEVIOLET);
        colors.add(Color.PINK);
        colors.add(Color.YELLOW);
        colors.add(Color.GREEN);
                
        GameBoardController gameBoardController = gameBoardLoader.getController();
        gameBoardController.init(this, randomBackgroundNumber,
                5, colors, 700);
        
        this.primaryStage.setScene(new Scene(gameBoardRoot));
        this.primaryStage.setTitle("SnakeXSnake");
        //this.primaryStage.setFullScreen(true);
        this.primaryStage.setOnCloseRequest(event -> {
            //server.stopThreads();
            Platform.exit();
            System.exit(0);
        });
        //this.primaryStage.setMinWidth(1000);
        //this.primaryStage.setMinHeight(1000);
        gameBoardRoot.requestFocus();
        this.primaryStage.show();

        gameBoardController.highlightSnake(3);
        
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (int i = 5; i > 0; i--) {
                    int number = i;
                    gameBoardController.showCountdown(number);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }

                gameBoardController.showCountdown(0);
                gameBoardController.hideCountdownLabel();
                //gameBoardController.showEndDialog(5);
                gameBoardController.showEndMessage("Winner!");
            }
        };
        
        thread.start();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
