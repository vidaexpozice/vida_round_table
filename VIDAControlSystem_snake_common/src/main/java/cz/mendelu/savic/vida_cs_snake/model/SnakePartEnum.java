/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake.model;

/**
 *
 * @author david
 */
public enum SnakePartEnum {
    HEAD_UP,
    HEAD_RIGHT,
    HEAD_DOWN,
    HEAD_LEFT,
    DEAD_HEAD_UP,
    DEAD_HEAD_RIGHT,
    DEAD_HEAD_DOWN,
    DEAD_HEAD_LEFT,
    END_UP,
    END_RIGHT,
    END_DOWN,
    END_LEFT,
    CORNER_UP_RIGHT,
    CORNER_DOWN_RIGHT,
    CORNER_DOWN_LEFT,
    CORNER_UP_LEFT,
    BODY_VERTICAL,
    BODY_HORIZONTAL,
    FOOD;
}
