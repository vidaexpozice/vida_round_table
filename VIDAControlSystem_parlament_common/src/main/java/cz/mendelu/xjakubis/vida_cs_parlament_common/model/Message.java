package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import java.io.Serializable;


public class Message implements Serializable {
    public Object object;
    public String message;

    public Message(String message, Object object) {
        this.object = object;
        this.message = message;
    }

    public Message(String message) {
        this.message = message;
    }
}

