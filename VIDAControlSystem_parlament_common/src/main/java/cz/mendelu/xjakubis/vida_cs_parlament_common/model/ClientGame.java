package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import java.util.List;

public class ClientGame extends Game {
    private Law clientLaw;
    private Character clientId;
    private boolean clientLawVoted = false;
    private int points = 0;

    public ClientGame(List<Law> laws, List<Character> clientIds, Law clientLaw, Character clientId) {
        super(laws, clientIds);
        this.clientLaw = clientLaw;
        this.clientId = clientId;
    }

    public boolean isClientLawVoted() {
        return clientLawVoted;
    }

    public void setClientLawVoted(boolean clientLawVoted) {
        this.clientLawVoted = clientLawVoted;
    }

    public Law getClientLaw() {
        return clientLaw;
    }

    public void setClientLaw(Law clientLaw) {
        this.clientLaw = clientLaw;
    }

    public Character getClientId() {
        return clientId;
    }

    public void setClientId(Character clientId) {
        this.clientId = clientId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addPoint(){
        this.points++;
    }
}
