package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import java.io.Serializable;

public enum LawState implements Serializable {
    Rejected,
    Approved,
    Unknown
}
