package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Summary implements Serializable {
    private List<Law> laws = new ArrayList<>();
    private int points = 0;
    private int rank = 0;

    public Summary(List<Law> laws, int points, int rank) {
        this.laws = laws;
        this.points = points;
        this.rank = rank;
    }

    public List<Law> getLaws() {
        return laws;
    }

    public void setLaws(List<Law> laws) {
        this.laws = laws;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
