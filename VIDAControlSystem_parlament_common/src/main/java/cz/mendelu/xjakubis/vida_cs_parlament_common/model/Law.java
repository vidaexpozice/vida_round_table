package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Entity
@Table(name="law")
public class Law implements Serializable, Persistable<Integer> {
    @Transient
    static int lastId;
    @Id
    @Column(name = "law_id")
    private int id;
    private String name_cz;
    private String name_en;
    @Transient
    private Character owner;

    @Column(columnDefinition = "TEXT")
    private String description_cz;

    @Column(columnDefinition = "TEXT")
    private String description_en;

    @Transient
    private LawState opinion;
    @Transient
    private LawState lawState;

    @OneToMany( mappedBy = "law" )
    private List<VotingResult> votingResultList = new ArrayList<>();

    public Law() {
    }

    public Law(int id, String name_cz, String name_en, String description_cz,
               String description_en, LawState opinion, LawState lawState) {
        this.name_cz = name_cz;
        this.name_en = name_en;
        this.description_cz = description_cz;
        this.description_en = description_en;
        this.opinion = opinion;
        this.lawState = lawState;
        this.id = id;
    }

    public Law(int id, String name_cz, String name_en, String description_cz,
               String description_en, LawState opinion, LawState lawState, Character owner) {
        this(id, name_cz, name_en, description_cz, description_en, opinion, lawState);
        this.owner = owner;
    }

    public String getName(Locale locale){
        switch (locale.getLanguage()) {
            case "cz" :
                return name_cz;
            default:
                return name_en;
        }
    }

    public String getDescription(Locale locale){
        switch (locale.getLanguage()) {
            case "cz" :
                return description_cz;
            default:
                return description_en;
        }
    }

    public String getName_cz() {
        return name_cz;
    }

    public void setName_cz(String name) {
        this.name_cz = name;
    }

    public LawState getOpinion() {
        return opinion;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public void setOpinion(LawState opinion) {
        this.opinion = opinion;
    }

    public LawState getLawState() {
        return lawState;
    }

    public void setLawState(LawState lawState) {
        this.lawState = lawState;
    }

    public int getLawId() {
        return this.id;
    }

    public List<VotingResult> getVotingResultList() {
        return votingResultList;
    }

    public int getNumberOfVotingResultsByType(LawState type){
        return (int)this.votingResultList.stream().filter(v-> v.getResult() == type).count();
    }

    public LawState getMajorVotingResult() {
        long numberOfVotingResults = votingResultList.size();
        long numberOfApprovals = votingResultList.stream()
                .filter(v -> v.getResult() == LawState.Approved).count();
        long numberOfRejections = votingResultList.stream()
                .filter(v -> v.getResult() == LawState.Rejected).count();

        if(numberOfApprovals > numberOfRejections)
            return LawState.Approved;
        else if(numberOfApprovals< numberOfRejections)
            return LawState.Rejected;
        else
            return LawState.Unknown;
    }

    public String getDescription_cz() {
        return description_cz;
    }

    public void setDescription_cz(String description_cz) {
        this.description_cz = description_cz;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public Character getOwner() {
        return owner;
    }

    public void setOwner(Character owner) {
        this.owner = owner;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return null == getId();
    }
}
