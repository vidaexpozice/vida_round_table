package cz.mendelu.xjakubis.vida_cs_parlament_common.utils;

import cz.mendelu.savic.vida_cs.model.FileHandler;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.Law;
import cz.mendelu.xjakubis.vida_cs_parlament_common.model.LawState;

import java.io.IOException;
import java.util.ArrayList;

public class LawsLoader {
    private final String FILE_PATH = "../resources/laws.csv";
    private final FileHandler fileHandler = new FileHandler(FILE_PATH);

    public LawsLoader() throws IOException {}

    public ArrayList<Law> getLaws() throws IOException {
        ArrayList<Law> laws = new ArrayList();
        this.fileHandler.openReader();
        int index = 1;
        this.fileHandler.readLine();

        String line;
        while((line = this.fileHandler.readLine()) != null) {
            String[] lineParts = line.split(";");
            laws.add(new Law(index, lineParts[0], lineParts[1], lineParts[2], lineParts[3], LawState.Unknown, LawState.Unknown));
            ++index;
        }

        this.fileHandler.closeReader();
        return laws;
    }
}
