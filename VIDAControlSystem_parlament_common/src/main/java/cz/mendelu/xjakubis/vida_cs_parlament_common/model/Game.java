package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Game implements Serializable {
    private List<Law> laws;
    private List<Character> clientIds;

    public Game(List<Law> laws, List<Character> clientIds) {
        this.laws = laws;
        this.clientIds = clientIds;
    }

    public List<VotingResult> getAllVotingResults(){
        List<VotingResult> allResults = new ArrayList<>();
        for(Law law : laws){
            for(VotingResult result: law.getVotingResultList()){
                allResults.add(result);
            }
        }

        return allResults;
    }

    public List<Law> getLaws() {
        return laws;
    }

    public void setLaws(List<Law> laws) {
        this.laws = laws;
    }

    public List<Character> getClientIds() {
        return clientIds;
    }

    public void setClientIds(List<Character> clientIds) {
        this.clientIds = clientIds;
    }

    public Law setVotingResult(VotingResult result){
        Law law = getLawById(result.getLaw().getLawId());
        law.getVotingResultList().add(result);
        return law;
    }

    public void evaluateLawState(int id){
        Law law = getLawById(id);
        if (law.getNumberOfVotingResultsByType(LawState.Rejected) < law.getNumberOfVotingResultsByType(LawState.Approved)){
            law.setLawState(LawState.Approved);
        } else {
            law.setLawState(LawState.Rejected);
        }
    }

    public long getNumberOfLawsByState(LawState state){
        return this.laws.stream().filter(l -> l.getLawState() == state).count();
    }

    public Law getLawById(int id){
        return this.laws
                .stream()
                .filter(l-> l.getLawId() == id)
                .collect(Collectors.toList())
                .iterator()
                .next();
    }
}
