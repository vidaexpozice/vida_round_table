package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name="votingResult")
public class VotingResult implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "votingResult_id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "law_id", updatable = false)
    private Law law;

    private Character voter;
    @Enumerated(EnumType.ORDINAL)
    private LawState result;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDateTime;

    public VotingResult() {}

    public VotingResult(Law law, Character voter, LawState result, LocalDateTime createDateTime) {
        this.law = law;
        this.voter = voter;
        this.result = result;
        this.createDateTime = createDateTime;
    }

    public Law getLaw() {
        return law;
    }

    public Character getVoter() {
        return voter;
    }

    public void setVoter(Character voter) {
        this.voter = voter;
    }

    public LawState getResult() {
        return result;
    }

    public void setResult(LawState result) {
        this.result = result;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }
}
