package cz.mendelu.xjakubis.vida_cs_parlament_common.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name="law_history")
public class LawHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "law_history_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "law_id", updatable = false)
    private Law law;

    @Enumerated(EnumType.ORDINAL)
    private LawState result;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDateTime;

    public LawHistory(Law law, LawState result, LocalDateTime createDateTime) {
        this.law = law;
        this.result = result;
        this.createDateTime = createDateTime;
    }

    public LawHistory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Law getLaw() {
        return law;
    }

    public void setLaw(Law law) {
        this.law = law;
    }

    public LawState getResult() {
        return result;
    }

    public void setResult(LawState result) {
        this.result = result;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }
}
