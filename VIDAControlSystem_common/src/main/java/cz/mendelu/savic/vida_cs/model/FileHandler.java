/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs.model;

import java.io.*;

/**
 *
 * @author david
 */
public class FileHandler {

    private final String FILE_PATH;
    private BufferedWriter bw;
    private BufferedReader br;

    public FileHandler(String filePath) throws IOException {
        this.FILE_PATH = filePath;

        File file = new File(FILE_PATH);

        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
    }

    public void openReader() throws FileNotFoundException, UnsupportedEncodingException {
        br = new BufferedReader(new InputStreamReader(new FileInputStream(FILE_PATH), "UTF-8"));
    }

    public void openWriter(boolean append) throws IOException {
        bw = new BufferedWriter(new FileWriter(FILE_PATH, append));
    }

    public void closeReader() throws IOException {
        if (br != null) {
            br.close();
        }
    }

    public void closeWriter() throws IOException {
        if (bw != null) {
            bw.close();
        }
    }

    public void write(String text) throws IOException {
        bw.write(text);
    }

    public void writeLine(String line) throws IOException {
        bw.write(line + System.lineSeparator());
    }

    public String readLine() throws IOException {
        return br.readLine();
    }
}
