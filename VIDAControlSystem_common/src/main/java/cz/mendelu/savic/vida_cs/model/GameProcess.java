/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs.model;

import java.io.IOException;

/**
 *
 * @author david
 */
public class GameProcess {

    private Process appProcess;
    private Thread appCheckingThread;
    private String command;
    private String arguments;
    private Runnable endFunction;

    public GameProcess(String command, String arguments,
            Runnable endFunction) {
        this.command = command;
        this.arguments = arguments;
        this.endFunction = endFunction;

        appCheckingThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (appProcess.isAlive()) {
                        Thread.sleep(1000);
                    }

                    if (endFunction != null) {
                        endFunction.run();
                    }
                } catch (InterruptedException e) {
                    // do nothing, because thread was just terminated
                    // (because game finished)
                    //e.printStackTrace();
                }
            }
        };
    }

    public void start() throws IOException {
        // launch server app
        appProcess = Runtime.getRuntime().exec(command + " " + arguments);
        appCheckingThread.start();
    }

    public void end() {
        endFunction = null;
        
        // terminate server app:
        if (appProcess != null) {
            appProcess.destroy();
        }
        if (appCheckingThread != null) {
            appCheckingThread.interrupt();
        }
    }
}
