package cz.mendelu.savic.vida_cs.model;

import java.io.IOException;
import java.util.ArrayList;

public class GameLoader {
    private final String FILE_PATH = "../resources/games.csv";
    private final FileHandler fileHandler;
    
    public GameLoader() throws IOException {
        fileHandler = new FileHandler(FILE_PATH);
    }
    
    public ArrayList<Game> getGames() throws IOException {
        ArrayList<Game> games = new ArrayList<>();
        fileHandler.openReader();
        
        String line;
        String[] lineParts;
        int index = 0;
        fileHandler.readLine(); // skip heading
        while ((line = fileHandler.readLine()) != null) {
            lineParts = line.split(";");
            games.add(new Game(lineParts[0], lineParts[1], lineParts[2],
                    Boolean.valueOf(lineParts[3]), lineParts[4], lineParts[5],
                    lineParts[6], index));
            index++;
        }
        
        fileHandler.closeReader();
        return games;
    }
}
