/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs.model;

import java.util.Random;
import javafx.scene.paint.Color;

/**
 *
 * @author david
 */
public class ColorHandler {
    public static Color getRandomDarkBasicColor(double opacity) {
        Random rand = new Random();
        int colorIndex = rand.nextInt(6) + 1;
        
        switch (colorIndex) {
            default:
            case 1:
                return new Color(0.75, 0.0, 0.0, opacity);
            case 2:
                return new Color(0.75, 0.75, 0.0, opacity);
            case 3:
                return new Color(0.25, 0.75, 0.0, opacity);
            case 4:
                return new Color(0.5, 0.75, 0.0, opacity);
            case 5:
                return new Color(0.75, 0.5, 0.0, opacity);
            case 6:
                return new Color(0.5, 0.5, 0.0, opacity);
        }
    }
    
    public static Color getRandomBasicColor() {
        return getRandomDarkBasicColor(1.0);
    }
    
    public static Color getRandomColor(double opacity) {
        Random rand = new Random();
        float red = rand.nextFloat();
        float green = rand.nextFloat();
        float blue = rand.nextFloat();
        return new Color(red, green, blue, opacity);
    }
    
    public static Color getRandomColor() {
        return getRandomColor(1.0);
    }
}
