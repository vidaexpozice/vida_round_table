package cz.mendelu.savic.vida_cs.model;

public class Game implements Comparable<Game> {

    private final String name;
    private final String description;
    private final String screenPath;
    private final boolean usesProjector;
    private final String numberOfPlayers;
    private final String runCommandClient;
    private final String runCommandServer;
    private final int number;
    private final int index;

    public String getName() {
        return name;
    }

    public Game(String name, String description, String screenPath, boolean usesProjector,
            String numberOfPlayers, String runCommandClient, String runCommandServer,
            int gameIndex) {
        this.name = name;
        this.description = description;
        this.screenPath = screenPath;
        this.usesProjector = usesProjector;
        this.numberOfPlayers = numberOfPlayers;
        this.runCommandClient = runCommandClient;
        this.runCommandServer = runCommandServer;
        this.index = gameIndex;
        this.number = gameIndex + 1;
    }

    public String getDescription() {
        return description;
    }

    public String getScreenPath() {
        return screenPath;
    }

    public boolean getIfUsesProjector() {
        return usesProjector;
    }

    public String getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public int getNumberOfPlayersMin() {
        if (numberOfPlayers.contains("-")) {
            return Integer.valueOf(numberOfPlayers.split("-")[0]);
        } else {
            return Integer.valueOf(numberOfPlayers);
        }
    }

    public int getNumberOfPlayersMax() {
        if (numberOfPlayers.contains("-")) {
            return Integer.valueOf(numberOfPlayers.split("-")[1]);
        } else {
            return Integer.valueOf(numberOfPlayers);
        }
    }

    public String getRunCommandClient() {
        return runCommandClient;
    }

    public String getRunCommandServer() {
        return runCommandServer;
    }

    public int getNumber() {
        return number;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public int compareTo(Game t) {
        return this.index - t.index;
    }
}
