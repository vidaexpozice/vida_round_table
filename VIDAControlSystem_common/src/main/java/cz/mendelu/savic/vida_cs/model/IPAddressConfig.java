package cz.mendelu.savic.vida_cs.model;

import java.io.IOException;
import javafx.application.Platform;

public class IPAddressConfig {

    private final String IP_CONFIG_PATH = "../resources/ipConfig.txt";
    private FileHandler fileHandler = null;

    public IPAddressConfig() {
        try {
            fileHandler = new FileHandler(IP_CONFIG_PATH);
        } catch (IOException e) {
            e.printStackTrace();

            Platform.exit();
            System.exit(0);
        }
    }

    public void setIPAddress(String ipAddress) throws IOException {
        fileHandler.openWriter(false);
        fileHandler.writeLine(ipAddress);
        fileHandler.closeWriter();
    }

    public String getIPAddress() throws IOException {
        String ipAddress = null;

        fileHandler.openReader();

        ipAddress = fileHandler.readLine();
        fileHandler.closeReader();

        return ipAddress;
    }

    public String getIP_CONFIG_PATH() {
        return IP_CONFIG_PATH;
    }
}
