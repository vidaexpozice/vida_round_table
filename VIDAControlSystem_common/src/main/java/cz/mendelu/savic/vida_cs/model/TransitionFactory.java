/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs.model;

import javafx.animation.FadeTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 *
 * @author david
 */
public class TransitionFactory {

    public static FadeTransition createFadingNode(Node node) {
        return createFadingNode(node, 0.7, 1, 0.25);
    }
    
    public static FadeTransition createSlightlyFadingNode(Node node) {
        return createFadingNode(node, 0.7, 1, 0.6);
    }
    
    public static FadeTransition createFadingNode(Node node, double durationInSeconds, double fromValue,
            double toValue) {
        FadeTransition transition = new FadeTransition(Duration.seconds(durationInSeconds), node);

        transition.setFromValue(fromValue);
        transition.setToValue(toValue);
        transition.setAutoReverse(true);
        transition.setCycleCount(Transition.INDEFINITE);
        
        return transition;
    }

    public static FadeTransition createShortTimeFadingNode(Node node) {
        FadeTransition transition = createFadingNode(node);
        transition.setCycleCount(6);
        return transition;
    }
    
    public static RotateTransition createSpinningNode(Node node) {
        RotateTransition transition = new RotateTransition();
        transition.setNode(node);
        transition.setByAngle(360);
        transition.setCycleCount(Transition.INDEFINITE);
        transition.setInterpolator(Interpolator.LINEAR);
        transition.setRate(0.03);
                
        return transition;
    }
    
    /**
     * Transition ideal for countdown numbers.
     * 
     * @return
     */
    public static ParallelTransition createAppearingNode(Node node) {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(300), node);
        fadeTransition.setFromValue(0);
        fadeTransition.setToValue(1);
        
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(300), node);
        scaleTransition.setFromX(2);
        scaleTransition.setFromY(2);
        scaleTransition.setToX(1);
        scaleTransition.setToY(1);
        
        ParallelTransition parallelTransition = new ParallelTransition();
        parallelTransition.getChildren().addAll(fadeTransition, scaleTransition);
        return parallelTransition;
    }
}
