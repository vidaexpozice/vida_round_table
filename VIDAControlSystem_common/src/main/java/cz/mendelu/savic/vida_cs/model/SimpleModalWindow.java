/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs.model;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 *
 * @author david
 */
public class SimpleModalWindow {

    private Stage modalWindow = null;

    public SimpleModalWindow(Window owner, String message) {
        final VBox group = new VBox();
        group.setAlignment(Pos.CENTER);
        group.setPadding(new Insets(20));
        group.setStyle("-fx-font-family: Arial;-fx-font-size: 24px;");
        modalWindow = new Stage();
        modalWindow.initModality(Modality.APPLICATION_MODAL);
        modalWindow.initOwner(owner);
        group.getChildren().add(new Text(message));
        Scene dialogScene = new Scene(group);
        modalWindow.setScene(dialogScene);
    }

    public Stage getModalWindow() {
        return modalWindow;
    }

    public void showModalWindow() {
        if (modalWindow != null) {
            modalWindow.showAndWait();
        }
    }
}
