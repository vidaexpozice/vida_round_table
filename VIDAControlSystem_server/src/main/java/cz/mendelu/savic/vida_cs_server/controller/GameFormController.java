/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.controller;

import cz.mendelu.savic.vida_cs_server.model.GameFormTimer;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
//import javafx.scene.control.RadioButton;
//import javafx.scene.control.TextArea;
//import javafx.scene.control.TextField;
//import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Paint;

/**
 * FXML Controller class
 *
 * @author david
 */
public class GameFormController implements Initializable {

    @FXML
    private Label nameLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label projectorLabel;
    @FXML
    private Label playersLabel;
    @FXML
    private Label clientCommandLabel;
    @FXML
    private Label serverCommandLabel;
    @FXML
    private ImageView screenshotImage;
//    @FXML
//    private TextField nameField;
//    @FXML
//    private TextArea descriptionArea;
//    @FXML
//    private TextField clientCommandField;
//    @FXML
//    private TextField serverCommandField;
//    @FXML
//    private ToggleGroup projectorRadioGroup;
//    @FXML
//    private RadioButton projectorRadioYesButton;
//    @FXML
//    private RadioButton projectorRadioNoButton;
    @FXML
    private LabeledIconController hideIconController;
    @FXML
    private LabeledIconController deleteIconController;
    @FXML
    private GridPane root;
    @FXML
    private ComboBox minPlayersComboBox;
    @FXML
    private ComboBox maxPlayersComboBox;
    @FXML
    private Label warningLabel;
    private GameFormPaneController parentController;
    private GameFormTimer timer;
    private final String WARNING_LABEL_DEFAULT_TEXT = "V�echna pole je�t� nejsou vypln�na nebo nejsou validn�.";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        hideIconController.init("Nezobrazovat hru", "../resources/images/pause_icon.png");
        deleteIconController.init("Odebrat hru", "../resources/images/delete_icon.png");

        deleteIconController.getRoot().setOnMouseClicked(e -> {
            removeGameForm();
        });
        
        warningLabel.setText(WARNING_LABEL_DEFAULT_TEXT);
        
        initComboBoxes();
        
        makeAllLabelsRed();
        screenshotImage.setStyle("-fx-border-color: red;-fx-border-radius: 5;-fx-padding: 10 10 10 10;-fx-background-color: black");
    }
    
    public void makeAllLabelsRed() {
        makeLabelTextRed(nameLabel);
        makeLabelTextRed(descriptionLabel);
        makeLabelTextRed(projectorLabel);
        makeLabelTextRed(playersLabel);
        makeLabelTextRed(clientCommandLabel);
        makeLabelTextRed(serverCommandLabel);
    }
    
    public void makeLabelTextRed(Label label) {
        label.setTextFill(Paint.valueOf("red"));
    }
    
    public void makeLabelTextBlack(Label label) {
        label.setTextFill(Paint.valueOf("black"));
    }
    
    private void initComboBoxes() {
        for (int i = 1; i <= 12; i++) {
            minPlayersComboBox.getItems().add(String.valueOf(i));
            maxPlayersComboBox.getItems().add(String.valueOf(i));
        }
    }

    public void setParentController(GameFormPaneController parentController) {
        this.parentController = parentController;
    }

    private void removeGameForm() {
        this.parentController.removeGameForm(root);
    }

    public void handleChange() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new GameFormTimer();
        timer.start();
    }
}
