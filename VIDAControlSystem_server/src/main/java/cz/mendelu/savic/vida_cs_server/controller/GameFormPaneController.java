/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.controller;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs_server.model.GameFileHandler;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author david
 */
public class GameFormPaneController {

    @FXML
    private VBox root;
    private GameFileHandler gameFileHandler;
    private Server server;

    public void init(Server server) {
        try {
            this.server = server;
            addAddIcon();
            //gameFileHandler = new GameFileHandler();
        } catch (IOException ex) {
            server.sendToServerConsole(ex);
            LogUtils.logException(this, ex);
        }
    }

    private void addAddIcon() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/mendelu/savic/vida_cs_server/view/LabeledIcon.fxml"));
        Parent addIcon = (Parent) loader.load();
        ((LabeledIconController) loader.getController()).init("P�idat hru", "../resources/images/add_icon.png");

        addIcon.setOnMouseClicked(e -> {
            try {
                addGameForm();
            } catch (IOException ex) {
                server.sendToServerConsole(ex);
                LogUtils.logException(this, ex);
            }
        });

        root.getChildren().add(addIcon);
    }

    private void addGameForm() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/mendelu/savic/vida_cs_server/view/GameForm.fxml"));
        Parent gameForm = (Parent) loader.load();
        ((GameFormController) loader.getController()).setParentController(this);

        root.getChildren().add(root.getChildren().size() - 1, gameForm);
    }
    
    public void removeGameForm(Node gameForm) {
        root.getChildren().remove(gameForm);
    }
}
