/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.model;

import cz.mendelu.savic.vida_cs.model.FileHandler;
import java.io.IOException;

/**
 *
 * @author david
 */
public class GameFileHandler {

    private final String GAME_FILE_PATH = "../resources/game.csv";
    private FileHandler fileHandler = null;

    public GameFileHandler() throws IOException {
        fileHandler = new FileHandler(GAME_FILE_PATH);
    }
    
    public void writeLine(String line) throws IOException {
        fileHandler.openWriter(true);
        fileHandler.writeLine(line);
        fileHandler.closeWriter();
    }
}
