/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.model;

import cz.mendelu.savic.vida_cs.model.Game;

/**
 *
 * @author david
 */
public class GameRecord extends Game {

    public GameRecord(String name, String description, String screenPath, boolean usesProjector,
            String numberOfPlayers, String runCommandClient, String runCommandServer, int gameIndex) {
        super(name, description, screenPath, usesProjector, numberOfPlayers, runCommandClient, runCommandServer, gameIndex);
    }

}
