/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.model;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs_server.controller.Server;
import cz.mendelu.savic.vida_cs.model.FileHandler;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import javafx.scene.control.TextArea;

/**
 *
 * @author david
 */
public class ConsoleLogger extends TimerTask {

    private final String LOGS_FOLDER_PATH = "../resources/consoleLogs/";
    private final long LOG_PERIOD = 60*60*1000; // period for logging -> one hour
    private FileHandler fileHandler;
    private Server server;
    private TextArea console;
    private Timer loggerTimer;

    public ConsoleLogger(Server server) {
        this.server = server;
        this.console = server.getController().getTextArea();
        loggerTimer = new Timer(true);
    }

    public void logConsole() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(Calendar.getInstance().getTime());
        String fileName = LOGS_FOLDER_PATH + "consoleLogFor_" + timeStamp + ".txt";

        if (!(new File(fileName).exists())) {
            fileHandler = new FileHandler(fileName);

            fileHandler.openWriter(false);
            fileHandler.write(console.getText().replace("\n", System.lineSeparator()));
            fileHandler.closeWriter();

            // keep 5 latest log files and delete the rest
            deleteOldLogs(5);
            console.clear();

            if (server != null) {
                server.sendToServerConsole("Text konzole byl pr�v� zalogov�n do souboru " + fileName);
            }
        }
    }

    private void deleteOldLogs(int numberOfLogsToKeep) {
        File logFolder = new File(LOGS_FOLDER_PATH);
        File[] logs = logFolder.listFiles();

        for (int i = 0; i < logs.length - numberOfLogsToKeep; i++) {
            logs[i].delete();
        }
    }

    @Override
    public void run() {
        try {
            logConsole();
        } catch (IOException ex) {
            server.sendToServerConsole(ex);
            LogUtils.logException(this, ex);
        }
    }
    
    public void start() {
        // schedule logger timer for every hour
        loggerTimer.scheduleAtFixedRate(this, LOG_PERIOD, LOG_PERIOD);
    }
    
}
