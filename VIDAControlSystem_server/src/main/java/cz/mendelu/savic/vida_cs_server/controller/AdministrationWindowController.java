package cz.mendelu.savic.vida_cs_server.controller;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs.model.FileHandler;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Dimension2D;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class AdministrationWindowController {

    // VIDA touch screen size
    private Dimension2D screenSize;
    // full HD screen size
    //private static final Dimension2D SCREEN_SIZE = new Dimension2D(1920, 1080);

    @FXML
    private TextArea textArea;
    @FXML
    private Label ipAddressLabel;
    //@FXML
    //private GameFormPaneController gameFormPaneController;
    private Server server;
    private Stage primaryStage;
    private Stage projectorWindowStage;
    private boolean showProjectorWindow = false;

    public void init(Server server, Stage primaryStage) {
        this.server = server;
        this.primaryStage = primaryStage;
        initScreenSize();
        //gameFormPaneController.init(server);
        initProjectorWindowStage();
    }

    public void initScreenSize() {
        FileHandler fileHandler = null;

        try {
            fileHandler = new FileHandler("../resources/resolution.txt");
            fileHandler.openReader();
            String[] lineParts = fileHandler.readLine().split("x");
            screenSize = new Dimension2D(Double.valueOf(lineParts[0]),
                    Double.valueOf(lineParts[1]));
        } catch (IOException ex) {
            server.sendToServerConsole(ex);
            LogUtils.logException(this, ex);
        } finally {
            if (fileHandler != null) {
                try {
                    fileHandler.closeReader();
                } catch (IOException ex) {
                    server.sendToServerConsole(ex);
                    LogUtils.logException(this, ex);
                }
            }
        }
    }

    public void outputText(String text) {
        String s = text;
        if (!text.endsWith(".") && !text.toLowerCase().contains("exception")) {
            s += ".";
        }
        s += "\n";  // add end of line

        String finalS = s;
        Platform.runLater(() -> {
            textArea.appendText(finalS);
        });
    }

    public void setLabelIpAddress(String ipAddress) {
        ipAddressLabel.setText("IP adresa serveru: " + ipAddress);
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public void showProjectorWindow() {
        showProjectorWindow = true;
        projectorWindowStage.show();
    }

    public void setProjectorWindowAlwaysOnTop(boolean alwaysOnTop) {
        if (projectorWindowStage != null) {
            projectorWindowStage.setAlwaysOnTop(alwaysOnTop);
            /*if (alwaysOnTop && showProjectorWindow) {
                projectorWindowStage.show();
            } else {
                projectorWindowStage.hide();
            }*/
        }
    }

    private void initProjectorWindowStage() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/cz/mendelu/savic/vida_cs_server/view/ProjectorWindow.fxml"));
            Parent root = loader.load();
            projectorWindowStage = new Stage();

            ProjectorWindowController controller = loader.getController();
            controller.init(projectorWindowStage, server);

            // make new window modal -> block events from parent:
            projectorWindowStage.initModality(Modality.WINDOW_MODAL);
            // keep primary stage on background (it's not minimized):
            projectorWindowStage.initOwner(primaryStage);
            projectorWindowStage.initStyle(StageStyle.UNDECORATED);
            Scene scene = new Scene(root);
            scene.setCursor(Cursor.NONE);
            projectorWindowStage.setScene(scene);

            projectorWindowStage.setOnCloseRequest((WindowEvent event) -> {
                showProjectorWindow = false;
            });

            Dimension2D projectorWindowSize = ProjectorWindowController.getWindowSize();
            projectorWindowStage.setX((screenSize.getWidth() - projectorWindowSize.getWidth()) / 2);
            projectorWindowStage.setY((screenSize.getHeight() - projectorWindowSize.getHeight()) / 2);
            projectorWindowStage.setAlwaysOnTop(true);
        } catch (IOException ex) {
            server.sendToServerConsole(ex);
            LogUtils.logException(this, ex);
        }
    }
}
