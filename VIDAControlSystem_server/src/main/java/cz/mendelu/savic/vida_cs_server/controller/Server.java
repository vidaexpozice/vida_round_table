package cz.mendelu.savic.vida_cs_server.controller;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs.model.Game;
import cz.mendelu.savic.vida_cs.model.GameLoader;
import cz.mendelu.savic.vida_cs_server.model.ConsoleLogger;
import cz.mendelu.savic.vida_cs_server.model.GameServer;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;

public class Server extends Thread {

    private ArrayList<GameServer> games;
    private ArrayList<ClientHandler> clientHandlers;
    private ServerSocket serverSocket;
    private AdministrationWindowController controller;
    private ConsoleLogger logger;
    private boolean startingGame = false;

    public Server(AdministrationWindowController controller) throws IOException {
        this.controller = controller;
        clientHandlers = new ArrayList<ClientHandler>();
        try {
            serverSocket = new ServerSocket(7000);
            //sendToServerConsole(serverSocket.getLocalSocketAddress().toString());
            sendToServerConsole("Poslouch�m na IP adrese "
                    + InetAddress.getLocalHost().getHostAddress() + ", na portu "
                    + serverSocket.getLocalPort());
            controller.setLabelIpAddress(InetAddress.getLocalHost().getHostAddress());
        } catch (IOException e) {
            sendToServerConsole(e);
            LogUtils.logException(this, e);
        }

        ArrayList<Game> games = new GameLoader().getGames();
        this.games = new ArrayList<>();

        for (int i = 0; i < games.size(); i++) {
            this.games.add(new GameServer(games.get(i), 1));
        }

        sendToServerConsole("�ek�m a� se p�ipoj� klienti");
        
        logger = new ConsoleLogger(this);
        logger.start(); // start console logger which logs console text every hour
    }

    public synchronized boolean isStartingGame() {
        return startingGame;
    }

    public synchronized void setStartingGame(boolean startingGame) {
        this.startingGame = startingGame;
    }

    @Override
    public void run() {
        while (true) {
            Socket socket;
            try {
                socket = serverSocket.accept();
                acceptClient(socket);
            } catch (IOException ex) {
                sendToServerConsole(ex);
                LogUtils.logException(this, ex);
            }
        }
    }

    private void acceptClient(Socket socket) {
        sendToServerConsole("P�ijal jsem klienta na portu " + socket.getPort() +
                " a IP adrese " + socket.getInetAddress().toString().replace("/", ""));

        ClientHandler clientHandler = new ClientHandler(this, socket);
        clientHandlers.add(clientHandler);
        clientHandler.start();
    }

    public void terminateClients() {
        sendToAllClients("KILL_YOURSELF");
        endEachClientHandlerGame();
    }

    private void endEachClientHandlerGame() {
        for (ClientHandler clientHandler : clientHandlers) {
            clientHandler.endGame();
        }
    }

    public synchronized void sendToServerConsole(String message) {
        //controller.outputText(message);
    }

    public synchronized void sendToServerConsole(Throwable exception) {
        /*StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        controller.outputText(sw.toString());
        sw = null;
        pw = null;*/
    }

    public synchronized void sendToAllClients(String message) {
        sendToSelectedClients(message, clientHandlers);
    }

    public synchronized void sendToSelectedClients(String message, ArrayList<ClientHandler> clients) {
        for (int i = 0; i < clients.size(); i++) {
            clients.get(i).getOutput().println(message);
        }
    }

    public synchronized GameServer getNonRunningGame(int index) {
        GameServer oldGameInstance = null;

        for (GameServer game : games) {
            if (game.getIndex() == index) {
                oldGameInstance = game;

                if (!game.isRunning()) {
                    return game;
                }
            }
        }

        if (oldGameInstance == null) {
            return null;
        }

        // if there was no non-running game of this index then
        // add new instance from some old instance of this game
        return addGameInstance(oldGameInstance);
    }

    public synchronized AdministrationWindowController getController() {
        return controller;
    }

    public GameServer addGameInstance(GameServer game) {
        GameServer newGameInstance = null;
        int instance = 1;
        boolean instanceExists;

        while (true) {
            instanceExists = false;

            for (ClientHandler client : clientHandlers) {
                if (client.getActualGame() != null
                        && client.getActualGame().getIndex() == game.getIndex()
                        && client.getActualGame().getInstance() == instance) {
                    instance++;
                    instanceExists = true;
                    break;
                }
            }

            if (!instanceExists) {
                newGameInstance = new GameServer(game, instance);
                games.add(newGameInstance);
                break;
            }
        }

        sendToAllClients("CONNECTED " + newGameInstance.getIndex() + " 0");

        return newGameInstance;
    }

    public int getNumberOfGameInstances(int gameIndex) {
        HashSet<GameServer> instances = new HashSet<>();
        for (GameServer gameFromList : games) {
            if (gameFromList.getIndex() == gameIndex) {
                instances.add(gameFromList);
            }
        }

        return instances.size();
    }

    public synchronized void removeLastNonRunningGameInstance(int gameIndex) {
        if (getNumberOfGameInstances(gameIndex) > 1) {
            GameServer game;

            for (int i = (games.size() - 1); i >= 0; i--) {
                game = games.get(i);
                if (game.getIndex() == gameIndex && game.getNumberOfConnectedClients() == 0
                        && !game.isRunning()) {
                    games.remove(game);
                    return;
                }
            }
        }
    }
    
    public synchronized ArrayList<ClientHandler> getNotConnectedClients(GameServer game) {
        ArrayList<ClientHandler> clients = new ArrayList<>();
        
        for (ClientHandler client : clientHandlers) {
            if (client.getActualGame() != game) {
                clients.add(client);
            }
        }
        
        return clients;
    }

    public ConsoleLogger getLogger() {
        return logger;
    }

    public ArrayList<GameServer> getGames() {
        return games;
    }
}
