package cz.mendelu.savic.vida_cs_server;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs_server.controller.AdministrationWindowController;
import cz.mendelu.savic.vida_cs_server.controller.Server;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.Instant;

public class MainLoader extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        FXMLLoader loader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_server/view/AdministrationWindow.fxml"));

        Parent root = loader.load();

        Server server = new Server(loader.getController());
        server.start();

        AdministrationWindowController controller = loader.getController();
        controller.init(server, primaryStage);

        this.primaryStage.setScene(new Scene(root));
        this.primaryStage.setTitle("VIDA administrace ��d�c�ho syst�mu");
        this.primaryStage.setMaximized(true);
        //this.primaryStage.setFullScreen(true);
        this.primaryStage.setOnCloseRequest(event -> {
            //server.stopThreads();
            server.terminateClients();
            try {
                server.getLogger().logConsole();
            } catch (IOException e) {
                e.printStackTrace();
                LogUtils.logException(this, e);
            }
            Platform.exit();
            System.exit(0);
        });
        this.primaryStage.show();
        LogUtils.logMessage(this, "Server spu�t�n " + Instant.now());
        controller.showProjectorWindow();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
