package cz.mendelu.savic.vida_cs_server.controller;

import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs.model.GameProcess;
import cz.mendelu.savic.vida_cs.model.IPAddressConfig;
import cz.mendelu.savic.vida_cs_server.model.GameServer;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.GamePool;
import cz.mendelu.xjakubis.vida_cs_analytics_common.service.ControlSystemAnalytics;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;

/**
 * Client handler for communicating with client and sending following message signals:
 *
 * Client -> Server:
 *   JOIN X - I wanna join game with number X (or create if I'm first).
 *   LEAVE X - I wanna leave waiting queue for game with number X.
 *   START X - I want to launch game with number X.
 *   GET_INFO X - I wanna know how many players are connected to game X.
 *   FINISH X - I finished game with number X (either game finished, player left, or error occured)
 *   GET_PROJECTOR_INFO - I wanna know if the projector is being used.
 *
 * Server -> Client:
 *   ACCEPT X - I accept your request to join game X. (he will be accepted as long
 *              as he is playing the game) -> player goes to INGAME status.
 *   REJECT X - I reject your request to join game X.
 *   CONNECTED X Y - The game with number X has Y players connected.
 *   MESSAGE text - Client, please notify the user with this message.
 *   IN_PROGRESS X - Game with number X is in progress (already played).
 *   LAUNCH X P - Client, please start game X which you requested on port P.
 *   END X - Client, close game X (if you didn't do it yet), because it finished.
 *   PROJECTOR_IN_USE - Client, projector is being used right now.
 *   PROJECTOR_NOT_IN_USE - Client, projector is available. You can use it.
 *   KILL_YOURSELF - Client, close your application.
 *
 * Where X is number of the game and Y number of players.
 *
 */
public class ClientHandler extends Thread {

    private BufferedReader input;
    private PrintWriter output;
    private Server server;
    private Socket socket;
    private GameServer actualGame;
    private boolean projectorInUse;
    private BlockingQueue<String> queue = new ArrayBlockingQueue<String>(20);
    private GamePool actualGamePool;

    public ClientHandler(Server server, Socket socket) {
        try {
            this.server = server;
            this.socket = socket;
            this.actualGame = null;
            this.projectorInUse = false;
            this.input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            this.output = new PrintWriter(
                    socket.getOutputStream(), true);
        } catch (IOException e) {
            server.sendToServerConsole(e);
            LogUtils.logException(this, e);
        }
    }

    public PrintWriter getOutput() {
        return output;
    }

    public Socket getSocket() {
        return socket;
    }

    public void sendToAClient(String message) {
        output.println(message);
    }

    public GameServer getActualGame() {
        return actualGame;
    }

    public void setActualGame(GameServer actualGame) {
        this.actualGame = actualGame;
    }

    public void wait(int miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            server.sendToServerConsole(e);
            LogUtils.logException(this, e);
        }
    }

    public void setProjectorInUse(boolean projectorInUse) {
        this.projectorInUse = projectorInUse;
    }

    private ArrayList<ClientHandler> getConnectedClients(GameServer game) {
        return game.getConnectedClients();
    }

    private ArrayList<ClientHandler> getNotConnectedClients(GameServer game) {
        return server.getNotConnectedClients(game);
    }

    private int getNumberOfConnectedClients(int gameIndex) {
        return server.getNonRunningGame(gameIndex).getNumberOfConnectedClients();
    }

    private boolean addConnectedClient(int gameIndex) {
        return server.getNonRunningGame(gameIndex).addConnectedClient(this);
    }

    private boolean removeConnectedClient(GameServer game) {
        return game.removeConnectedClient(this);
    }

    private boolean isActualGameRunning() {
        if (actualGame == null) {
            return false;
        }
        return actualGame.isRunning();
    }

    public void serverGameProcessCleanUp() {
        server.sendToServerConsole("Server pro hru " + (actualGame.getNumber())
                + " (instance " + actualGame.getInstance()
                + ") byl ukon�en -> ukon�uji i klienty");
        endGame();
    }

    /**
     * This method ends game. This cleans up on server and on each client. It is supposed to run
     * just once for the game (not on every ClientHandler, just one)
     */
    public void endGame() {
        if (actualGame == null || !isActualGameRunning()) {
            return;
        }
        GameServer game = actualGame;

        ControlSystemAnalytics.stopGamePool(actualGame.getInstance());
        // SERVER:
        // set game isRunning to false
        game.setRunning(false);
        server.removeLastNonRunningGameInstance(game.getIndex());

        if (game.getIfUsesProjector()) {
            server.sendToAllClients("PROJECTOR_NOT_IN_USE");
        }

        // CLIENTS:
        // tell clients to terminated their launched app
        server.sendToSelectedClients("END " + game.getIndex(), getConnectedClients(game));

        // SERVER:
        // clear connectedClients, set their actualGame to none and projectorInUse to false
        game.clearConnectedClients();

        Platform.runLater(() -> {
            server.getController().setProjectorWindowAlwaysOnTop(true);
        });

        // terminate server app:
        game.endProcess();
        server.sendToServerConsole("Hra ��slo " + game.getNumber()
                + " (instance " + game.getInstance() + ") skon�ila");

        // CLIENTS:
        // set connected players on clients to 0
        server.sendToAllClients("CONNECTED " + game.getIndex() + " "
                + server.getNonRunningGame(game.getIndex()).getNumberOfConnectedClients());
    }

    @Override
    public void run() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        queue.put(input.readLine());
                    } catch (SocketException e) {
                        // client disconneted
                        try {
                            LogUtils.logMessage(this, "Klient na portu " + socket.getPort()
                                    + " se odpojil");
                            server.sendToServerConsole("Klient na portu " + socket.getPort()
                                    + " se odpojil");
                            if (isActualGameRunning()) {
                                endGame();
                            } else if (actualGame != null) {
                                removeConnectedClient(actualGame);
                                server.sendToAllClients("CONNECTED " + actualGame.getIndex() + " "
                                        + getNumberOfConnectedClients(actualGame.getIndex()));
                            }
                            socket.close();
                            return;
                        } catch (IOException ex) {
                            server.sendToServerConsole(e);
                            LogUtils.logException(this, ex);
                        }
                        break;
                    } catch (InterruptedException e) {
                        server.sendToServerConsole(e);
                        LogUtils.logException(this, e);
                    } catch (IOException e) {
                        server.sendToServerConsole(e);
                        LogUtils.logException(this, e);
                    }
                }
            }
        });

        thread.start();

        String message;
        while (true) {
            try {
                message = queue.take();
                //LogUtils.logMessage(this, "queue.take: " + message);

                String[] messageParts = message.split(" ");

                if (message.startsWith("GET_PROJECTOR_INFO")) {
                    server.sendToAllClients(projectorInUse
                            ? "PROJECTOR_IN_USE"
                            : "PROJECTOR_NOT_IN_USE");
                    continue;
                }

                int gameIndex = Integer.valueOf(messageParts[1]);
                int gameNumber = gameIndex + 1;

                if (message.startsWith("JOIN")) {
                    final GameServer nonRunningGame = server.getNonRunningGame(gameIndex);
                    if (actualGame == null && !isActualGameRunning()
                            && (!nonRunningGame.getIfUsesProjector()
                            || !projectorInUse)
                            && addConnectedClient(gameIndex)) {
                        setActualGame(nonRunningGame);
                        this.sendToAClient("ACCEPT " + gameIndex);
                        server.sendToServerConsole("Klient na portu "
                                + socket.getPort()
                                + " se p�idal k �ek�n� na hru " + gameNumber
                                + " (instance " + nonRunningGame.getInstance()
                                + ") po�et p�ipojen�ch hr��� je nyn� "
                                + (getNumberOfConnectedClients(gameIndex)));
                        server.sendToAllClients("CONNECTED " + gameIndex + " "
                                + getNumberOfConnectedClients(gameIndex));
                    }
                } else if (message.startsWith("LEAVE")) {
                    if (!isActualGameRunning() && actualGame != null
                            && removeConnectedClient(actualGame)) {
                        server.sendToServerConsole("Klient na portu "
                                + socket.getPort()
                                + " ji� ne�ek� na hru " + gameNumber + " (instance "
                                + actualGame.getInstance()
                                + ") po�et p�ipojen�ch hr��� je nyn� "
                                + getNumberOfConnectedClients(gameIndex) + ")");
                        server.sendToAllClients("CONNECTED " + gameIndex + " "
                                + getNumberOfConnectedClients(gameIndex));
                        actualGame = null;
                    }
                } else if (message.startsWith("START")) {
                    if (!server.isStartingGame()) {
                        server.setStartingGame(true);
                        if (actualGame != null && !isActualGameRunning()
                                && actualGame.getIndex() == gameIndex
                                && (!actualGame.getIfUsesProjector() || !projectorInUse)) {
                            if (actualGame.getIfUsesProjector()) {
                                projectorInUse = true;
                                server.sendToSelectedClients("PROJECTOR_IN_USE",
                                        getNotConnectedClients(actualGame));
                            }

                            ControlSystemAnalytics.newActiveGamePool((byte)actualGame.getNumberOfConnectedClients(),actualGame.getInstance(), actualGame.getIndex());
                            int gamePort = actualGame.createGamePort();
                            server.sendToServerConsole("Spou�t�m hru " + gameNumber
                                    + " (instance " + actualGame.getInstance()
                                    + ") na portu " + gamePort);
                            String arguments = String.valueOf(gamePort) + " "
                                    + new IPAddressConfig().getIPAddress();

                            actualGame.setRunning(true);
                            //server.addGameInstance(actualGame);

                            actualGame.setServerProcess(new GameProcess(actualGame.getRunCommandServer(),
                                    arguments, this::serverGameProcessCleanUp));

                            actualGame.startServerProcess();

                            wait(1000);
                            // launch client apps

                            server.sendToSelectedClients("LAUNCH " + gameIndex + " " + gamePort + " "
                                            + ControlSystemAnalytics.getGamePoolId(actualGame.getInstance()),
                                    getConnectedClients(actualGame));

                            server.sendToAllClients("CONNECTED " + actualGame.getIndex() + " 0");
                            if (actualGame.getIfUsesProjector()) {
                                Platform.runLater(() -> {
                                    server.getController().setProjectorWindowAlwaysOnTop(false);
                                });
                            } else {
                                Platform.runLater(() -> {
                                    server.getController().setProjectorWindowAlwaysOnTop(true);
                                });
                            }

                        }
                        server.setStartingGame(false);
                    }
                } else if (message.startsWith("GET_INFO")) {
                    sendToAClient("CONNECTED " + gameIndex + " " + server.getNonRunningGame(gameIndex).getNumberOfConnectedClients());
                } else if (message.startsWith("FINISH")) {
                    server.sendToServerConsole("Klient na portu "
                            + socket.getPort()
                            + " ukon�il hru " + gameNumber + " (instance "
                            + actualGame.getInstance()
                            + ") -> zav�r�m i zbyl� klienty t�to hry");
                    endGame();
                }
            } catch (NullPointerException e) {
                server.sendToServerConsole(e);
                LogUtils.logException(this, e);
            } catch (Exception e) {
                server.sendToServerConsole(e);
                LogUtils.logException(this, e);
            } finally {
            }
        }
    }
}
