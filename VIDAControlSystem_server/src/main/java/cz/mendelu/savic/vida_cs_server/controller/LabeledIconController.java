/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.controller;

import java.io.File;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author david
 */
public class LabeledIconController {

    @FXML
    private VBox root;
    @FXML
    private Label descriptionLabel;
    @FXML
    private ImageView iconImage;

    public void init(String description, String imagePath) {
        this.descriptionLabel.setText(description);
        initImage(imagePath);
        setHoverEffect();
    }

    private void initImage(String imagePath) {
        File file = new File(imagePath);
        Image image = new Image(file.toURI().toString(), 80, 80, true, true);
        this.iconImage.setImage(image);
    }

    private void setHoverEffect() {
        root.setOnMouseEntered(e -> {
            root.setStyle("-fx-background-color:lightblue");
        });

        root.setOnMouseExited(e -> {
            root.setStyle("-fx-background-color:transparent;");
        });
    }

    public VBox getRoot() {
        return root;
    }
}
