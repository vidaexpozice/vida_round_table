/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.model;

import cz.mendelu.savic.vida_cs.model.TransitionFactory;
import cz.mendelu.savic.vida_cs_server.controller.ProjectorWindowController;
import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.animation.Interpolator;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.StrokeTransition;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

/**
 *
 * @author david
 */
public class ProjectorWindowEffects {

    private final Color LIGHT_BLUE = new Color(0.3, 0.3, 1, 1);
    private final Color LIGHT_GREEN = new Color(0.2, 1, 0.2, 1);
    private final Duration COLOR_CHANGE_DURATION = Duration.seconds(30);
    private ProjectorWindowController controller;

    public ProjectorWindowEffects(ProjectorWindowController controller) {
        this.controller = controller;
        createTransitions();
        createColorTransitions();
    }

    private void createTransitions() {
        RotateTransition stackPaneTransition
                = TransitionFactory.createSpinningNode(controller.getCenterStackPane());
        stackPaneTransition.play();

        FadeTransition vidaLogoFadingOutTransition
                = createOneTimeSlowlyFadingOutNode(controller.getVidaLogoImageView());
        vidaLogoFadingOutTransition.onFinishedProperty().set(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controller.generateInfoText();
            }
        });

        SequentialTransition centerNodesTransition = new SequentialTransition(
                createOneTimeSlowlyFadingInNode(controller.getInfoText()),
                createOneTimeSlowlyFadingOutNode(controller.getInfoText()),
                createOneTimeSlowlyFadingInNode(controller.getVidaLogoImageView()),
                vidaLogoFadingOutTransition
        );
        centerNodesTransition.setCycleCount(Transition.INDEFINITE);
        centerNodesTransition.play();
    }

    private void createColorTransitions() {
        ParallelTransition parallelTransition = new ParallelTransition(
                createColorStrokeTransition(controller.getCircle()),
                createColorFillTransition(controller.getInfoText()));
        parallelTransition.setCycleCount(Transition.INDEFINITE);
        parallelTransition.play();
    }

    private SequentialTransition createColorStrokeTransition(Shape shape) {
        StrokeTransition circleTransitionFromYellow = new StrokeTransition(
                COLOR_CHANGE_DURATION, shape, Color.YELLOW, LIGHT_GREEN);
        StrokeTransition circleTransitionFromGreen = new StrokeTransition(
                COLOR_CHANGE_DURATION, shape, LIGHT_GREEN, Color.AQUA);
        StrokeTransition circleTransitionFromAqua = new StrokeTransition(
                COLOR_CHANGE_DURATION, shape, Color.AQUA, LIGHT_BLUE);
        StrokeTransition circleTransitionFromBlue = new StrokeTransition(
                COLOR_CHANGE_DURATION, shape, LIGHT_BLUE, Color.MAGENTA);
        StrokeTransition circleTransitionFromMagenta = new StrokeTransition(
                COLOR_CHANGE_DURATION, shape, Color.MAGENTA, Color.RED);
        StrokeTransition circleTransitionFromRed = new StrokeTransition(
                COLOR_CHANGE_DURATION, shape, Color.RED, Color.YELLOW);

        circleTransitionFromYellow.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromGreen.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromAqua.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromBlue.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromMagenta.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromRed.setInterpolator(Interpolator.LINEAR);

        SequentialTransition sequentialTransition = new SequentialTransition(
                circleTransitionFromYellow, circleTransitionFromGreen, circleTransitionFromAqua,
                circleTransitionFromBlue, circleTransitionFromMagenta, circleTransitionFromRed);

        return sequentialTransition;
    }

    private SequentialTransition createColorFillTransition(Shape shape) {
        FillTransition circleTransitionFromYellow = new FillTransition(
                COLOR_CHANGE_DURATION, shape, Color.YELLOW, LIGHT_GREEN);
        FillTransition circleTransitionFromGreen = new FillTransition(
                COLOR_CHANGE_DURATION, shape, LIGHT_GREEN, Color.AQUA);
        FillTransition circleTransitionFromAqua = new FillTransition(
                COLOR_CHANGE_DURATION, shape, Color.AQUA, LIGHT_BLUE);
        FillTransition circleTransitionFromBlue = new FillTransition(
                COLOR_CHANGE_DURATION, shape, LIGHT_BLUE, Color.MAGENTA);
        FillTransition circleTransitionFromMagenta = new FillTransition(
                COLOR_CHANGE_DURATION, shape, Color.MAGENTA, Color.RED);
        FillTransition circleTransitionFromRed = new FillTransition(
                COLOR_CHANGE_DURATION, shape, Color.RED, Color.YELLOW);

        circleTransitionFromYellow.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromGreen.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromAqua.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromBlue.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromMagenta.setInterpolator(Interpolator.LINEAR);
        circleTransitionFromRed.setInterpolator(Interpolator.LINEAR);

        SequentialTransition sequentialTransition = new SequentialTransition(
                circleTransitionFromYellow, circleTransitionFromGreen, circleTransitionFromAqua,
                circleTransitionFromBlue, circleTransitionFromMagenta, circleTransitionFromRed);

        return sequentialTransition;
    }

    private FadeTransition createOneTimeSlowlyFadingInNode(Node node) {
        return createOneTimeSlowlyFadingNode(node, 0, 1, Interpolator.EASE_OUT);
    }

    private FadeTransition createOneTimeSlowlyFadingOutNode(Node node) {
        return createOneTimeSlowlyFadingNode(node, 1, 0, Interpolator.EASE_IN);
    }

    private FadeTransition createOneTimeSlowlyFadingNode(Node node, double fromValue,
            double toValue, Interpolator interpolator) {
        FadeTransition transition
                = new FadeTransition(Duration.seconds(4), node);
        transition.setFromValue(fromValue);
        transition.setToValue(toValue);
        transition.setInterpolator(interpolator);

        return transition;
    }
}
