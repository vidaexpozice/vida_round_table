/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.controller;

import cz.mendelu.savic.vida_cs_server.model.GameServer;
import cz.mendelu.savic.vida_cs_server.model.ProjectorWindowEffects;
import java.io.File;
import java.util.Random;
import javafx.fxml.FXML;
import javafx.geometry.Dimension2D;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author david
 */
public class ProjectorWindowController {

    private static final Dimension2D WINDOW_SIZE = new Dimension2D(1024, 768);

    @FXML
    private ImageView vidaLogoImageView;
    @FXML
    private Circle circle;
    @FXML
    private StackPane root;
    @FXML
    private StackPane centerStackPane;
    @FXML
    private Text infoText;
    private Stage projectorStage;
    private Server server;
    private Font infoTextDefaultFont;
    private Font infoTextSmallFont;

    public void init(Stage projectorStage, Server server) {
        this.projectorStage = projectorStage;
        this.server = server;
        setImages();
        initCircle();
        root.setPrefWidth(WINDOW_SIZE.getWidth());
        root.setPrefHeight(WINDOW_SIZE.getHeight());
        new ProjectorWindowEffects(this);
        infoTextDefaultFont = new Font("Arial Regular", 100);
        infoTextSmallFont = new Font("Arial Regular", 70);
        infoText.setFont(infoTextDefaultFont);
    }

    private void setImages() {
        // set crossButtonImage
        File file = new File("../resources/images/cross.png");
        Image image = new Image(file.toURI().toString(), 50, 50, true, true);

        // set vidaLogoImage
        File file2 = new File("../resources/images/VIDA_logo.png");
        Image image2 = new Image(file2.toURI().toString(), 520, 520, true, true);
        vidaLogoImageView.setStyle("-fx-background-color: transparent;");
        vidaLogoImageView.setImage(image2);
    }

    private void initCircle() {
        File file = new File("../resources/images/background_original.jpg");
        Image image = new Image(file.toURI().toString(), 1300, 1300, false, true);
        ImagePattern pattern = new ImagePattern(image);
        circle.setFill(pattern);
    }

    public void handleExit() {
        projectorStage.close();
    }

    public static Dimension2D getWindowSize() {
        return WINDOW_SIZE;
    }

    public ImageView getVidaLogoImageView() {
        return vidaLogoImageView;
    }

    public Circle getCircle() {
        return circle;
    }

    public StackPane getCenterStackPane() {
        return centerStackPane;
    }

    public Text getInfoText() {
        return infoText;
    }

    public String getPlayerWordFormInCzech(int numberOfPlayers, boolean isAccusative) {
        if (numberOfPlayers == 1) {
            return isAccusative ? "hr��e" : "hr��";
        } else if (numberOfPlayers >= 2 && numberOfPlayers <= 4) {
            return isAccusative ? "hr��e" : "hr��i";
        } else {
            return "hr���";
        }
    }

    public void generateInfoText() {
        GameServer gameInWaitingStatus = null;
        for (GameServer game : server.getGames()) {
            if (game.isWaitingForMorePlayers()) {
                gameInWaitingStatus = game;
                break;
            }
        }

        if (gameInWaitingStatus != null) {
            int missingPlayers = gameInWaitingStatus.getNumberOfPlayersMin()
                    - gameInWaitingStatus.getNumberOfConnectedClients();
            infoText.setFont(infoTextSmallFont);
                    
            switch (new Random().nextInt(3)) {
                case 0:
                    infoText.setText("Chyb� n�m\nje�t� " + missingPlayers + " " + getPlayerWordFormInCzech(missingPlayers, false) + "\npro spu�t�n� hry\n" + gameInWaitingStatus.getName() + ".\nP�idej se taky!");
                    break;
                case 1:
                    infoText.setText("Hej!\nPot�ebujeme\nje�t� " + missingPlayers + " " + getPlayerWordFormInCzech(missingPlayers, true) + "\nke h�e " + gameInWaitingStatus.getName() + ".\nPoj� se p�idat :)");
                    break;
                case 2:
                    infoText.setText("Poj� si zahr�t hru\n" + gameInWaitingStatus.getName() + ".\nU� n�m chyb� jen\n" + missingPlayers + " " + getPlayerWordFormInCzech(missingPlayers, false) + ".");
                    break;
            }
        } else {
            infoText.setFont(infoTextDefaultFont);
            infoText.setText("Vyber hru");
        }
    }
}
