/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_server.model;

import cz.mendelu.savic.vida_cs_server.controller.ClientHandler;
import cz.mendelu.savic.vida_cs.model.Game;
import cz.mendelu.savic.vida_cs.model.GameProcess;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author david
 */
public class GameServer extends Game {

    private ArrayList<ClientHandler> connectedClients;
    private boolean running;
    private int instance;
    private GameProcess serverProcess;

    public GameServer(Game game, int instance) {
        this(game.getName(), game.getDescription(), game.getScreenPath(), game.getIfUsesProjector(),
                game.getNumberOfPlayers(), game.getRunCommandClient(), game.getRunCommandServer(),
                game.getIndex(), instance);
    }

    public GameServer(String name, String description, String screenPath, boolean usesProjector,
            String numberOfPlayers, String runCommandClient, String runCommandServer,
            int gameIndex, int instance) {
        super(name, description, screenPath, usesProjector, numberOfPlayers, runCommandClient,
                runCommandServer, gameIndex);
        this.connectedClients = new ArrayList<>();
        this.running = false;
        this.instance = instance;
    }

    /**
     * This method clears list of connected clients for this game, to each client sets actualGame
     * to none and projectorInUse to false.
     */
    public void clearConnectedClients() {
        for (ClientHandler connectedClient : connectedClients) {
            connectedClient.setActualGame(null);
            connectedClient.setProjectorInUse(false);
        }
        connectedClients.clear();
    }

    public synchronized boolean addConnectedClient(ClientHandler client) {
        if (connectedClients.size() < getNumberOfPlayersMax()) {
            return connectedClients.add(client);
        }

        return false;
    }

    public synchronized boolean removeConnectedClient(ClientHandler client) {
        if (connectedClients.size() > 0) {
            return connectedClients.remove(client);
        }

        return false;
    }

    public synchronized int getNumberOfConnectedClients() {
        return connectedClients.size();
    }

    public ArrayList<ClientHandler> getConnectedClients() {
        return connectedClients;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public void setServerProcess(GameProcess serverProcess) {
        this.serverProcess = serverProcess;
    }

    public void startServerProcess() throws IOException {
        if (serverProcess != null) {
            serverProcess.start();
        }
    }

    public void endProcess() {
        if (serverProcess != null) {
            serverProcess.end();
        }
    }

    public synchronized int getInstance() {
        return instance;
    }

    public int createGamePort() {
        return 7000 + getNumber() + (getInstance() * 100);
    }
    
    public boolean isWaitingForMorePlayers() {
        return (connectedClients.size() > 0 && connectedClients.size() < getNumberOfPlayersMin());
    }
}
