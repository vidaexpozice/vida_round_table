/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_client.controller;

import cz.mendelu.savic.vida_cs_snake.controller.EndGameListener;
import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake_client.MainLoader;
import java.io.IOException;
import java.util.List;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author david
 */
public class ClientWindowController {

    /**
     * StackPane holding start scene and board scene.
     */
    @FXML
    private StackPane root;
    /**
     * Start scene for choosing color of snake before the game starts.
     */
    @FXML
    private VBox rootStart;
    /**
     * Board scene. The main scene with game board, which is shown after the game is started.
     */
    @FXML
    private FlowPane rootGame;
    @FXML
    private Label countdownLabel;

    private GameBoardController gameBoardController;
    private StartupColorPickerController colorPickerController;
    private Parent gameBoardRoot;
    private int numberOfSnakes;

    public void initColorPicker(MainLoader mainLoader) throws IOException {
        root.getChildren().remove(rootGame);

        FXMLLoader colorPickerLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_snake_client/view/StartupColorPicker.fxml"));
        Parent colorPickerRoot = colorPickerLoader.load();
        colorPickerController = colorPickerLoader.getController();
        colorPickerController.init(0.2, 18);
        rootStart.getChildren().add(1, colorPickerRoot);
    }

    public int getNumberOfSnakes() {
        return numberOfSnakes;
    }

    public void initGame(MainLoader mainLoader, int backgroundNumber, int numberOfSnakes,
                         List<Color> snakeColors, Client client, int playerSnakeNumber) throws IOException {
        this.numberOfSnakes = numberOfSnakes;
        Platform.runLater(() -> {
                    try {
                        root.getChildren().clear();
                        root.getChildren().add(rootGame);

                        FXMLLoader gameBoardLoader = new FXMLLoader(
                                getClass().getResource("/cz/mendelu/savic/vida_cs_snake/view/GameBoard.fxml"));

                        gameBoardRoot = gameBoardLoader.load();
                        gameBoardController = gameBoardLoader.getController();
                        gameBoardController.init(mainLoader, backgroundNumber, numberOfSnakes, snakeColors, 768);
                        gameBoardController.addListener(end -> client.endGame());
                        rootGame.getChildren().add(gameBoardRoot);

                        GameBoardListener eventListener = new GameBoardListener(gameBoardRoot, client);
                        mainLoader.setFocus(gameBoardRoot);

                        gameBoardController.highlightSnake(playerSnakeNumber - 1);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
        );
    }

    public Parent getGameBoardRoot() {
        return gameBoardRoot;
    }

    public StackPane getRoot() {
        return root;
    }

    public GameBoardController getGameBoardController() {
        return gameBoardController;
    }

    public void updateCountdownLabel(String text) {
        Platform.runLater(() -> {
            countdownLabel.setText("Hra za�ne za " + text);
        });
    }

    public void emptyCountdownLabel() {
        Platform.runLater(() -> {
            countdownLabel.setText("");
        });
    }

    public Color getSnakeColor() {
        return colorPickerController.getActualColor();
    }
}
