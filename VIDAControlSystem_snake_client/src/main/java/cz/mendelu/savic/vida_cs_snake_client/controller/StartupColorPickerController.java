/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_client.controller;

import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * FXML Controller class
 *
 * @author david
 */
public class StartupColorPickerController {

    @FXML
    private TilePane root;

    private double saturationOffset;
    private int colorCount;
    private ArrayList<Color> colors;
    private Color actualColor;
    private List<StartupColorPickerListener> listeners
            = new ArrayList<>();

    public void init(double saturationOffset, int colorCount) {
        this.saturationOffset = saturationOffset;
        this.colorCount = colorCount;
        initColors();

        ToggleGroup colorButtonGroup = new ToggleGroup();
        colorButtonGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) -> {
            handleColorButton((ToggleButton) newValue);
        });

        for (Color color : colors) {
            ToggleButton colorButton = new ToggleButton();
            String style = "-fx-background-color:#" +
                    color.toString().substring(2) + ";";
            colorButton.setStyle(style);
            colorButton.setPrefWidth(40);
            colorButton.setPrefHeight(40);
            colorButton.setToggleGroup(colorButtonGroup);
            colorButton.selectedProperty().addListener(
                    (ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
                if (newValue) {
                    colorButton.setStyle(style + "-fx-border-color:black;-fx-border-radius:3;-fx-border-width:5;");
                } else {
                    colorButton.setStyle(style + "-fx-border-width:0");
                }
            });
            root.getChildren().add(colorButton);
        }
        
        Random random = new Random(System.currentTimeMillis());
        int randomBackgroundNumber = random.nextInt(root.getChildren().size());
        
        colorButtonGroup.getToggles().get(randomBackgroundNumber).setSelected(true);
    }

    private ArrayList<Color> getColors() {
        if (colors == null) {
            initColors();
        }

        return colors;
    }

    private void handleColorButton(ToggleButton button) {
        if (button != null) {
            actualColor = Color.web(button
                    .getStyle()
                    .substring(button
                            .getStyle()
                            .indexOf("-fx-background-color:") + "-fx-background-color:".length())
                    .substring(0, 9));
            for (StartupColorPickerListener listener : listeners) {
                listener.colorChanged(actualColor);
            }
        }
    }

    private void initColors() {
        colors = new ArrayList<>();

        for (int i = 0; i < colorCount; i++) {
            // sort colors into 6 classes according to RGB color channels
            // (RGB: 100, 110, 010, 011, 001, 101)
            int spectrumIndex = (int) (6 * ((double) i / colorCount));
            double offset = 6 * ((double) i / colorCount) - spectrumIndex;
            double fullColor = 1 - saturationOffset;
            double emptyColor = saturationOffset;
            double offsetColor = (fullColor - saturationOffset) * offset;

            switch (spectrumIndex) {
                case 0:
                    colors.add(new Color(fullColor, offsetColor, emptyColor, 1));
                    break;
                case 1:
                    colors.add(new Color(1 - offsetColor, fullColor, emptyColor, 1));
                    break;
                case 2:
                    colors.add(new Color(emptyColor, fullColor, offsetColor, 1));
                    break;
                case 3:
                    colors.add(new Color(emptyColor, 1 - offsetColor, fullColor, 1));
                    break;
                case 4:
                    colors.add(new Color(offsetColor, emptyColor, fullColor, 1));
                    break;
                case 5:
                    colors.add(new Color(fullColor, emptyColor, 1 - offsetColor, 1));
                    break;
                case 6:
                    colors.add(new Color(fullColor, emptyColor, emptyColor, 1));
            }
        }
    }

    public TilePane getRoot() {
        return root;
    }

    public void addListener(StartupColorPickerListener listener) {
        listeners.add(listener);
    }

    public Color getActualColor() {
        return actualColor;
    }
}
