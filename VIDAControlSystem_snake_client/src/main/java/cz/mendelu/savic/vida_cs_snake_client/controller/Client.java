/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_client.controller;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake.model.DirectionEnum;
import cz.mendelu.savic.vida_cs_snake.controller.ColorUtils;
import cz.mendelu.savic.vida_cs_snake_client.MainLoader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.paint.Color;

/**
 * <p>Client for communicating with server and sending following message signals:</p>
 * 
 * <p><b>Client -> Server:</b><br>
 *   <ul>
 *   <li>MY_COLOR_IS C - Client is ready for game and its snake will have color C.</li>
 *   <li>MOVE_ME D - Move my snake to direction D.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Server -> Client:</b>
 *   <ul>
 *   <li>COUNTDOWN_COLOR T - Client, you have T seconds to choose snake color.</li>
 *   <li>COUNTDOWN_START T - Client, the game starts (snakes will start to move) at T seconds.</li>
 *   <li>SEND_ME_COLOR - Client, send me the color you've chosen.</li>
 *   <li>INIT_BOARD INIT_STRING - Clients, init the board according to information
 *       from INIT_STRING.</li>
 *   <li>MOVE DIRECTIONS_STRING - Move all snakes according to DIRECTIONS_STRING</li>
 *   <li>ADD_FOOD P C - Add food to position P with color C.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Where:</b>
 *   <ul>
 *   <li>T is a text, usually number of seconds to countdown or message
 *      (used for countdown = 0 - e.g. Start!),</li>
 *   <li>X is number of snake,</li>
 *   <li>D is one of direction: UP, RIGHT, DOWN, LEFT,</li>
 *   <li>C is color represented by HEX web value, for example #ff7f50.</li>
 *   <li>P specifies place in a board. It is represented by two numbers separated by hash.</li>
 *   <li>INIT_STRING is string with information about selected background, player snake number,
 *       number of snakes and colors of these snakes. It is made of number representing chosen background,
 *       followed by hash and six symbols for each snake representing HEX color of the snake.
 *       For example 3#1#ff7f50#ff7fff for background number 3, 2 snakes with the colors
 *       according to HEX values and player plays for snake number 1.</li>
 *   <li>DIRECTION_STRING is string with information about all snakes directions.
 *       It is made of letters for each snake representing their current directions.
 *       The letters are U (for up), D (for down), R (for right) and L (for left).
 *       For example DULLR is information of about directions for five snakes.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Sequence to start the game:</b><br>
 *   COUNTDOWN_COLOR -> SEND_ME_COLOR -> MY_COLOR_IS -> INIT_BOARD -> COUNTDOWN_START -> MOVE
 * </p>
 */
public class Client extends Thread {
    private final ClientWindowController clientWindowController;
    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private boolean connected = false;
    private MainLoader mainLoader;
    
    public Client(MainLoader mainLoader, ClientWindowController clientWindowController) {
        this.mainLoader = mainLoader;
        this.clientWindowController = clientWindowController;
    }
    
    /**
     * Metoda pro p�ipojen� k serveru.
     * 
     * @param ipAddress IP adresa serveru
     * @return zdali se poda�ilo p�ipojit
     */
    public boolean connect(String ipAddress, int port) {
        if (ipAddress == null) {
            return false;
        }

        try {
            socket = new Socket(ipAddress, port);
            socket.setKeepAlive(true);
            socket.setSoTimeout(Integer.MAX_VALUE);
            input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(
                    socket.getOutputStream(), true);

            start();
            connected = true;
            
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean isConnected() {
        return connected;
    }

    /**
     * P�et�en� metoda run, kter� p�ij�m� zpr�vy od serveru.
     */
    @Override
    public void run() {
        String response;

        while (true) {
            try {
                response = input.readLine();
                String[] responseParts = response.split(" ");

                if (response.startsWith("COUNTDOWN_COLOR")) {
                    clientWindowController.updateCountdownLabel(responseParts[1]);
                } else if (response.startsWith("COUNTDOWN_START")) {
                    clientWindowController
                            .getGameBoardController()
                            .showCountdown(Integer.valueOf(responseParts[1]));
                } else if (response.startsWith("SEND_ME_COLOR")) {
                    clientWindowController.emptyCountdownLabel();
                    sendColor(clientWindowController.getSnakeColor());
                } else if (response.startsWith("INIT_BOARD")) {
                    String[] infoParts = responseParts[1].split("#");
                    int backgroundNumber = Integer.valueOf(infoParts[0]);
                    int numberOfSnakes = infoParts.length - 2;
                    List<Color> snakeColors = new ArrayList<>();
                    for (int i = 2; i < infoParts.length; i++) {
                        snakeColors.add(Color.web("#" + infoParts[i]));
                    }
                    clientWindowController.initGame(mainLoader, backgroundNumber, numberOfSnakes,
                            snakeColors, this, Integer.parseInt(infoParts[1]));
                } else if (response.startsWith("MOVE")) {
                    char[] directionsCharArray = responseParts[1].toCharArray();
                    for (int i = 0; i < directionsCharArray.length; i++) {
                        switch (directionsCharArray[i]) {
                            case 'U':
                                clientWindowController.getGameBoardController().setDirection(i, DirectionEnum.UP);
                                break;
                            case 'D':
                                clientWindowController.getGameBoardController().setDirection(i, DirectionEnum.DOWN);
                                break;
                            case 'L':
                                clientWindowController.getGameBoardController().setDirection(i, DirectionEnum.LEFT);
                                break;
                            case 'R':
                                clientWindowController.getGameBoardController().setDirection(i, DirectionEnum.RIGHT);
                                break;
                        }
                    }
                    clientWindowController.getGameBoardController().moveSnakes();
                } else if (response.startsWith("ADD_FOOD")) {
                    Color color = Color.web(responseParts[2]);
                    clientWindowController
                            .getGameBoardController()
                            .addFood(new Point2D(
                                    Double.valueOf(responseParts[1].split("#")[0]),
                                    Double.valueOf(responseParts[1].split("#")[1])),
                                    new Color(color.getRed(), color.getGreen(), color.getBlue(),
                                            0.6));
                }
            } catch (SocketException e) {
                // server disconnected
                try {
                    socket.close();
                    Platform.exit();
                    System.exit(0);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Parent getGameBoardRoot() {
        return clientWindowController.getGameBoardRoot();
    }

    public void moveSnake(DirectionEnum direction) {
        output.println("MOVE_ME " + direction.name());
    }

    public void sendColor(Color color) {
        output.println("MY_COLOR_IS " + ColorUtils.getWebStringFromColor(color));
    }

    public void endGame() {
        GameBoardController gameBoardController = clientWindowController.getGameBoardController();
        String winnerColorName = gameBoardController.getAliveColorName();
        if (winnerColorName == null) {
            if (clientWindowController.getNumberOfSnakes() == 1) {
                gameBoardController.showEndMessage("Prohr�l jsi");
            } else {
                gameBoardController.showEndMessage("Nikdo nevyhr�l");
            }
        } else {
            gameBoardController.showEndMessage("Vyhr�l " + winnerColorName + " had");
        }
        /*try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Platform.exit();
        System.exit(0);*/
    }
}
