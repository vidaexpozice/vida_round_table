/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_client;

import cz.mendelu.savic.vida_cs_snake_client.controller.Client;
import cz.mendelu.savic.vida_cs_snake_client.controller.ClientWindowController;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class MainLoader extends Application {

    private static String ipAddress;
    private static int port;
    private Stage primaryStage;
    private Client client;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setFullScreenExitHint("");
        this.primaryStage.setFullScreen(true);

        FXMLLoader clientWindowLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_snake_client/view/ClientWindow.fxml"));

        Parent clientWindowRoot = clientWindowLoader.load();

        ClientWindowController clientWindowController = clientWindowLoader.getController();
        // TODO
        clientWindowController.initColorPicker(this);
        
        client = new Client(this, clientWindowController);
        client.connect(ipAddress, port);
        if (!client.isConnected()) {
            System.out.println("Client couldn't connect to IP address " + ipAddress +
                    " and port " + port);
            Platform.exit();
            System.exit(0);
        }

        this.primaryStage.setScene(new Scene(clientWindowRoot));
        this.primaryStage.setTitle("SnakeXSnake");
        this.primaryStage.setMinWidth(1024);
        this.primaryStage.setMinHeight(768);
        //this.primaryStage.setFullScreen(true);
        this.primaryStage.setOnCloseRequest(event -> {
            //server.stopThreads();
            Platform.exit();
            System.exit(0);
        });
        //clientWindowRoot.requestFocus();
        // TODO
        //clientWindowController.getGameBoardRoot().requestFocus();
        this.primaryStage.show();
    }

    public void setFocus(Parent parent) {
        parent.requestFocus();
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            return;
        }

        port = Integer.valueOf(args[0]);
        ipAddress = args[1];

        launch(args);
    }

}
