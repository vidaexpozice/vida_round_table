/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_client.controller;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake.model.DirectionEnum;
import cz.mendelu.savic.vida_cs_snake.model.Position;
import javafx.scene.Parent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;

/**
 *
 * @author david
 */
public class GameBoardListener {
    private Parent gameBoardRoot;
    private Client client;
    private Position lastTouchPoint = null;
    private boolean touched = false;

    public GameBoardListener(Parent gameBoardRoot, Client client) {
        this.client = client;
        setEvents(gameBoardRoot);
    }

    public void handleTouchPressed(TouchEvent event) {
        if (!touched) {
            lastTouchPoint = new Position(event.getTouchPoint().getX(), event.getTouchPoint().getY());
        }
        touched = true;
    }

    public void handleTouchReleased(TouchEvent event) {
        Position releaseTouchPoint = new Position(event.getTouchPoint().getX(), event.getTouchPoint().getY());
        double diffX = releaseTouchPoint.getX() - lastTouchPoint.getX();
        double diffY = releaseTouchPoint.getY() - lastTouchPoint.getY();
        boolean xGreater = Math.abs(diffX) > Math.abs(diffY);

        if (xGreater) {
            if (diffX < 0) {
                moveSnakeLeft();
            } else {
                moveSnakeRight();
            }
        } else {
            if (diffY < 0) {
                moveSnakeUp();
            } else {
                moveSnakeDown();
            }
        }

        touched = false;
    }

    private void setEvents(Parent gameBoardRoot) {
        this.gameBoardRoot = gameBoardRoot;
        gameBoardRoot.setOnKeyPressed((KeyEvent event) -> {
            keyPressed(event);
        });

        gameBoardRoot.setOnTouchPressed(this::handleTouchPressed);
        gameBoardRoot.setOnTouchReleased(this::handleTouchReleased);
    }

    public void moveSnakeUp() {
        client.moveSnake(DirectionEnum.UP);
    }

    public void moveSnakeRight() {
        client.moveSnake(DirectionEnum.RIGHT);
    }

    public void moveSnakeDown() {
        client.moveSnake(DirectionEnum.DOWN);
    }

    public void moveSnakeLeft() {
        client.moveSnake(DirectionEnum.LEFT);
    }

    public void keyPressed(KeyEvent ke) {
        switch (ke.getCode()) {
            case UP:
                moveSnakeUp();
                break;
            case RIGHT:
                moveSnakeRight();
                break;
            case DOWN:
                moveSnakeDown();
                break;
            case LEFT:
                moveSnakeLeft();
                break;
        }
    }
}
