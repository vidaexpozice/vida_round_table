package cz.mendelu.savic.vida_cs_snake_client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.input.TouchPoint;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

/**
 * Created by david on 04.06.2017.
 */
public class SwipeTestApp extends Application {
    public void handleSwipe(SwipeEvent event) {
        System.out.println(event.getX() + "x" + event.getY());
        System.out.println(event.getSceneX() + "x" + event.getSceneY());
        System.out.println(event.getScreenX() + "x" + event.getScreenY());
        System.out.println("-----------------");

        event.getTouchCount();
    }

    public void handleTouchPressed(TouchEvent event) {
        System.out.println("pressed: " + event.getTouchPoint().getX() + "x" + event.getTouchPoint().getY());
    }

    public void handleTouchReleased(TouchEvent event) {
        System.out.println("released: " + event.getTouchPoint().getX() + "x" + event.getTouchPoint().getY());
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        final VBox vBox = new VBox();
        final Scene scene = new Scene(vBox);

        scene.setOnSwipeUp(this::handleSwipe);
        scene.setOnSwipeRight(this::handleSwipe);
        scene.setOnSwipeDown(this::handleSwipe);
        scene.setOnSwipeLeft(this::handleSwipe);
        scene.setOnTouchPressed(this::handleTouchPressed);
        scene.setOnTouchReleased(this::handleTouchReleased);

        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
        primaryStage.setFullScreenExitHint("");
        primaryStage.setWidth(1000);
        primaryStage.setHeight(700);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
