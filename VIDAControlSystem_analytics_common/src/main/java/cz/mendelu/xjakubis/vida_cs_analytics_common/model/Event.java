package cz.mendelu.xjakubis.vida_cs_analytics_common.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name="event")
public class Event implements Serializable {

    @Id
    @GeneratedValue()
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gameIndex")
    private Game game;

    @ManyToOne
    @JoinColumn(name = "game_pool_id", updatable = false)
    private GamePool gamePool;

    private String category;

    private String resource;

    private String action;

    private String gamerUuid;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDateTime;

    public Event(Game game, GamePool gamePool, String category, String resource, String action, LocalDateTime createDateTime, String gamerUuid) {
        this(game,category,resource,action,createDateTime);
        this.gamePool = gamePool;
        this.gamerUuid = gamerUuid;
    }

    public Event(Game game, String category, String resource, String action, LocalDateTime createDateTime) {
        this(category, resource,action, createDateTime);
        this.game = game;
    }

    public Event(String category, String resource, String action, LocalDateTime createDateTime) {
        this.category = category;
        this.resource = resource;
        this.action = action;
        this.createDateTime = createDateTime;
    }

    public Event() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public GamePool getGamePool() {
        return gamePool;
    }

    public void setGamePool(GamePool gamePool) {
        this.gamePool = gamePool;
    }

    public String getGamerUuid() {
        return gamerUuid;
    }

    public void setGamerUuid(String gamerUuid) {
        this.gamerUuid = gamerUuid;
    }
}
