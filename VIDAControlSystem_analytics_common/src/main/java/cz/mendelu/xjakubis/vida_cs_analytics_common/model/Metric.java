package cz.mendelu.xjakubis.vida_cs_analytics_common.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity()
@Table(name = "metric")
public class Metric implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "metric_id")
    private Long id;

    private String displayName;

    @Column(columnDefinition = "TEXT")
    private String query;

    public Metric(String displayName, String query) {
        this.displayName = displayName;
        this.query = query;
    }

    public Metric() { }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
