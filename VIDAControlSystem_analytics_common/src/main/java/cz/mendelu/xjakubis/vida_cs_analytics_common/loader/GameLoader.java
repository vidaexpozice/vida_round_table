package cz.mendelu.xjakubis.vida_cs_analytics_common.loader;
import cz.mendelu.savic.vida_cs.model.FileHandler;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Game;

import java.io.IOException;
import java.util.ArrayList;

public class GameLoader {
    private final String FILE_PATH = "../resources/games.csv"; //TODO nastavit podla umiestnenia
    private final FileHandler fileHandler;

    public GameLoader() throws IOException {
        fileHandler = new FileHandler(FILE_PATH);
    }

    public ArrayList<Game> getGames() throws IOException {
        ArrayList<Game> games = new ArrayList();
        this.fileHandler.openReader();
        int index = 0;
        this.fileHandler.readLine();

        String line;
        while((line = this.fileHandler.readLine()) != null) {
            String[] lineParts = line.split(";");
            games.add(new Game(lineParts[0], lineParts[1], lineParts[2], Boolean.valueOf(lineParts[3]), lineParts[4], lineParts[5], lineParts[6], index));
            ++index;
        }

        this.fileHandler.closeReader();
        return games;
    }
}