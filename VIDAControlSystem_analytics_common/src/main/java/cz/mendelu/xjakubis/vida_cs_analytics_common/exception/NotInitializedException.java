package cz.mendelu.xjakubis.vida_cs_analytics_common.exception;

public class NotInitializedException extends RuntimeException {

    public NotInitializedException() {
        super("The Analytics module is not properly initialized. Please call Analytics.initialized().");
    }
}
