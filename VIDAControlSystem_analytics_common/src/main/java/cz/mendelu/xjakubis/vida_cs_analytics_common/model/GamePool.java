package cz.mendelu.xjakubis.vida_cs_analytics_common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="game_pool")
public class GamePool implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "game_pool_id")
    private Long id;
    private LocalDateTime createDateTime;
    private LocalDateTime endDateTime;
    private byte numberOfClients;
    private boolean finished;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gameIndex")
    private Game game;


    @OneToMany( mappedBy = "gamePool" )
    private List<Event> events;

    public GamePool() {}

    public GamePool(Long id) {
        this.id = id;
    }

    public GamePool(LocalDateTime createDateTime, byte numberOfClients, Game game) {
        this.createDateTime = createDateTime;
        this.numberOfClients = numberOfClients;
        this.game = game;
    }

    public GamePool(LocalDateTime createDateTime, byte numberOfClients, Game game, List<Event> events ) {
        this(createDateTime, numberOfClients, game);
        this.events = events;
    }

    public byte getNumberOfClients() {
        return numberOfClients;
    }

    public void setNumberOfClients(byte numberOfClients) {
        this.numberOfClients = numberOfClients;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
