package cz.mendelu.xjakubis.vida_cs_analytics_common.helper;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.GamePool;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Game;
import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;

import java.time.LocalDateTime;
import java.util.concurrent.Future;

class GamePoolHelper {

    public static Future<GamePool> newActiveGamePool(byte numberOfClients, int gameId) {
        GamePool gamePool = new GamePool(LocalDateTime.now(), numberOfClients, new Game(gameId));
        return GameAnalytics.getRestService().at("gamePool", "add").sendPost(gamePool, GamePool.class);
    }

    public static void stopGamePool(GamePool gamePool){
        if(gamePool!= null){
            gamePool.setEndDateTime(LocalDateTime.now());
            GameAnalytics.getRestService().at("gamePool", "add").sendPost(gamePool, GamePool.class);
        }
    }
}
