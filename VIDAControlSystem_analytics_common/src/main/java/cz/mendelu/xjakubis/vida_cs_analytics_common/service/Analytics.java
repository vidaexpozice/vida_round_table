package cz.mendelu.xjakubis.vida_cs_analytics_common.service;

import cz.mendelu.xjakubis.vida_cs_analytics_common.helper.EventHelper;
import org.springframework.boot.web.client.RestTemplateBuilder;

public class Analytics {

    public static AnalyticsService getRestService(){
        return new AnalyticsService(new RestTemplateBuilder());
    }

    public static void newEvent(String category, String resource, String action){
        EventHelper.newEvent(category, resource, action);
    }
}
