package cz.mendelu.xjakubis.vida_cs_analytics_common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "game")
public class Game implements Serializable {
    @Id
    @Column(name = "game_id")
    private int gameIndex;

    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column(length=1000)
    private String screenPath;

    private boolean usesProjector;

    private String numberOfPlayers;

    @Column(length=1000)
    private String runCommandClient;

    @Column(length=1000)
    private String runCommandServer;

    //Spring bug workaround
    @JsonIgnore
    @OneToMany(
            mappedBy = "game",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    private List<GamePool> gamePools;

    public Game() {
    }

    public Game(String name, String description, String screenPath, boolean usesProjector, String numberOfPlayers, String runCommandClient, String runCommandServer, int gameIndex) {
        this.name = name;
        this.description = description;
        this.screenPath = screenPath;
        this.usesProjector = usesProjector;
        this.numberOfPlayers = numberOfPlayers;
        this.runCommandClient = runCommandClient;
        this.runCommandServer = runCommandServer;
        this.gameIndex = gameIndex;
    }

    public Game(int gameIndex) {
        this.gameIndex = gameIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScreenPath() {
        return screenPath;
    }

    public void setScreenPath(String screenPath) {
        this.screenPath = screenPath;
    }

    public boolean isUsesProjector() {
        return usesProjector;
    }

    public void setUsesProjector(boolean usesProjector) {
        this.usesProjector = usesProjector;
    }

    public String getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public void setNumberOfPlayers(String numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;
    }

    public String getRunCommandClient() {
        return runCommandClient;
    }

    public void setRunCommandClient(String runCommandClient) {
        this.runCommandClient = runCommandClient;
    }

    public String getRunCommandServer() {
        return runCommandServer;
    }

    public void setRunCommandServer(String runCommandServer) {
        this.runCommandServer = runCommandServer;
    }

    public int getGameIndex() {
        return gameIndex;
    }

    public void setGameIndex(int gameIndex) {
        this.gameIndex = gameIndex;
    }

    public List<GamePool> getGamePools() {
        return gamePools;
    }

    public void setGamePools(List<GamePool> gamePooles) {
        this.gamePools = gamePooles;
    }
}
