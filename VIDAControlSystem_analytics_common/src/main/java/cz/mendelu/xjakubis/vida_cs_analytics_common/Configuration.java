package cz.mendelu.xjakubis.vida_cs_analytics_common;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.Properties;

public class Configuration {

    private static Configuration single_instance = null;
    private Properties appProps;
    private static String PATH = "../resources/analytics.properties";

    private Configuration() {
        try {
            appProps = PropertiesLoaderUtils.loadProperties(
                    new FileSystemResource(PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getProperty(String key){
        return appProps.getProperty(key);
    }

    public static Configuration getInstance() {
        if (single_instance == null)
            single_instance = new Configuration();

        return single_instance;
    }

    public static void setConfigurationPath(String path){
        PATH = path;
    }
}
