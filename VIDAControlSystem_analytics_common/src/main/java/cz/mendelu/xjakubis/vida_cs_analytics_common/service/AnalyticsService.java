package cz.mendelu.xjakubis.vida_cs_analytics_common.service;

import cz.mendelu.xjakubis.vida_cs_analytics_common.Configuration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class AnalyticsService {

    private final String apiUrl;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private final RestTemplate restTemplate;
    private final List<String> paths = new ArrayList<>();

    public AnalyticsService(RestTemplateBuilder restTemplateBuilder) {
        this.apiUrl = Configuration.getInstance().getProperty("analyticsApi.url");
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(500))
                .setReadTimeout(Duration.ofSeconds(500))
                .build();
    };

    public <T> AnalyticsService at(String... paths) {
        this.paths.addAll(Arrays.asList(paths));
        return this;
    }

    public <T,U> Future<U> sendPost( T entity, Class<U> responseType) {
        Callable<U> callable = () -> {
            try {
                HttpHeaders headers = new HttpHeaders();
                headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
                headers.setContentType(MediaType.APPLICATION_JSON);

                RestTemplate restTemplate = new RestTemplate();

                // Data attached to the request.
                HttpEntity<T> requestBody = new HttpEntity<>(entity, headers);

                // Send request with POST method.
                U e = restTemplate.postForObject(getUrl(), requestBody, responseType);

                if (e != null) {
                    System.out.println("POST was successful");
                } else {
                    System.out.println("Something error!");
                }
                return responseType.cast(e);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        };

        return runCallable(callable);
    }

    public <T> Future<T> get(Class<T> responseType) {
        Callable<T> callable = () -> {
            try {
                ResponseEntity<T> response = restTemplate.getForEntity(getUrl(), responseType);
                if (response.getStatusCode() == HttpStatus.OK) {
                    return responseType.cast(response.getBody());
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        };

        return runCallable(callable);
    }

    private <T> Future<T> runCallable(Callable callable){
        FutureTask<T> futureTask = new FutureTask<T>(callable);
        Thread thread = new Thread(futureTask);
        thread.start();
        return futureTask;
    }

    public void delete() {
        Runnable runnable = () -> {
            try {
                restTemplate.delete(getUrl());
            } catch (Exception e) {
                throw e;
            }
        };

        executor.submit(runnable);
    }

    private String getUrl(){
        String url = apiUrl;
        for(String path : this.paths){
            url += "/"+ path;
        }
        return url;
    }
}
