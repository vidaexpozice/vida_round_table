package cz.mendelu.xjakubis.vida_cs_analytics_common.api_model;

import java.time.LocalDateTime;

public class VotingResultsStatistic {
    private long proCount;
    private long againstCount;
    private LocalDateTime from;
    private LocalDateTime to;
    private long approvedCount;
    private long rejectedCount;

    public VotingResultsStatistic(long proCount, long againstCount, LocalDateTime from, LocalDateTime to, long approvedCount, long rejectedCount) {
        this.proCount = proCount;
        this.againstCount = againstCount;
        this.from = from;
        this.to = to;
        this.approvedCount = approvedCount;
        this.rejectedCount = rejectedCount;
    }

    public long getApprovedCount() {
        return approvedCount;
    }

    public void setApprovedCount(long approvedCount) {
        this.approvedCount = approvedCount;
    }

    public long getRejectedCount() {
        return rejectedCount;
    }

    public void setRejectedCount(long rejectedCount) {
        this.rejectedCount = rejectedCount;
    }

    public VotingResultsStatistic() {
    }

    public long getProCount() {
        return proCount;
    }

    public void setProCount(long proCount) {
        this.proCount = proCount;
    }

    public long getAgainstCount() {
        return againstCount;
    }

    public void setAgainstCount(long againstCount) {
        this.againstCount = againstCount;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }
}
