package cz.mendelu.xjakubis.vida_cs_analytics_common.service;

import cz.mendelu.xjakubis.vida_cs_analytics_common.exception.NotInitializedException;
import cz.mendelu.xjakubis.vida_cs_analytics_common.helper.EventHelper;

import java.util.UUID;

public class GameAnalytics extends Analytics {

    private static int uniqueGameId = -1;
    private static long uniqueGamePoolId = -1;
    private static String gamerUUID = "";

    public static void initialize(int gameId, long gamePoolId){
        initialize(gameId);
        uniqueGamePoolId = gamePoolId;
        gamerUUID = UUID.randomUUID().toString();
    }

    public static void initialize(int gameId){
        uniqueGameId = gameId;
    }

    public static void newEvent(String category, String resource, String action){
        validateInitialization();
        EventHelper.newEvent(uniqueGameId, uniqueGamePoolId, gamerUUID, category, resource, action );
    }

    private static void validateInitialization() {
        if (uniqueGameId == -1){
            throw new NotInitializedException();
        }
    }
}
