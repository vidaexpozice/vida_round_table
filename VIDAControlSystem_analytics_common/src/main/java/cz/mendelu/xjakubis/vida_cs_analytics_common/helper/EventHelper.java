package cz.mendelu.xjakubis.vida_cs_analytics_common.helper;

import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Event;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.GamePool;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Game;
import cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics;

import java.time.LocalDateTime;

public class EventHelper {

    public static void newEvent(int gameId, long gamePoolId, String gamerUUID, String category, String resource, String action) {
        if (gamePoolId != -1) {
           newEvent(new Game(gameId), new GamePool(gamePoolId), category, resource, action, gamerUUID);
        } else {
            newEvent(gameId, category, resource, action);
        }
    }

    public static void newEvent(Game game, GamePool gamePool, String category, String resource, String action, String gamerUUID) {
        Event event = new Event(game, gamePool, category, resource, action, LocalDateTime.now(), gamerUUID);
        GameAnalytics.getRestService().at("event","add").sendPost(event, Event.class);
    }

    public static void newEvent(int gameId, String category, String resource, String action){
        Event event = new Event(new Game(gameId), category, resource, action, LocalDateTime.now());
        GameAnalytics.getRestService().at("event","add").sendPost(event, Event.class);
    }

    public static void newEvent(String category, String resource, String action){
        Event event = new Event( category, resource, action, LocalDateTime.now());
        GameAnalytics.getRestService().at("event","add").sendPost(event, Event.class);
    }
}
