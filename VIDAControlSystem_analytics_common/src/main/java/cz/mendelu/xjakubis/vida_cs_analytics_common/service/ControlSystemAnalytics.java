package cz.mendelu.xjakubis.vida_cs_analytics_common.service;

import cz.mendelu.xjakubis.vida_cs_analytics_common.helper.EventHelper;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.GamePool;
import cz.mendelu.xjakubis.vida_cs_analytics_common.model.Game;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.concurrent.Future;

import static cz.mendelu.xjakubis.vida_cs_analytics_common.service.GameAnalytics.getRestService;

public class ControlSystemAnalytics extends Analytics {
    private static HashMap<Integer, Future<GamePool>> gamePoolsByGameInstance = new HashMap<>();

    public static Future<GamePool> newActiveGamePool(byte numberOfClients, int gameInstance, int gameIndex) {
        GamePool gamePool = new GamePool(LocalDateTime.now(), numberOfClients, new Game(gameIndex));
        Future<GamePool> gamePoolFuture = GameAnalytics.getRestService().at("gamePool", "save").sendPost(gamePool, GamePool.class);
        gamePoolsByGameInstance.put(gameInstance, gamePoolFuture);
        return gamePoolFuture;
    }

    public static void stopGamePool(int gameInstance) {
        try {
            Future<GamePool> futureGamePool = gamePoolsByGameInstance.remove(gameInstance);
            if (futureGamePool != null) {
                GamePool gamePool = futureGamePool.get();
                gamePool.setEndDateTime(LocalDateTime.now());
                getRestService().at("gamePool", "save").sendPost(gamePool, GamePool.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long getGamePoolId(int gameInstance) {
        try {
            Future<GamePool> futureGamePool = gamePoolsByGameInstance.get(gameInstance);
            GamePool gamePool = futureGamePool.get();
            return gamePool.getId();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }
}
