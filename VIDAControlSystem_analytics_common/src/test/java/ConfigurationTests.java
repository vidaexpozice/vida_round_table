import cz.mendelu.xjakubis.vida_cs_analytics_common.Configuration;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.io.IOException;

public class ConfigurationTests {

    @Test
    public void configurationReturnsProperties() throws IOException {
        String databaseUrl = Configuration.getInstance().getProperty("database.url");

        assertEquals("jdbc:mysql://localhost:3306/db_analytics", databaseUrl);

    }
/*
    @Test
    public void propertiesReader() throws IOException {
        PropertiesReader reader = new PropertiesReader("pom.properties");
        String property = reader.getProperty("my.awesome.property");
        assertEquals("property-from-pom", property);
    }*/
}
