import cz.mendelu.savic.utils.LogUtils;
import cz.mendelu.savic.vida_cs.model.IPAddressConfig;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.time.Instant;

/**
 * Client for communicating with server and sending following message signals:
 *
 * Client -> Server:
 *   JOIN X - I wanna join game with number X (or create if I'm first).
 *   LEAVE X - I wanna leave waiting queue for game with number X.
 *   START X - I want to launch game with number X.
 *   GET_INFO X - I wanna know how many players are connected to game X.
 *   FINISH X - I finished game with number X (either game finished, player left, or error occured)
 *   GET_PROJECTOR_INFO - I wanna know if the projector is being used.
 *
 * Server -> Client:
 *   ACCEPT X - I accept your request to join game X. (he will be accepted as long
 *              as he is playing the game) -> player goes to INGAME status.
 *   REJECT X - I reject your request to join game X.
 *   CONNECTED X Y - The game with number X has Y players connected.
 *   MESSAGE text - Client, please notify the user with this message.
 *   IN_PROGRESS X - Game with number X is in progress (already played).
 *   LAUNCH X P - Client, please start game X which you requested on port P.
 *   END X - Client, close game X (if you didn't do it yet), because it finished.
 *   PROJECTOR_IN_USE - Client, projector is being used right now.
 *   PROJECTOR_NOT_IN_USE - Client, projector is available. You can use it.
 *   KILL_YOURSELF - Client, close your application.
 *
 * Where X is number of the game and Y number of players.
 *
 */
/**
 * <p>Client for communicating with server and sending following message signals:</p>
 *
 * <p><b>Client -> Server:</b><br>
 *   <ul>
 *   <li>MY_COLOR_IS C - Client is ready for game and its snake will have color C.</li>
 *   <li>MOVE_ME D - Move my snake to direction D.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Server -> Client:</b>
 *   <ul>
 *   <li>COUNTDOWN_COLOR T - Client, you have T seconds to choose snake color.</li>
 *   <li>COUNTDOWN_START T - Client, the game starts (snakes will start to move) at T seconds.</li>
 *   <li>SEND_ME_COLOR - Client, send me the color you've chosen.</li>
 *   <li>INIT_BOARD INIT_STRING - Clients, init the board according to information
 *       from INIT_STRING.</li>
 *   <li>MOVE DIRECTIONS_STRING - Move all snakes according to DIRECTIONS_STRING</li>
 *   <li>ADD_FOOD P C - Add food to position P with color C.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Where:</b>
 *   <ul>
 *   <li>T is a text, usually number of seconds to countdown or message
 *      (used for countdown = 0 - e.g. Start!),</li>
 *   <li>X is number of snake,</li>
 *   <li>D is one of direction: UP, RIGHT, DOWN, LEFT,</li>
 *   <li>C is color represented by HEX web value, for example #ff7f50.</li>
 *   <li>P specifies place in a board. It is represented by two numbers separated by hash.</li>
 *   <li>INIT_STRING is string with information about selected background, player snake number,
 *       number of snakes and colors of these snakes. It is made of number representing chosen background,
 *       followed by hash and six symbols for each snake representing HEX color of the snake.
 *       For example 3#1#ff7f50#ff7fff for background number 3, 2 snakes with the colors
 *       according to HEX values and player plays for snake number 1.</li>
 *   <li>DIRECTION_STRING is string with information about all snakes directions.
 *       It is made of letters for each snake representing their current directions.
 *       The letters are U (for up), D (for down), R (for right) and L (for left).
 *       For example DULLR is information of about directions for five snakes.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Sequence to start the game:</b><br>
 *   COUNTDOWN_COLOR -> SEND_ME_COLOR -> MY_COLOR_IS -> INIT_BOARD -> COUNTDOWN_START -> MOVE
 * </p>
 */
public class Client extends Thread {
    private String lastMessage;
    private Instant lastMessageTime;
    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private NewMessageListener listener;

    public Client(int port) throws IOException {
        IPAddressConfig ipConfig = new IPAddressConfig();
        socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(ipConfig.getIPAddress(), port), 5000);
        } catch (ConnectException ex) {
            ex.printStackTrace();
        }

        input = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        output = new PrintWriter(
                socket.getOutputStream(), true);

        start();
    }

    public void disconnect() throws IOException {
        socket.close();
    }

    public synchronized String getLastMessage() throws InterruptedException {
        return lastMessage;
    }

    private synchronized void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
        if (listener != null) {
            listener.messageReceiver(lastMessage);
        }
    }

    @Override
    public void run() {
        String response;

        while (true) {
            try {
                response = input.readLine();
                setLastMessage(response);

                String[] responseParts = response.split(" ");

                if (response.startsWith("CONNECTED")) {

                }
            } catch (SocketException e) {
                // server disconnected
                try {
                    socket.close();
                    System.out.println("closing socket");
                    Platform.exit();
                    System.exit(0);
                } catch (IOException e1) {
                    LogUtils.logException(this, e1);
                }
                return;
            } catch (IOException e) {
                LogUtils.logException(this, e);
                e.printStackTrace();
            } catch (Exception e) {
                LogUtils.logException(this, e);
                e.printStackTrace();
            }
        }
    }

    public void getInfo(int gameIndex) {
        output.println("GET_INFO " + gameIndex);
    }

    public void join(int gameIndex) {
        output.println("JOIN " + gameIndex);
    }

    public void start(int gameIndex) {
        output.println("START " + gameIndex);
    }

    public void leave(int gameIndex) {
        output.println("LEAVE " + gameIndex);
    }

    public void sendColor(String colorHex) {
        output.println("MY_COLOR_IS " + colorHex);
    }

    public void moveMe(String direction) {
        output.println("MOVE_ME " + direction);
    }

    public void setListener(NewMessageListener listener) {
        this.listener = listener;
    }
}
