import cz.mendelu.savic.vida_cs.model.IPAddressConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 08.10.2017.
 */
public class ITMultipleSnakesTest {
    private static final long SERVER_START_UP_WAIT_TIME = 1000;
    private static final long RESPONSE_WAIT_TIME = 30;
    private static final boolean RUN_SERVER = false;
    private static Thread processChecker;
    private static Process process;
    private static final int PORT = 8888;

    public void init() throws IOException, InterruptedException {
        if (RUN_SERVER) {
            final String ipAddress = new IPAddressConfig().getIPAddress();
            process = Runtime.getRuntime().exec(
                    "java -jar VIDAControlSystem_snake_server/VIDAControlSystem_snake_server.jar " +
                            PORT);
            processChecker = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            if (process.isAlive()) {
                                Thread.sleep(1000);
                                continue;
                            }
                        } catch (InterruptedException ex) {
                        }

                        return;
                    }
                }
            };
            processChecker.start();

            // wait for server app to start
            Thread.sleep(SERVER_START_UP_WAIT_TIME);
        }
    }

    private String getLastClientMessage(Client client) throws InterruptedException {
        waitForResponse();
        String lastMessage;
        while ((lastMessage = client.getLastMessage()) == null) {
            Thread.sleep(200);
        }

        return lastMessage;
    }

    private void waitForResponse() throws InterruptedException {
        Thread.sleep(RESPONSE_WAIT_TIME);
    }

    public enum DirectionsEnum {
        UP,
        RIGHT,
        DOWN,
        LEFT;
    }

    class ArrayBufferList<T> extends ArrayList<T> {
        public ArrayBufferList() {
            super();
        }

        public ArrayBufferList(List<T> directions) {
            super(directions);
        }

        @Override
        public T get(int i) {
            final T object = super.get(i);
            super.remove(i);
            return object;
        }

        public T get() {
            return get(0);
        }
    }

    public void theBigTest() throws IOException, InterruptedException {
        List<String> directions1 = new ArrayBufferList<>();
        for (int i = 0; i < 50; i++) {
            directions1.add(DirectionsEnum.RIGHT.name());
            directions1.add(DirectionsEnum.RIGHT.name());
            directions1.add(DirectionsEnum.DOWN.name());
            directions1.add(DirectionsEnum.DOWN.name());
            directions1.add(DirectionsEnum.LEFT.name());
            directions1.add(DirectionsEnum.LEFT.name());
            directions1.add(DirectionsEnum.UP.name());
            directions1.add(DirectionsEnum.UP.name());
        }

        List<String> directions2 = new ArrayBufferList<>(directions1);
        List<String> directions3 = new ArrayBufferList<>(directions1);
        List<String> directions4 = new ArrayBufferList<>(directions1);
        List<String> directions5 = new ArrayBufferList<>(directions1);
        List<String> directions6 = new ArrayBufferList<>(directions1);
        List<String> directions7 = new ArrayBufferList<>(directions1);

        Client client1 = new Client(PORT);
        Client client2 = new Client(PORT);
        Client client3 = new Client(PORT);
        Client client4 = new Client(PORT);
        Client client5 = new Client(PORT);
        Client client6 = new Client(PORT);
        Client client7 = new Client(PORT);
        client1.setListener(message -> {
            System.out.println(message);
            if (message.startsWith("MOVE") && !directions1.isEmpty()) {
                client1.moveMe(directions1.get(0));
            }
        });
        client2.setListener(message -> {
            if (message.startsWith("MOVE") && !directions2.isEmpty()) {
                String direction = directions2.get(0);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
                client2.moveMe(direction);
            }
        });
        client3.setListener(message -> {
            if (message.startsWith("MOVE") && !directions3.isEmpty()) {
                String direction = directions3.get(0);
                client3.moveMe(direction);
            }
        });
        client4.setListener(message -> {
            if (message.startsWith("MOVE") && !directions4.isEmpty()) {
                String direction = directions4.get(0);
                client4.moveMe(direction);
            }
        });
        client5.setListener(message -> {
            if (message.startsWith("MOVE") && !directions5.isEmpty()) {
                String direction = directions5.get(0);
                client5.moveMe(direction);
            }
        });
        client6.setListener(message -> {
            if (message.startsWith("MOVE") && !directions6.isEmpty()) {
                String direction = directions6.get(0);
                client6.moveMe(direction);
            }
        });
        client7.setListener(message -> {
            if (message.startsWith("MOVE") && !directions7.isEmpty()) {
                String direction = directions7.get(0);
                client7.moveMe(direction);
            }
        });
        while (!getLastClientMessage(client1).equals("SEND_ME_COLOR") &&
                !getLastClientMessage(client2).equals("SEND_ME_COLOR") &&
                !getLastClientMessage(client3).equals("SEND_ME_COLOR")) {
            Thread.sleep(1000);
        }
        client1.sendColor("#ff7f50");
        client2.sendColor("#7fff50");
        client3.sendColor("#7f50ff");
        client4.sendColor("#507fff");
        client5.sendColor("#50ff7f");
        client6.sendColor("#ff507f");
        client7.sendColor("#ffffff");

        while (true) {
            Thread.sleep(1000);
        } // wait for socket disconnect
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        ITMultipleSnakesTest tester = new ITMultipleSnakesTest();
        tester.init();
        tester.theBigTest();
    }
}
