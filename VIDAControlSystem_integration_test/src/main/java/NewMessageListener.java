/**
 * Created by david on 05.11.2017.
 */
public interface NewMessageListener {
    void messageReceiver(String message);
}
