import org.junit.*;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by david on 08.07.2017.
 */
public class ITControlSystemServerTest {
    private static final long SERVER_START_UP_WAIT_TIME = 7000;
    private static final long RESPONSE_WAIT_TIME = 30;
    private static final int PORT = 7000;
    private static Thread processChecker;
    private static Process process;

    @Before
    public void init() throws IOException, InterruptedException {
        process = Runtime.getRuntime().exec("java -jar VIDAControlSystem_server/VIDAControlSystem_server.jar");
        processChecker = new Thread() {
            public void run() {
            while (true) {
                try {
                    if (process.isAlive()) {
                        Thread.sleep(1000);
                        continue;
                    }
                } catch (InterruptedException ex) {
                }

                return;
            }
            }
        };
        processChecker.start();

        // wait for server app to start
        Thread.sleep(SERVER_START_UP_WAIT_TIME);
    }

    @After
    public void cleanUp() throws InterruptedException {
        process.destroy();
    }

    private void waitForResponse() throws InterruptedException {
        Thread.sleep(RESPONSE_WAIT_TIME);
    }

    private String getLastClientMessage(Client client) throws InterruptedException {
        waitForResponse();
        return client.getLastMessage();
    }

    @Test
    public void testMultipleJoinsAndLeaves() throws IOException, InterruptedException {
        Client client = new Client(PORT);
        for (int i = 0; i < 10; i++) {
            client.join(0);
            assertConnected(client, 1);
            client.leave(0);
            assertConnected(client, 0);
        }
    }

    private void assertConnected(Client client, int numberOfPlayers) throws InterruptedException {
        client.getInfo(0);
        Assert.assertEquals("CONNECTED 0 " + numberOfPlayers, getLastClientMessage(client));
    }

    @Test
    public void testMultipleClientsJoin() throws IOException, InterruptedException {
        final int NUMBER_OF_CLIENTS = 10;
        ArrayList<Client> clients = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_CLIENTS; i++) {
            clients.add(new Client(PORT));
        }
        Assert.assertEquals(NUMBER_OF_CLIENTS, clients.size());

        for (int i = 0; i < 500; i++) {
            System.out.println((i + 1) + ". run.");
            for (Client client : clients) {
                client.join(0);
            }

            assertConnected(clients.get(0), 8);

            for (Client client : clients) {
                client.leave(0);
            }
        }

        Thread.sleep(5000);
    }

    @Test
    public void testMultipleClientsConnect() throws IOException, InterruptedException {
        final int NUMBER_OF_CLIENTS = 500;
        ArrayList<Client> clients = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_CLIENTS; i++) {
            clients.add(new Client(PORT));
        }
        Assert.assertEquals(NUMBER_OF_CLIENTS, clients.size());

        Thread.sleep(5000);
    }
}
