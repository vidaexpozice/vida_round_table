/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_server;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake_server.controller.InitBoardListener;
import cz.mendelu.savic.vida_cs_snake_server.controller.ProjectorWindowController;
import cz.mendelu.savic.vida_cs_snake_server.controller.Server;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import cz.mendelu.savic.vida_cs_snake_server.model.ClientWithColor;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author david
 */
public class MainLoader extends Application {

    private Stage primaryStage;
    private static int port;
    private Server server;
    private ProjectorWindowController projectorWindowController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        server = new Server(port);
        server.addListener(new InitBoardListener() {
            @Override
            public void initBoard() {
                initBoards();
                startGameWithCountdown();
            }
        });

        server.start();

        final BorderPane mainPane = new BorderPane();
        final Label label = new Label("waiting for clients to connect");
        label.setFont(new Font("Arial", 28));
        mainPane.setCenter(label);
        mainPane.setPrefSize(600, 400);
        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setTitle("SnakeXSnake projector");
        //primaryStage.setFullScreen(true);
        primaryStage.setOnCloseRequest(event -> {
            //server.stopThreads();
            Platform.exit();
            System.exit(0);
        });
        //primaryStage.setMinWidth(1000);
        //primaryStage.setMinHeight(1000);
        primaryStage.show();
    }

    public MainLoader getMainLoader() {
        return this;
    }

    private void initBoards() {
        FXMLLoader projectorWindowLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_snake_server/view/ProjectorWindow.fxml"));

        Parent projectorWindowRoot;
        try {
            projectorWindowRoot = projectorWindowLoader.load();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        Random random = new Random(System.currentTimeMillis());
        int randomBackgroundNumber = random.nextInt(4) + 1;

        // get list of colors from clientsWithColors list
        List<Color> colors = server.getClientsWithColors().stream().map(ClientWithColor::getColor)
                .collect(Collectors.toCollection(ArrayList::new));

        server.sendInitGameToAll(randomBackgroundNumber);

        projectorWindowController
                = projectorWindowLoader.getController();

        GameBoardController gameBoardController = null;
        try {
            gameBoardController = projectorWindowController.init(
                    server,
                    getMainLoader(),
                    randomBackgroundNumber,
                    server.getClientsWithColors().size(),
                    colors);
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        server.setGameBoardController(gameBoardController);

        Platform.runLater(() -> {
            this.primaryStage.setFullScreenExitHint("");
            this.primaryStage.setFullScreen(true);
            this.primaryStage.setScene(new Scene(projectorWindowRoot));
            projectorWindowRoot.requestFocus();
        });
    }
    
    public void startGameWithCountdown() {
        countdownFrom(5);
        projectorWindowController.startMoveTimer();
    }
    
    public void countdownFrom(int number) {
        GameBoardController gameBoardController =
                projectorWindowController.getGameBoardController();
        
        try {
            Thread.sleep(1000);
            
            while (number > 0) {
                server.sendCountdownStart(number);
                gameBoardController.showCountdown(number);
                number--;
                Thread.sleep(1000);
            }
            
            Thread thread = new Thread() {
                @Override
                public void run() {
                    server.sendCountdownStart(0);
                    gameBoardController.showCountdown(0);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    gameBoardController.hideCountdownLabel();
                }
            };
            
            thread.start();
            
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            return;
        }

        port = Integer.valueOf(args[0]);
        launch(args);
    }
}
