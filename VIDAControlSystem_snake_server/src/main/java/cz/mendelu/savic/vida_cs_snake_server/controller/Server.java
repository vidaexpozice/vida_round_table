/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_server.controller;

import cz.mendelu.savic.vida_cs_snake.controller.ColorUtils;
import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake.model.DirectionEnum;
import cz.mendelu.savic.vida_cs_snake.model.SnakePart;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import cz.mendelu.savic.vida_cs_snake_server.model.ClientWithColor;
import javafx.application.Platform;
import javafx.scene.paint.Color;

/**
 * @author david
 */
public class Server extends Thread {

    private ServerSocket serverSocket;
    private boolean acceptClients = true;
    private List<InitBoardListener> initBoardListeners = new ArrayList<>();
    private GameBoardController gameBoardController;
    private List<ClientWithColor> clientsWithColors;
    private HashMap<ClientHandler, DirectionEnum> nextDirections;

    public Server(int port) throws IOException {
        clientsWithColors = new ArrayList<>();
        serverSocket = new ServerSocket(port);
        nextDirections = new HashMap<>();

        printMessageOnServer("Poslouch�m na IP adrese "
                + InetAddress.getLocalHost().getHostAddress() + ", na portu "
                + serverSocket.getLocalPort());
        printMessageOnServer("�ek�m a� se p�ipoj� klienti");
    }

    @Override
    public void run() {
        while (acceptClients) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
                socket.setKeepAlive(true);
                socket.setSoTimeout(Integer.MAX_VALUE);
                acceptClient(socket);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public synchronized void setGameBoardController(GameBoardController gameBoardController) {
        this.gameBoardController = gameBoardController;
    }

    public synchronized void acceptClient(Socket socket) {
        printMessageOnServer("P�ijal jsem klienta na portu " + socket.getPort() +
                " a IP adrese " + socket.getInetAddress().toString().replace("/", ""));

        ClientHandler clientHandler = new ClientHandler(this, socket);
        clientsWithColors.add(new ClientWithColor(clientsWithColors.size(), clientHandler));
        clientHandler.start();

        if (clientsWithColors.size() == 1) {
            countdownColorChoosing();
        }
    }

    private void countdownColorChoosing() {
        Thread colorCountdownThread = new Thread() {
            @Override
            public void run() {
                super.run();
                // when first client is accepted start countdown for choosing color

                for (int i = 10; i > 0; i--) {
                    sendToAllClients("COUNTDOWN_COLOR " + i);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }

                // from this moment don't accept any more clients
                acceptClients = false;

                sendToAllClients("SEND_ME_COLOR");
            }
        };

        colorCountdownThread.start();

    }

    private synchronized void printMessageOnServer(String message) {
        System.out.println(message);
    }

    public synchronized void sendToAllClients(String message) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Iterator<ClientWithColor> iterator = clientsWithColors.iterator();
                        ClientWithColor clientHandler;

                        while (iterator.hasNext()) {
                            clientHandler = iterator.next();
                            clientHandler.getClientHandler().getOutput().println(message);
                    /*try {
                        // delay -> wait, between sending the messages
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        printMessageOnServer(e.getMessage());
                    }*/
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            thread.start();
        } catch (Exception ex) {
        }
    }

    private void sendToSelectedClient(String message, ClientWithColor clientHandler) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        clientHandler.getClientHandler().getOutput().println(message);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            thread.start();
        } catch (Exception ex) {
        }
    }

    public synchronized void addClientColor(ClientHandler clientHandler, Color color) {
        boolean allHaveColor = true;

        for (ClientWithColor client : clientsWithColors) {
            if (client.getClientHandler().equals(clientHandler)) {
                client.setColor(color);
            } else if (client.getColor() == null) {
                allHaveColor = false;
            }
        }

        if (allHaveColor) {
            for (InitBoardListener listener : initBoardListeners) {
                listener.initBoard();
            }
        }
    }

    public synchronized void addListener(InitBoardListener listener) {
        initBoardListeners.add(listener);
    }

    public synchronized List<ClientWithColor> getClientsWithColors() {
        return clientsWithColors;
    }

    public synchronized void updateSnakeDirections() {
        ClientHandler clientHandler;
        if (gameBoardController != null) {
            for (ClientWithColor client : clientsWithColors) {
                clientHandler = client.getClientHandler();
                if (nextDirections.containsKey(clientHandler)) {
                    gameBoardController.setDirection(client.getIndex(), nextDirections.get(clientHandler));
                }
            }
        }

        nextDirections.clear();
    }

    public synchronized void setSnakeDirection(ClientHandler clientHandler, DirectionEnum direction) {
        nextDirections.put(clientHandler, direction);
    }

    public synchronized void sendInitGameToAll(int backgroundNumber) {
        String initInfo = String.valueOf(backgroundNumber);
        initInfo += "#$";
        Iterator<ClientWithColor> iterator = clientsWithColors.iterator();

        while (iterator.hasNext()) {
            ClientWithColor client = iterator.next();
            initInfo += ColorUtils.getWebStringFromColor(client.getColor());
        }

        iterator = clientsWithColors.iterator();
        while (iterator.hasNext()) {
            ClientWithColor client = iterator.next();
            sendToSelectedClient("INIT_BOARD " + initInfo.replace("$", String.valueOf(client.getIndex() + 1)),
                    client);
        }
    }

    public synchronized void sendCountdownStart(int number) {
        sendToAllClients("COUNTDOWN_START " + number);
    }

    public synchronized void sendFoodInfo(SnakePart foodPart) {
        sendToAllClients("ADD_FOOD " + (int) foodPart.getX() + "#" + (int) foodPart.getY() + " " +
                ColorUtils.getWebStringFromColor(foodPart.getColor()));
    }

    public synchronized String getSnakeDirectionsString() {
        return gameBoardController.getSnakeDirectionsString();
    }

    public synchronized void endGame(String winnerColorName) {
        if (winnerColorName == null) {
            if (clientsWithColors.size() == 1) {
                gameBoardController.showEndMessage("Prohr�l jsi");
            } else {
                gameBoardController.showEndMessage("Nikdo nevyhr�l");
            }
        } else {
            gameBoardController.showEndMessage("Vyhr�l " + winnerColorName + " had");
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
        Platform.exit();
        System.exit(0);
    }
}
