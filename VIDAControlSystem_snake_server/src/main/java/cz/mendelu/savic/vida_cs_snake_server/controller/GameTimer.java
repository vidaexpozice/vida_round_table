/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_server.controller;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake.model.SnakePart;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author david
 */
public class GameTimer extends TimerTask {

    private final long MOVE_PERIOD = 300; // period for moving snakes in ms
    private final GameBoardController gameBoardController;
    private final int numberOfPlayers;
    private final Server server;
    private Timer timer;
    private int step;
    private int actualSnakeIndex;
    private final boolean moveSnakesSeparately;
    private boolean endGame = false;

    public GameTimer(Server server, GameBoardController gameBoardController, int numberOfPlayers) {
        this(server, gameBoardController, numberOfPlayers, false);
    }
    
    public GameTimer(Server server, GameBoardController gameBoardController, int numberOfPlayers,
            boolean moveSnakesSeparately) {
        this.server = server;
        this.gameBoardController = gameBoardController;
        this.step = 0;
        this.numberOfPlayers = numberOfPlayers;
        this.actualSnakeIndex = 0;
        this.moveSnakesSeparately = moveSnakesSeparately;

        this.gameBoardController.addListener(winnerColorName -> {
            server.endGame(winnerColorName);
            endGame = true;
        });
    }

    @Override
    public void run() {
        if (endGame) {
            return;
        }

        server.updateSnakeDirections();
        server.sendToAllClients("MOVE " + server.getSnakeDirectionsString());
        if (moveSnakesSeparately) {
            //gameBoardController.moveSnake(actualSnakeIndex);
        } else {
            gameBoardController.moveSnakes();
        }

        if (!moveSnakesSeparately || actualSnakeIndex == 0) {
            // every 15th move add food
            if (step % 15 == 14) {
                SnakePart foodPart = gameBoardController.addRandomFood();
                if (foodPart != null) {
                    server.sendFoodInfo(foodPart);
                }
            }

            step++;
        }
        
        if (moveSnakesSeparately) {
            actualSnakeIndex = (actualSnakeIndex + 1) % numberOfPlayers;
        }
    }

    public void start() {
        timer = new Timer(true);
        if (moveSnakesSeparately) {
            timer.scheduleAtFixedRate(this, 1000, MOVE_PERIOD / numberOfPlayers);
        } else {
            timer.scheduleAtFixedRate(this, 1000, MOVE_PERIOD);
        }
    }

}
