/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import cz.mendelu.savic.vida_cs_snake.model.DirectionEnum;
import javafx.scene.paint.Color;

/**
 * <p>Client handler (server part) for communicating with client and sending following message
 * signals:</p>
 * 
 * <p><b>Client -> Server:</b><br>
 *   <ul>
 *   <li>MY_COLOR_IS C - Client is ready for game and its snake will have color C.</li>
 *   <li>MOVE_ME D - Move my snake to direction D.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Server -> Client:</b>
 *   <ul>
 *   <li>COUNTDOWN_COLOR T - Client, you have T seconds to choose snake color.</li>
 *   <li>COUNTDOWN_START T - Client, the game starts (snakes will start to move) at T seconds.</li>
 *   <li>SEND_ME_COLOR - Client, send me the color you've chosen.</li>
 *   <li>INIT_BOARD INIT_STRING - Clients, init the board according to information
 *       from INIT_STRING.</li>
 *   <li>MOVE DIRECTIONS_STRING - Move all snakes according to DIRECTIONS_STRING</li>
 *   <li>ADD_FOOD P C - Add food to position P with color C.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Where:</b>
 *   <ul>
 *   <li>T is a text, usually number of seconds to countdown or message
 *      (used for countdown = 0 - e.g. Start!),</li>
 *   <li>X is number of snake,</li>
 *   <li>D is one of direction: UP, RIGHT, DOWN, LEFT,</li>
 *   <li>C is color represented by HEX web value, for example #ff7f50.</li>
 *   <li>P specifies place in a board. It is represented by two numbers separated by hash.</li>
 *   <li>INIT_STRING is string with information about selected background, player snake number,
 *       number of snakes and colors of these snakes. It is made of number representing chosen background,
 *       followed by hash and six symbols for each snake representing HEX color of the snake.
 *       For example 3#1#ff7f50#ff7fff for background number 3, 2 snakes with the colors
 *       according to HEX values and player plays for snake number 1.</li>
 *   <li>DIRECTION_STRING is string with information about all snakes directions.
 *       It is made of letters for each snake representing their current directions.
 *       The letters are U (for up), D (for down), R (for right) and L (for left).
 *       For example DULLR is information of about directions for five snakes.</li>
 *   </ul>
 * </p>
 *
 * <p><b>Sequence to start the game:</b><br>
 *   COUNTDOWN_COLOR -> SEND_ME_COLOR -> MY_COLOR_IS -> INIT_BOARD -> COUNTDOWN_START -> MOVE
 * </p>
 */
public class ClientHandler extends Thread {

    private BufferedReader input;
    private PrintWriter output;
    private Server server;
    private Socket socket;
    private BlockingQueue<String> queue = new ArrayBlockingQueue<String>(50);

    public ClientHandler(Server server, Socket socket) {
        try {
            this.server = server;
            this.socket = socket;
            this.input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            this.output = new PrintWriter(
                    socket.getOutputStream(), true);
        } catch (IOException ex) {
        }
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true) {
                            try {
                                queue.put(input.readLine());
                            } catch (SocketException ex) {
                                // client disconnected
                                // TODO
                                try {
                                    socket.close();
                                    return;
                                } catch (IOException ex1) {
                                    ex1.printStackTrace();
                                }
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

                thread.start();

                String message;

                while (true) {
                    message = queue.take();
                    String[] messageParts = message.split(" ");

                    System.out.println(message);

                    if (message.startsWith("MY_COLOR_IS")) {
                        server.addClientColor(this, Color.web(messageParts[1]));
                    } else if (message.startsWith("MOVE_ME")) {
                        DirectionEnum direction = DirectionEnum.valueOf(messageParts[1]);
                        server.setSnakeDirection(this, direction);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public PrintWriter getOutput() {
        return output;
    }

    public Socket getSocket() {
        return socket;
    }
}
