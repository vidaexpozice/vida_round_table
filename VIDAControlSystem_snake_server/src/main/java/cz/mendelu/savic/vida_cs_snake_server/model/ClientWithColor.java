package cz.mendelu.savic.vida_cs_snake_server.model;

import cz.mendelu.savic.vida_cs_snake_server.controller.ClientHandler;
import javafx.scene.paint.Color;

/**
 * Created by david on 16.04.2017.
 */
public class ClientWithColor {
    private final int index;
    private final ClientHandler clientHandler;
    private Color color = null;

    public ClientWithColor(int index, ClientHandler clientHandler) {
        this.index = index;
        this.clientHandler = clientHandler;
    }

    public int getIndex() {
        return index;
    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
