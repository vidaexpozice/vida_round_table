/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.mendelu.savic.vida_cs_snake_server.controller;

import cz.mendelu.savic.vida_cs_snake.controller.GameBoardController;
import cz.mendelu.savic.vida_cs_snake_server.MainLoader;
import java.io.IOException;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Dimension2D;
import javafx.scene.Parent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author david
 */
public class ProjectorWindowController {

    @FXML
    private FlowPane root;
    private Parent gameBoardRoot;
    private GameTimer timer;
    private static final Dimension2D WINDOW_SIZE = new Dimension2D(900, 740);
    private Server server;
    private GameBoardController gameBoardController;
    private int numberOfSnakes;

    public GameBoardController init(Server server, MainLoader mainLoader, int backgroundNumber, int numberOfSnakes,
            List<Color> snakeColors) throws IOException {
        this.server = server;
        this.numberOfSnakes = numberOfSnakes;
        root.setPrefWidth(WINDOW_SIZE.getWidth());
        root.setPrefHeight(WINDOW_SIZE.getHeight());

        FXMLLoader gameBoardLoader = new FXMLLoader(
                getClass().getResource("/cz/mendelu/savic/vida_cs_snake/view/GameBoard.fxml"));

        gameBoardRoot = gameBoardLoader.load();
        gameBoardController = gameBoardLoader.getController();
        gameBoardController.init(mainLoader, backgroundNumber, numberOfSnakes, snakeColors, 500);
        
        root.getChildren().add(gameBoardRoot);
        return gameBoardController;
    }
    
    public void startMoveTimer() {
        timer = new GameTimer(server, gameBoardController, numberOfSnakes);
        timer.start();
    }

    public GameBoardController getGameBoardController() {
        return gameBoardController;
    }
}
